<footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="<?php echo base_url(); ?>#" target="_blank">Sistema de Gestão</a>. Todos Direitos Reservados</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">PRÚNI - Soluções de TI, LDA </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  
<script src="<?php echo base_url(); ?>assets/js/main.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.tabledit.js" charset="utf-8"></script>
<!-- plugins:js -->
  <script src="<?php echo base_url(); ?>assets/vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="<?php echo base_url(); ?>assets/vendors/chart.js/Chart.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url(); ?>assets/js/off-canvas.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/template.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url(); ?>assets/js/dashboard.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/wizard.js"></script>
  <!-- End custom js for this page-->

  <script>
    $(document).ready(function(){
      $(".mybtnlink").click(function(){
        var myhiperlink = $(this).attr("href");
        
        window.location = myhiperlink;
      });
    });
  </script>



  <script src="<?php echo base_url(); ?>assets/js/zepto.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/datepicker/bootstrap-datepicker.min.js"></script>


  <script>
      $(document).ready(function() {
          $('.ajax-popup-link').magnificPopup({
            type: 'ajax',
            closeBtnInside: true,
            showCloseBtn: true,
            closeOnContentClick: false,
            closeOnBgClick: false
          });

          $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
          });

          $('#notificationDropdown').click(function(){
            $(".mybellsign").hide();
          });

          $('.myuitemnotif').hover(function(){
            var n = $(this).attr("idn");
            var p = $(this).attr("idp");
            
            $.ajax({
                type:'POST',
                url: '<?php echo base_url(); ?>Notificacao/markAsViewed/'+n+'/'+p
            });
          });

          VMasker($('.ntel')).maskPattern('+244 (999) 999 999');
          VMasker($('.only-numbers')).maskNumber();

          $(".ntel").keyup(function(){
            if($(this).val().length >= 18)
            {
              $(this).removeClass("ntel-error");
              $(this).addClass("ntel-ok");
            }
            else
            {
              $(this).removeClass("ntel-ok");
              $(this).addClass("ntel-error");
            }
          });

      });

      function closePopup() {
          $.magnificPopup.close();
      }
  </script>





</body>

</html>