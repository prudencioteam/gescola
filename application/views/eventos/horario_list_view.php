<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_pessoas = lista("pessoas");
?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Lista de Horários</h4>
                  <p class="card-description"><button href="<?php echo base_url(); ?>eventos/horario_modelos" class="mybtnlink btn btn-sm btn-primary">Modelos de Horário</button> </p>
                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                      <input type="text" class="form-control" name="_search" value="<?php echo ($_POST["_search"]??""); ?>"  placeholder="Procurar Horários das Turmas" aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                </div>
                
                <div class="row">
                    <div class="col-md-12">
                      
                      <?php

                      $_pesquisa = "";
                      if(isset($_POST["_search"]))
                      {
                        $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
                        echo $_pesquisa;
                      }

                      ?>

                    </div>
                </div>

                  <div class="table-responsive pt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                           Turma
                          </th>
                          
                          <th>
                           Modelo
                          </th>
                          
                          <th>
                            Data de Criação
                          </th>
                       
                          <th>
                            Criador
                          </th>
                       
                          <th>
                            Opções
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>

                      <?php
                          foreach ($dados as $key => $value)
                          {
                            echo '<tr>
                            <td>'.$value["turma_nome"].'</td>
                            <td>'.$value["hmod_titulo"].'</td>
                            <td>'.$value["hr_dtCria"].'</td>
                            <td>'.($a_pessoas[$value["hr_userCria"]]??"").'</td>
  
                            <td>

                              <button href="'.base_url().'eventos/horario/ver/turma/'.$value["hr_turma"].'" type="button" class="mybtnlink btn btn-success btn-sm">
                              <i class="ti-eye btn-icon-prepend"></i></button>&nbsp;&nbsp;
                        
                              <button href="'.base_url().'eventos/horario_edit/'.$value["hr_id"].'" type="button" class="mybtnlink btn btn-warning btn-sm">
                              <i class="ti-pencil btn-icon-prepend"></i></button>&nbsp;&nbsp;
                        
                              <button href="'.base_url().'eventos/delete_horario_modal/'.$value["hr_id"].'" type="button" class="ajax-popup-link btn btn-danger btn-sm">
                              <i class="ti-trash btn-icon-prepend"></i></button>

                            </td>
                            </tr>';
                          }
                      ?>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        function go_criar_horario()
        {
            var turma = $("#temodal").val();
            var modelo = $("#memodal").val();
            window.location="<?php echo base_url(); ?>eventos/horario_from_modelo/"+modelo+"/"+turma;
        }


        </script>