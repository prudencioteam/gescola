<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_pessoas = lista("pessoas");

$dadosTurma = get_turmaById($turma);

?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Novo Horário</h4>
                  <p class="card-description">&nbsp;</p>

                  <div class="row">&nbsp;</div>
                
                  <form action="<?php echo base_url(); ?>eventos/save_turma" method="post">

                    <div class="row">
                        <!-- col-md-6 -->

                        <div class="col-md-12">

                            <input type="hidden" name="_modelo" value="<?php echo $modelo; ?>" />
                            
                            <div class="col-md-3 row">
                                <label>Turma</label>
                                <select class="form-control" name="_turma" required="required">
                                    <?php
                                    $a_turmas = lista("turmas");
                                    foreach ($a_turmas as $key => $value)
                                    {
                                        if($key == $turma) echo '<option value="'.$key.'">'.$value.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="row">&nbsp;</div>
                            
                            
                            <div class="table-responsive">
                                <table class="table table-bordered mb30" style="text-align: center;">
                                    <thead>
                                        <tr>
                                            <th style="color:#FFF; background-color:#339966;">Tempo</th>
                                            <th style="color:#FFF; background-color:#339966;">Segunda-Feira</th>
                                            <th style="color:#FFF; background-color:#339966; width: 100px;">Sala</th>
                                            <th style="color:#FFF; background-color:#339966;">Terça-Feira</th>
                                            <th style="color:#FFF; background-color:#339966; width: 100px;">Sala</th>
                                            <th style="color:#FFF; background-color:#339966;">Quarta-Feira</th>
                                            <th style="color:#FFF; background-color:#339966; width: 100px;">Sala</th>
                                            <th style="color:#FFF; background-color:#339966;">Quinta-Feira</th>
                                            <th style="color:#FFF; background-color:#339966; width: 100px;">Sala</th>
                                            <th style="color:#FFF; background-color:#339966;">Sexta-Feira</th>
                                            <th style="color:#FFF; background-color:#339966; width: 100px;">Sala</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    
                                    
                                    $ds = json_decode($dadosTurma["turma_disciplinas"], TRUE);
                                    $disciplinas = array();
                                    $disciplinas[""] = "Escolha uma opção";

                                    foreach ($ds as $key => $value)
                                    {
                                        $tmpVar = get_disciplinaById($value);
                                        $disciplinas[$tmpVar["disciplina_id"]] = $tmpVar["disciplina_nome"];
                                    }

                                    $listaDados = json_decode($dados["hmod_body"], TRUE);

                                    foreach ($listaDados as $key => $value)
                                    {
                                        echo '<tr>
                                                <td style="color:#FFF; background-color:#339966;">'.$value["inicio"].' - '.$value["fim"].'</td>
                                                <td>'.form_dropdown("seg_disciplina[]", $disciplinas, "", "class='form-control'").'</td>
                                                <td><input type="text" name="seg_sala[]" value="" class="form-control number" /></td>
                                                <td>'.form_dropdown("ter_disciplina[]", $disciplinas, "", "class='form-control'").'</td>
                                                <td><input type="text" name="ter_sala[]" value="" class="form-control number" /></td>
                                                <td>'.form_dropdown("qua_disciplina[]", $disciplinas, "", "class='form-control'").'</td>
                                                <td><input type="text" name="qua_sala[]" value="" class="form-control number" /></td>
                                                <td>'.form_dropdown("qui_disciplina[]", $disciplinas, "", "class='form-control'").'</td>
                                                <td><input type="text" name="qui_sala[]" value="" class="form-control number" /></td>
                                                <td>'.form_dropdown("sex_disciplina[]", $disciplinas, "", "class='form-control'").'</td>
                                                <td><input type="text" name="sex_sala[]" value="" class="form-control number" /></td>
                                            </tr>';
                                    }

                                    ?>
                        
                                    </tbody>
                                </table>
                            </div><!-- table-responsive -->
                        </div><!-- col-md-6 -->

                    </div><!-- row -->

                    

                    <div class="row">&nbsp;</div>

                    <div class="row">
                        <div class="col-md-12">
                                        
                            <button type="submit" class="btn btn-primary" style="float:right;">Salvar</button>
                        
                        </div>
                    </div>
                
                  </form>




                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        $(document).ready(function()
        {
            $(".number").keypress(function(e){
                var charCode = e.charCode ? e.charCode : e.keyCode;
                // charCode 8 = backspace   
                // charCode 9 = tab
                if (charCode != 8 && charCode != 9) {
                    // charCode 48 equivale a 0   
                    // charCode 57 equivale a 9
                    if (charCode < 48 || charCode > 57) {
                        return false;
                    }
                }
            });
        });

        </script>