<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$meuid = $this->session->userdata("pessoa_id");
$a_minhasTurmas = lista("turma_estudante", $meuid);

$a_classes = lista("classes");
$a_cursos = lista("cursos");
$a_disciplinas = lista("disciplinas");

?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                <?php
                

                    $dadosTurma = get_turmaById($_turma);
                    $dadosHorario = get_horarioByTurma($dadosTurma["turma_id"]);
                    $hmodelo = get_horarioModeloById($dadosHorario["hr_modelo"]);
                    
                  ?>

                  <div class="row">
                    <!-- col-md-6 -->
                    
                    <div class="col-md-12">
                      <center>
                        <h5 class="subtitle mb5">ITEL - Instituto de Telecomunicações</h5>
                        <h5 class="subtitle mb5">Horário da Turma <?php echo $dadosTurma["turma_nome"]; ?> - <?php echo ($a_classes[$dadosTurma["turma_classeId"]]??"") ?></h5>
                        <h5 class="subtitle mb5">Curso: <?php echo ($a_cursos[$dadosTurma["turma_curso"]]??"") ?> </h5>
                        <h5 class="subtitle mb5" align="right" style="margin-top:-20px">Ano Lectivo: <?php echo $dadosTurma["turma_ano"]; ?> </h5>
                      </center>
                      <div class="table-responsive">
                        <table class="table table-bordered mb30" style="text-align: center;">
                          <thead>
                            <tr>
                              <th style="color:#FFF; background-color:#339966;">Tempo</th>
                              <th style="color:#FFF; background-color:#339966;">Segunda-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                              <th style="color:#FFF; background-color:#339966;">Terça-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                              <th style="color:#FFF; background-color:#339966;">Quarta-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                              <th style="color:#FFF; background-color:#339966;">Quinta-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                              <th style="color:#FFF; background-color:#339966;">Sexta-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                            </tr>
                          </thead>
                          <tbody>
                            
                            <?php

                            $a_modelo = json_decode($hmodelo["hmod_body"], TRUE);
                            $a_dados = json_decode($dadosHorario["hr_body"], TRUE);

                            foreach ($a_modelo as $key => $value)
                            {
                              echo '<tr>
                                      <td style="color:#FFF; background-color:#339966;">'.$value["inicio"].' - '.$value["fim"].'</td>
                                      <td>'.($a_disciplinas[$a_dados["seg_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["seg_sala"][$key].'</td>
                                      <td>'.($a_disciplinas[$a_dados["ter_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["ter_sala"][$key].'</td>
                                      <td>'.($a_disciplinas[$a_dados["qua_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["qua_sala"][$key].'</td>
                                      <td>'.($a_disciplinas[$a_dados["qui_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["qui_sala"][$key].'</td>
                                      <td>'.($a_disciplinas[$a_dados["sex_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["sex_sala"][$key].'</td>
                                    </tr>';
                            }
                            ?>

                          </tbody>
                      </table>
                      </div><!-- table-responsive -->
                    </div><!-- col-md-6 -->
                    
                  </div><!-- row -->
<?php

        
        ?>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        function boletim_search(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_estudante_boletim/'+s,
              success:function(data){
                  console.log(data);
                  var dados = JSON.parse(data);

                  $("#bsearchresult").html("");
                  console.log(dados);
                  var xc = false;
                  for(var key in dados)
                  {
                    xc = true;
                    $("#bsearchresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }

                  if(xc)
                  {
                    $("#bsearchresult").show();
                  }
                  else
                  {
                    $("#bsearchresult").hide();
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function boletim_selectPessoa(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_pturmas_boletim/'+s,
              success:function(data){
                  var dados = JSON.parse(data);
                  $("#bselectresult").html("");
                  for(var key in dados)
                  {
                    $("#bselectresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function submit_boletim_modal()
        {
          var idpessoa = $("#bsearchresult").val();
          var turma = $("#bselectresult").val();
          var trimestre = $("#btrimestre").val();

          window.location = '<?php echo base_url(); ?>Secretario/declaracao_sem_notas/'+idpessoa+'/'+turma+'/'+trimestre;
        }

        </script>