<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_pessoas = lista("pessoas");
?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Modelos de Horários</h4>
                  <p class="card-description"><button href="<?php echo base_url(); ?>eventos/horario/todos" class="mybtnlink btn btn-sm btn-primary">Lista de Horários</button> </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                      <input type="text" class="form-control" name="_search" value="<?php echo ($_POST["_search"]??""); ?>"  placeholder="Procurar Modelos de Horários" aria-label="Procurar Horários..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
 
                      <button type="button" href="<?php echo base_url(); ?>Eventos/horario_modelos/add" class="btn btn-outline-primary btn-icon-text btn-sm mybtnlink" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Novo Modelo
                        </button>

                      </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                      
                      <?php

                      $_pesquisa = "";
                      if(isset($_POST["_search"]))
                      {
                        $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
                        echo $_pesquisa;
                      }

                      ?>

                    </div>
                </div>

                  <div class="table-responsive pt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                           Referencia
                          </th>
                          
                          <th>
                           Data de Criação
                          </th>
                          
                          <th>
                            Criador
                          </th>
                       
                          <th>
                            Opções
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>

                      <?php
                          foreach ($dados as $key => $value)
                          {
                            echo '<tr>
                            <td>'.$value["hmod_titulo"].'</td>
                            <td>'.$value["hmod_dtCria"].'</td>
                            <td>'.($a_pessoas[$value["hmod_userCria"]]??"").'</td>
  
                            <td>

                              <button href="'.base_url().'eventos/horario_modelos/ver/'.$value["hmod_id"].'" type="button" class="mybtnlink btn btn-success btn-sm">
                              <i class="ti-eye btn-icon-prepend"></i></button>&nbsp;&nbsp;

                              <button href="'.base_url().'eventos/criar_horario_turma_modal/'.$value["hmod_id"].'" type="button" class="ajax-popup-link btn btn-primary btn-sm">
                              <i class="ti-arrow-right btn-icon-prepend"></i></button>&nbsp;&nbsp;
                        
                              <button href="'.base_url().'eventos/delete_hmodelo_modal/'.$value["hmod_id"].'" type="button" class="ajax-popup-link btn btn-danger btn-sm">
                              <i class="ti-trash btn-icon-prepend"></i></button>

                            </td>
                            </tr>';
                          }
                      ?>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        function go_criar_horario()
        {
            var turma = $("#temodal").val();
            var modelo = $("#memodal").val();
            window.location="<?php echo base_url(); ?>eventos/horario_from_modelo/"+modelo+"/"+turma;
        }


        </script>