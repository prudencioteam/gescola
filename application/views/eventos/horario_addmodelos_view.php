<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_pessoas = lista("pessoas");


?>


<style>

.myfloatlefted div
{
    float: left;
}

</style>


      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Novo Modelo de Horario</h4>
                  <p class="card-description">DESCRICAO </p>

                  <div class="row">&nbsp;</div>
                  <div class="row">&nbsp;</div>

                  <form action="<?php echo base_url(); ?>Eventos/horario_modelos_save" method="post">

                    <div class="row" id="lugar">

                        <div class="col-md-12">
                          <div class="col-md-3">
                                <label>Referencia</label>
                                <input type="text" class="form-control" name="_ref" />
                            </div>
                        </div>
                    
                        <div class="col-md-12 myfloatlefted" id="line1">

                            <div class="col-md-3">
                                <label>Hora Inicial</label>
                                <input type="time" class="form-control" name="_horaInicio[]" />
                            </div>

                            <div class="col-md-3">
                                <label>Hora Final</label>
                                <input type="time" class="form-control" name="_horaFim[]" />
                            </div>

                            <div class="col-md-2">
                                <label>Opções</label><br>
                                <button onclick="rm_line(1)" class="btn btn-danger"><i class="ti ti-trash"></i></button>
                            </div>
                            
                        </div>
                    
                    
                    </div>


                        
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">&nbsp;</div>

                    <div class="col-md-12">
                        <button type="button" onclick="addLine()" class="btn btn-success">Add Linha</button>
                        <button type="submit" class="btn btn-primary" style="float:right;">Salvar</button>
                    </div>

                </form>


              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        var countLine = 1;


        function rm_line(a)
        {
          $("#line"+a).remove();
        }

        function addLine()
        {
          countLine++;
          $("#lugar").append('<div class="col-md-12 myfloatlefted" id="line'+countLine+'"><div class="col-md-3"> <label>Hora Inicial</label> <input type="time" class="form-control" name="_horaInicio[]" /></div><div class="col-md-3">  <label>Hora Final</label>  <input type="time" class="form-control" name="_horaFim[]" /></div><div class="col-md-2">  <label>Opções</label><br>  <button onclick="rm_line('+countLine+')" class="btn btn-danger"><i class="ti ti-trash"></i></button></div></div>');
        }


        </script>