<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_pessoas = lista("pessoas");


?>


<style>

.myfloatlefted div
{
    float: left;
}

</style>


      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Ver Modelo de Horario</h4>
                  <p class="card-description">DESCRICAO </p>

                  <!--div class="row">
                  <div class="col-md-12">
                 <!button type="button" href="<?php echo base_url(); ?>eventos/horario_modelos"  class="btn btn-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                    <i class="ti-arrow-left btn-icon-prepend"></i>                                                    
                    Voltar
                  </button>

                </div>
                  </div-->

                  
                  <div class="row">
                    <div class="col-md-12">

                        <h4>REFERÊNCIA: <span class="badge badge-dark" style="font-size: 18px;"><i><?php echo $dados["hmod_titulo"]; ?></i></span></h4>
                        <br>

                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>Hora de Inicio</th>
                                <th>Hora do Fim</th>
                            </tr>

                            <?php
                            $list_data = json_decode($dados["hmod_body"], TRUE);
                            foreach ($list_data as $key => $value)
                            {
                                echo '<tr>
                                            <td>'.$value["inicio"].'</td>
                                            <td>'.$value["fim"].'</td>
                                        </tr>';
                            }
                            ?>

                        </table>

                    </div>
                  </div>


              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        var countLine = 1;


        function rm_line(a)
        {
          $("#line"+a).remove();
        }

        function addLine()
        {
          countLine++;
          $("#lugar").append('<div class="col-md-12 myfloatlefted" id="line'+countLine+'"><div class="col-md-3"> <label>Hora Inicial</label> <input type="time" class="form-control" name="_horaInicio[]" /></div><div class="col-md-3">  <label>Hora Final</label>  <input type="time" class="form-control" name="_horaFim[]" /></div><div class="col-md-2">  <label>Opções</label><br>  <button onclick="rm_line('+countLine+')" class="btn btn-danger"><i class="ti ti-trash"></i></button></div></div>');
        }


        </script>