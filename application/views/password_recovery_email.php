<?php $this->load->view("header"); ?>

<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="<?php echo base_url(); ?>assets/images/logo.svg" alt="logo">
              </div>
              
              <h4>Recuperar a conta</h4>
              <h6 class="font-weight-light">Insira o teu email ou número de processo para recuperar a conta.</h6>


              <form action="<?php echo base_url(); ?>auth/recovery_final" method="post" class="pt-3">
                <div class="form-group">
                  <input type="text" name="user" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Utilizador">
                </div>
                <div class="mt-3">
                  <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">ENVIAR EMAIL</button>
                </div>
                
                <!--
                <div class="text-center mt-4 font-weight-light">
                  Don't have an account? <a href="register.html" class="text-primary">Create</a>
                </div>
                -->

              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>



 <?php $this->load->view("footer"); ?>