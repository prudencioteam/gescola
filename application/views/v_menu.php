<?php
/*
TIpos de Usuarios:
1- Direcção da Escola
2- Secretaria
3- Professor
4- Aluno
*/

?>

<nav class="sidebar sidebar-offcanvas" id="sidebar" style="background-color: #FFFFFF;">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>index.php">
              <i class="ti-home menu-icon"></i>
              <span class="menu-title">Início</span>
            </a>
          </li>

          <?php if(($this->session->userdata("pessoa_usertipo") == 1))  { ?>
      
            <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#geral" aria-expanded="false" aria-controls="geral">
              <i class="ti-view-list-alt menu-icon menu-icon"></i>
              <span class="menu-title">Cadastros Gerais</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="geral">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/lista_admins">Administradores </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/lista_estudantes">Estudantes </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/lista_professores">Professores </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/lista_encarregados">Encarregados </a></li>
              </ul>
            </div>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#eventos" aria-expanded="false" aria-controls="eventos">
              <i class="ti ti-alarm-clock menu-icon menu-icon"></i>
              <span class="menu-title">Eventos</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="eventos">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>eventos/horario/todos">Horário Escolar </a></li>
                <!--li class="nav-item"> <a class="nav-link" href="<?php //echo base_url(); ?>#">Actividades </a></li-->

              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>notificacao/listar">
              <i class="ti ti-bell menu-icon"></i>
              <span class="menu-title">Notificações</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#config" aria-expanded="false" aria-controls="config">
              <i class="ti-settings menu-icon menu-icon"></i>
              <span class="menu-title">Configurações</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="config">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>Config/base">Configurações Gerais </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/ano_lectivo">Ano Lectivo </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/cursos">Cursos </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/turmas">Turmas </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/disciplinas">Disciplinas </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/classes">Classes </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>direcao/turnos">Turnos </a></li>
              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ti-bar-chart menu-icon"></i>
              <span class="menu-title">Relatórios</span>
            </a>
          </li>


          <?php } ?>

          <?php if($this->session->userdata("pessoa_usertipo") == 2) { ?>
  
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="ti-user menu-icon"></i>
              <span class="menu-title">Estudantes</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link"  href="<?php echo base_url(); ?>secretario/mostrar_turmas">Lista de Turmas</a></li>
              
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>notificacao/listar">
              <i class="ti ti-bell menu-icon"></i>
              <span class="menu-title">Notificações</span>
            </a>
          </li>
  

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="ti-zip menu-icon"></i>
              <span class="menu-title">Emitir Documentos</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>secretario/lista_boletim_notas">Boletim de Notas</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>secretario/lista_declaracao_sem_notas">Declaração Sem Notas</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>secretario/lista_declaracao_com_notas">Declaração Com Notas</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>secretario/lista_certificados">Certificados</a></li>
              
              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>secretario/documentos_solicitados">
              <i class="ti ti-folder menu-icon"></i>
              <span class="menu-title">Pedidos Documentos</span>
            </a>
          </li>
          
          <!--li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ti-bar-chart menu-icon"></i>
              <span class="menu-title">Relatórios</span>
            </a>
          </li-->

          <?php } ?>

          <?php

          if($this->session->userdata("pessoa_usertipo") == 3)
          {
          ?>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#turmas" aria-expanded="false" aria-controls="turmas">
              <i class="ti-user menu-icon menu-icon"></i>
              <span class="menu-title"> Estudantes</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="turmas">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link"  href="<?php echo base_url(); ?>professor/turmas">Lista Turmas </a></li>
              </ul>
            </div>
          </li>
          
          <!--li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ti-bar-chart-alt menu-icon"></i>
              <span class="menu-title">Aproveitamento</span>
            </a>
          </li-->

          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>notificacao/listar">
              <i class="ti ti-bell menu-icon"></i>
              <span class="menu-title">Notificações</span>
            </a>
          </li>
          
          <?php

          }

          ?>

<?php
if($this->session->userdata("pessoa_usertipo") == 4)
          {
?>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#academico" aria-expanded="false" aria-controls="academico">
              <i class="ti ti-list menu-icon menu-icon"></i>
              <span class="menu-title">Academico</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="academico">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>aluno/ano_lectivo">Ano Lectivo </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>aluno/horario">Horário Escolar </a></li>

              </ul>
            </div>
          </li>
          
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#avaliacao" aria-expanded="false" aria-controls="avaliacao">
              <i class="ti ti-pencil menu-icon menu-icon"></i>
              <span class="menu-title">Avaliações</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="avaliacao">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>aluno/notas">Notas </a></li>
                <!--li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>aluno/aproveitamento">Aproveitamento</a></li-->

              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>notificacao/listar">
              <i class="ti ti-bell menu-icon"></i>
              <span class="menu-title">Notificações</span>
            </a>
          </li>


          <li class="nav-item" >
            <a class="nav-link" href="<?php echo base_url(); ?>aluno/documentos">
              <i class="ti ti-files menu-icon"></i>
              <span class="menu-title">Documentos</span>
            </a>
          </li>
          
          <?php } ?>


        </ul>
      </nav>
      <!-- partial -->