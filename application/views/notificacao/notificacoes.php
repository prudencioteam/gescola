<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php 

$a_notifgrupos = lista("notify_groups");
$a_pessoas = lista("pessoas");
$a_turmas = lista("turmas");

$meuid = $this->session->userdata("pessoa_id");

get_estudantesByProf($meuid);

?>
      <!-- partial -->
      <div class="main-panel">          
        <div class="content-wrapper">
          <div class="row">

            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Notificações</h4>
                  <p class="card-description">Notificações Recebidas e Enviadas </p>
                    <button type="button" href="<?php echo base_url(); ?>notificacao/listar/recebidas" class="btn btn-outline-primary btn-icon-text btn-sm mybtnlink <?php echo ($type == "in") ? "active" : ""; ?>">
                        Recebidas
                    </button>

                    <?php if(!is_estudante()){ ?>
                    <button type="button" href="<?php echo base_url(); ?>notificacao/listar/enviadas" class="btn btn-outline-success btn-icon-text btn-sm mybtnlink <?php echo ($type == "out") ? "active" : ""; ?>">
                     Enviadas
                    </button>
                    <?php } ?>

                    <div class="row">&nbsp;</div>
                  
                  <!--p class="card-description">Add class <code>.alert .alert-*</code></p-->

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                        <input type="text" class="form-control" name="_search" value="<?php echo ($_POST["_search"]??""); ?>"  placeholder="Procurar Notificações..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div> 

                              

                  <div class="col-md-7">

                        <?php if(!is_estudante()){ ?>
                          <button type="button" href="<?php echo base_url(); ?>notificacao/nova_notificacao" class="btn btn-outline-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                            <i class="ti-plus btn-icon-prepend"></i>                                                    
                            Nova Notificação
                          </button>
                        <?php } ?>

                      </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                      
                      <?php

                      $_pesquisa = "";
                      if(isset($_POST["_search"]))
                      {
                        $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
                        echo $_pesquisa;
                      }

                      ?>

                    </div>
                </div>

                <div class="row">&nbsp;</div>

                  

                  <table class="table table-striped">
                    <thead>
                      <tr>
                      <th scope="col">#</th>
                       <th scope="col">Enviado Por</th>
                        <th scope="col">Titulo</th>
                        <th scope="col" style="max-width: 40%;">Mensagem</th>
                        <th scope="col">Data</th>
                        <th scope="col"><?php echo ($type == "in") ? "De" : "Destino"; ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php

                      foreach ($dados as $key => $value) {
                        $_mensagem = (strlen($value["notif_message"]) > 100) ? substr($value["notif_message"], 0, 100)." ..." : $value["notif_message"];
                        echo '<tr data-href="'.base_url().'notificacao/ver/'.$value["notif_id"].'">
                        <td class="py-1"><img src="'.base_url().'assets/images/faces/user.png" alt="image"/> </td>
                                <td>'.($a_pessoas[$value["notif_criador"]]??"").'</td>
                                <th scope="row">'.$value["notif_titulo"].'</th>
                                <td>'.$_mensagem.'</td>
                                <td>'.date("Y-m-d", strtotime($value["notif_dtCria"])).'</td>';

                                if($type == "in")
                                {
                                  echo '<td>'.($a_pessoas[$value["notif_criador"]]??"").'</td>';
                                }
                                else
                                {
                                  if($value["notif_for"] == "class")
                                  {
                                    echo '<td>Estudantes da Turma '.($a_turmas[$value["notif_turma"]]??"").'</td>';
                                  }
                                  else
                                  {
                                    echo '<td>'.($a_notifgrupos[$value["notif_for"]]??"").'</td>';
                                  }
                                }

                                echo '<td>';
                                if($value["notif_criador"] == $meuid)
                                {
                                  echo '<button type="button" href="'.base_url().'notificacao/delete_notificacao_modal/'.$value["notif_id"].'" class="ajax-popup-link btn btn-danger btn-sm" style="float:right; margin-right:4px;">
                                  <i class="ti-trash btn-icon-prepend"></i></button>';
                                }
                                else
                                {
                                  echo '<button type="button" href="'.base_url().'notificacao/hide_notificacao_modal/'.$value["nvista_id"].'" class="ajax-popup-link btn btn-dark btn-sm" style="float:right; margin-right:4px;">
                                  <i class="ti-eraser btn-icon-prepend"></i></button>';
                                }
                                echo '</td>';
                              
                              echo '</tr>';
                      }

                      ?>

                    </tbody>
                  </table>

                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>


<script>

function notifdestino_change(id)
{
  var destino = $(id).val();
  if((destino == "group"))
  {
    $("#notifonediv").hide();
    $("#notifgroupdiv").show();
  }
  else if((destino == "person"))
  {
    $("#notifgroupdiv").hide();
    $("#notifonediv").show();
  }
  else
  {
    $("#notifgroupdiv").hide();
    $("#notifonediv").hide();
  }
  
}
</script>