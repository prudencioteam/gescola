<?php

$a_alunos = lista("allAlunos");
$a_trimestre = lista("trimestres");
$a_notifGrupos = lista("notify_groups");
$a_pessoas = lista("pessoas");
$meuid = $this->session->userdata("pessoa_id");
$usertipo = $this->session->userdata("pessoa_usertipo");



if($usertipo == 3)//SE É PROFESSOR
{
  $a_alunosMinhasTurmas = get_estudantesByProf($meuid);
  $a_encarregadosMinhasTurmas = get_encarregadosByProf($meuid);

  $a_pessoas = ($a_alunosMinhasTurmas + $a_encarregadosMinhasTurmas);
}

?>

<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Notificação</h4>
                  <form action="<?php echo base_url(); ?>notificacao/add_notificacao" method="post">
                    <div class="form-group">
                        <label for="title">Destinatário</label>
                        <select  id="notifdestino" class="form-control" name="_for" onchange="notifdestino_change(this)" required>
                          <?php
                          if($usertipo == 3) $a_notifGrupos = lista("notify_groups_prof");
                          foreach ($a_notifGrupos as $key => $value) 
                          {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                          }
                          ?>
                        </select>
                    </div>

                    <div class="form-group" style="display: none;" id="notifgroupdiv">
                        <label for="title">Grupo de Usuarios</label>
                        <select class="form-control" name="_group[]" id="notifgroup" multiple="multiple">
                          <?php
                          foreach ($a_pessoas as $key => $value) 
                          {
                            if($key != $meuid) echo '<option value="'.$key.'">'.$value.'</option>';
                          }
                          ?>
                        </select>
                    </div>

                    <div class="form-group" style="display: none;" id="notifonediv">
                        <label for="title">Usuario</label>
                        <select class="form-control" name="_group[]" id="notifone">
                          <?php
                          foreach ($a_pessoas as $key => $value) 
                          {

                            if($key != $meuid) echo '<option value="'.$key.'">'.$value.'</option>';
                          }
                          ?>
                        </select>
                    </div>

                    <div class="form-group">

                      <label class="mt-2" for="title">Assunto</label>
                      <input type="text" class="form-control" name="_title" placeholder="Escreva o título" value="" required>

                      <label class="mt-2" for="message">Mensagem</label>
                      <textarea placeholder="Escreva a mensagem aqui" name="_msg" class="form-control" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Enviar notificação</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
