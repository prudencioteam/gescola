<?php

$a_alunos = lista("allAlunos");
$a_trimestre = lista("trimestres");
$a_notifGrupos = lista("notify_groups");
$a_pessoas = lista("pessoas");
$a_turmas = lista("turmas");

?>

<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Notificação</h4>
                  <form action="<?php echo base_url(); ?>notificacao/add_notificacao_turma" method="post">
                    <div class="form-group">
                        <label for="title">Para Estudantes da turma:</label>
                        <select  id="notifdestino" class="form-control" name="_for">
                          <?php
                          foreach ($a_turmas as $key => $value) 
                          {
                              if($id == $key) echo '<option value="'.$key.'">'.$value.'</option>';
                          }
                          ?>
                        </select>
                    </div>

                    <div class="form-group">

                      <label class="mt-2" for="title">Assunto</label>
                      <input type="text" class="form-control" name="_title" placeholder="Escreva o título" value="" required>

                      <label class="mt-2" for="message">Mensagem</label>
                      <textarea placeholder="Escreva a mensagem aqui" name="_msg" class="form-control" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Enviar notificação</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>


