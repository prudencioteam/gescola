<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$_pessoaId = $this->session->userdata("pessoa_id");
$dadosUsuario = get_pessoaById($_pessoaId);
$a_estadoCivil = lista("estado_civil");
$a_userTipo = lista("tipo_usuario");


?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">



            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                  <form action="<?php echo base_url(); ?>Config/base_save" method="post" enctype="multipart/form-data">
                  

                    <h4 class="card-title">CONFIGURAÇÕES DO SISTEMA</h4>
                    
                    <p class="card-description">
                    Dados da Instituição
                    </p>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Logotipo da Instituição</label>
                          <div class="col-sm-9">
                            <div style="width: auto; max-width: unset; padding: 15px; border: 1px solid #CCC;">
                              <img src="<?php echo base_url(); ?>Files/get_imgLogoInstituicao" width="150px" /><br><br>
                              <input type="file" name="img" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        
                      </div>
                    </div>
                    
                    
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nome da Instituição</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo ($dados["config_nomeEscola"]??""); ?>" name="nomeInstituicao" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sigla (Abreviatura)</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo ($dados["config_siglaEscola"]??""); ?>" autocomplete="off" name="siglaInstituicao" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Endereço</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo ($dados["config_enderecoEscola"]??""); ?>" name="endereco" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Terminal telefónico</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control ntel" value="<?php echo ($dados["config_telEscola"]??""); ?>" autocomplete="off" name="nTel" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Email</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo ($dados["config_emailEscola"]??""); ?>" name="email" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Outro campo</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo ($dados["config_id"]??""); ?>" autocomplete="off" name="bi_emissao" />
                          </div>
                        </div>
                      </div>
                    </div>

                    
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                    <h4 class="card-title">CONFIGURAÇÕES DO SISTEMA</h4>
                    <div class="row">&nbsp;</div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Inicio Contagem / Nº Processo</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control only-numbers" value="<?php echo ($dados["config_nProcessInicio"]??""); ?>" name="nProcess" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Diretor Administrativo</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo ($dados["config_diretorAdministrativo"]??""); ?>" name="dirAdministrativo" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Diretor Pedagógico</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo ($dados["config_diretorPedagogico"]??""); ?>" autocomplete="off" name="dirPedagogico" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">&nbsp;</div>

                    <div class="row">
                      <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                      </div>
                      <div class="col-md-6">
                        
                      </div>
                    </div>


                  </form>



                </div>
              </div>
            </div>



          </div>
        </div>
        <!-- content-wrapper ends -->


        <!-- partial:partials/_footer.html -->
 <?php $this->load->view("footer"); ?>

