<?php $this->load->view("header"); ?>



<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="<?php echo base_url(); ?>assets/images/logo.svg" alt="logo">
              </div>
              
              <h4>Recuperar a conta</h4>
              <h6 class="font-weight-light">Escolha um metodo para recuperar a sua conta.</h6>

              <br>
              <br>

                <ul class="nav flex-column nav-pills">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Enviar email de recuperação</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pergunta de segurança</a>
                    </li>
                </ul>
              
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>



 <?php $this->load->view("footer"); ?>