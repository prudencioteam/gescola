<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$a_turnos = lista("turnos");
$a_turmaExtremos = get_turmaAnoExtremos();


$a_range = array();

foreach (range($a_turmaExtremos["inicio"], $a_turmaExtremos["fim"]) as $key => $value)
{
  $a_range[$value] = $value;
}

?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

                <div class="col-lg-12">
                  <h4 class="font-weight-bold mb-0">Turmas</h4>
                  <div class="row">&nbsp;</div>
                  <div class="row">
                    <div class="col-md-3">
                    <?php
                      echo form_dropdown("_ano", $a_range, $_ano, "class='form-control classtano'")
                    ?>
                    </div>
                  </div>
                  
                </div>
                
                <div class="col-lg-12">&nbsp;</div>
                <div class="col-lg-12">&nbsp;</div>
          
            <?php

            foreach ($dados as $key => $value)
            {

              $arrDiscip = json_decode($value["turmaPessoa_disciplinas"], TRUE);
              $arrDiscip = is_array($arrDiscip) ? $arrDiscip : array("all");

              foreach ($arrDiscip as $k => $v)
              {

              $dadosTurma = get_turmaById($value["turmaPessoa_turma"]);
              $dadosClasse = get_classeById($dadosTurma["turma_classeId"]);
              $dadosCurso = get_cursoById($dadosTurma["turma_curso"]);
              $dadosDisciplina = get_disciplinaById($v);
              
            ?>

            <div class="col-lg-3 grid-margin stretch-card">
              <div class="card">

                <div class="card-body">
                  <h4 class="card-title"> TURMA: <b><?php echo $dadosTurma["turma_nome"]; ?> | Classe: <?php echo $dadosClasse["classe_nome"]; ?></b></h4>
                  <p class="card-description">Curso: <?php echo $dadosCurso["curso_nome"]; ?></p>
                  <p class="card-description">Disciplina: <?php echo $dadosDisciplina["disciplina_nome"]; ?></p>
                  <p class="card-description">Turno: <?php echo isset($a_turnos[$dadosTurma["turma_turno"]]) ? $a_turnos[$dadosTurma["turma_turno"]] : "Undefined" ?></p>

                  <div class="card-description">

                    <a href="<?php echo base_url(); ?>professor/pauta/<?php echo $value["turmaPessoa_turma"]; ?>/<?php echo $dadosDisciplina["disciplina_id"]; ?>" class="btn btn-xs btn-primary btn-icon-text"><i class="ti-pencil-alt btn-icon-prepend"></i>Avaliações</a>
                    <a href="<?php echo base_url(); ?>notificacao/nova_notificacao_turma/<?php echo $value["turmaPessoa_turma"]; ?>" class="ajax-popup-link btn btn-xs btn-warning btn-icon-text"><i class="ti-bell btn-icon-prepend"></i>Enviar Notificação</a>

                  </div>

                  

                </div>
              </div>
            </div>

            <?php
              }

            }

            ?>

            </div>
        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
 <?php $this->load->view("footer"); ?>


<script>

$(document).ready(function(){
  $(".classtano").change(function(){
    var v = $(this).val();
    window.location = '<?php echo base_url(); ?>Professor/turmas/' + v;
  });
});

</script>