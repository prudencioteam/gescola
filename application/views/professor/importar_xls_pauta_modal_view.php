
<div style="position: relative; background: #FFF; padding: 15px 10px; left: unset; width:98%; max-width: 450px; margin: 20px auto; min-height: 180px;">

<h4 class="text-muted">IMPORTAR PAUTA</h4>
<br>
<div class="content">
    
    <form action="<?php echo base_url(); ?>Professor/importar_pauta_xls/<?php echo $turma; ?>/<?php echo $disciplina; ?>/<?php echo $trimestre; ?>" method="post" enctype="multipart/form-data">
        <input onchange="verif_file_xls(this)" type="file" name="pauta" accept=".xls,.xlsx" required /><br>
        <span style="display: none;" id="span-info-xls">Ficheiro Invalido</span>
        <br><br>
        <button class="btn btn-primary" style="float: right;" type="submit" ><i class="fa fa-upload"></i>Importar</button><br>

    </form>

</div>

<div class="row">&nbsp;</div>

</div>
