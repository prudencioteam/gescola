<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$a_classes = lista("classes");
$a_turnos = lista("turnos");

$turmaInfo = get_turmaById($idTurma);
$disciplinaInfo = get_disciplinaById($idDisciplina);

$_pesquisa = "";
if(isset($_POST["_search"]))
{
  $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
}

?>

    <div class="main-panel">
        
        
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Lista de Alunos</h4>
                            <h5>TURMA: <?php echo $turmaInfo["turma_nome"]; ?> | <?php echo $a_classes[$turmaInfo["turma_classeId"]]; ?></h5>
                        </div>


                        <div class="col-md-12">

                            <table class="table table-striped table-bordered">
                                
                                <thead>
                                    <tr>
                                        <th width="60px">Foto</th>
                                        <th>Nome Completo</th>
                                        <th>Idade</th>
                                        <th>Genero</th>
                                        <th>Contacto</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    
                                    <?php

                                    foreach ($dados as $key => $value)
                                    {
                                        echo '<tr>
                                                <td><img src="'.base_url().'Files/get_profilePhoto/'.md5($value["pessoa_id"]).'"</td>
                                                <td>'.$value["pessoa_nome"].'</td>
                                                <td>'.(date("Y") - date("Y", strtotime($value["pessoa_dtNascimento"]))).' anos</td>
                                                <td>'.$value["pessoa_genero"].'</td>
                                                <td>'.$value["pessoa_username"].'</td>
                                            </tr>';
                                    }

                                    ?>

                                </tbody>

                            </table>

                            <div class="row">
                                <div class="col-md-12">&nbsp;</div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    <!-- content-wrapper ends -->

    <!-- partial:partials/_footer.html -->
 <?php $this->load->view("footer"); ?>