<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$a_classes = lista("classes");
$a_turnos = lista("turnos");

$turmaInfo = get_turmaById($idTurma);
$disciplinaInfo = get_disciplinaById($idDisciplina);
$dadosCurso = get_cursoById($turmaInfo["turma_curso"]);

$_pesquisa = "";
if(isset($_POST["_search"]))
{
  $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
}

$a_trimestres = lista("trimestres");

?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Lista de Notas</h4>

                  <select class="strim form-control" style="color: #000;">
                    <?php
                    foreach ($a_trimestres as $key => $value)
                    {
                      $_selecionado = ($key == $trimestre) ? "selected" : "";
                      echo '<option value="'.$key.'" '.$_selecionado.'>'.$value.'</option>';
                    }
                    ?>
                  </select>

                  <br>

                  <p class="card-description">
                    Curso: <b><?php echo $dadosCurso["curso_nome"]; ?></b> <span class="text-info"> <b>Turma:  <?php echo $turmaInfo["turma_nome"]; ?> </b> | <b><?php echo $a_classes[$turmaInfo["turma_classeId"]]; ?> - <?php echo $a_turnos[$turmaInfo["turma_turno"]]; ?></b></span>
                    <br>Disciplina: <b><?php echo $disciplinaInfo["disciplina_nome"]; ?></b>
                  </p>

                  

                <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                        <input type="text" class="form-control" name="_search" value="<?php echo $filtro; ?>" placeholder="Procurar Estudantes.." aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
 
                    <button type="button" class="btn btn-secundary btn-icon-texts btn-sm" data-toggle="dropdown" style="float:right;"><i class="ti-menu-alt"></i></button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item text-muted" href="<?php echo base_url(); ?>Professor/pauta_xls/<?php echo $idTurma; ?>/<?php echo $idDisciplina; ?>/all"><i class="ti-export"></i>  Exportar Todas Notas [Excel]</a>
                            <a class="dropdown-item text-muted" href="<?php echo base_url(); ?>Professor/pauta_pdf/<?php echo $idTurma; ?>/<?php echo $idDisciplina; ?>/all/download"><i class="ti-export"></i>  Exportar Todas Notas [PDF]</a>
                              <a class="dropdown-item ajax-popup-link text-muted" href="<?php echo base_url(); ?>Professor/importar_pauta_xls_modal/<?php echo $idTurma; ?>/<?php echo $idDisciplina; ?>/<?php echo $trimestre; ?>"> <i class="ti-import"></i>  Importar Notas</a>
                              <a class="dropdown-item ajax-popup-link text-muted" href="<?php echo base_url(); ?>Professor/limpar_pauta_modal/<?php echo $idTurma; ?>/<?php echo $idDisciplina; ?>/<?php echo $trimestre; ?>"><i class="ti-trash"></i>  Limpar Tudo  </a>
                            </div>      


                  <button type="button" href="<?php echo base_url(); ?>Professor/pauta_xls/<?php echo $idTurma; ?>/<?php echo $idDisciplina; ?>/<?php echo $trimestre; ?>" class="mybtnlink btn btn-inverse-success btn-icon-texts btn-sm" style="float:right;">
                          <i class="ti-file btn-icon-prepend" style="margin-right: 5px;"></i>                                                    
                          Excel
                  </button>
                  
                        <button type="button" href="<?php echo base_url(); ?>Professor/pauta_pdf/<?php echo $idTurma; ?>/<?php echo $idDisciplina; ?>/<?php echo $trimestre; ?>/download" class="mybtnlink btn btn-inverse-danger btn-icon-text btn-sm" style="float:right;">
                          PDF
                          <i class="ti-file btn-icon-append"></i>                                                                              
                        </button>
                        <button type="button" href="<?php echo base_url(); ?>Professor/pauta_pdf/<?php echo $idTurma; ?>/<?php echo $idDisciplina; ?>/<?php echo $trimestre; ?>" class="mybtnlink btn btn-inverse-primary btn-icon-text btn-sm" style="float:right;">
                          <i class="ti-eye btn-icon-prepend"></i>                                                    
                          Ver
                        </button>

                      </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <?php echo $_pesquisa; ?>
                  </div>
                </div>
              
                  <form action="<?php echo base_url(); ?>Professor/save_pauta" method="post">

                  <input type="hidden" value="<?php echo $idTurma ?>" name="turmaId" />
                  <input type="hidden" value="<?php echo $idDisciplina ?>" name="disciplinaId" />
                  <input type="hidden" value="<?php echo $trimestre ?>" name="trimestre" />

                  <div class="table-responsive">
                    <table id="tabledit " class="table table-striped table-hover">
                      <thead>
                        <tr>
                          <th>
                            #
                          </th>

                          <th>
                            Nº De Processo
                          </th>

                          <th>
                            Nome
                          </th>
                        
                          <th>
                            1º Frequência
                          </th>
                          <th>
                            2º Frequência
                          </th>
                          <th>
                            Avaliações
                          </th>
                          <th>
                            Média
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>
                       
                        <?php

                        $counter = 0;

                        foreach ($dados as $key => $value)
                        {
                          $dadosAvaliacoes = get_alunoAvaliacoes($value["turmaPessoa_turma"], $value["pessoa_id"], $idDisciplina, $trimestre);

                          $notas = array("", "", "", "", "_", "", "", "");
                          $notasId = array("", "", "", "", "", "", "", "");

                          foreach ($dadosAvaliacoes as $k => $v)
                          {
                            $notas[$v["avaliacao_tipo"]] = $v["avaliacao_nota"];
                            $notasId[$v["avaliacao_tipo"]] = $v["avaliacao_id"];
                          }

                          $media = (((float)$notas[1] + (float)$notas[2] + (float)$notas[3])/3);

                          ?>
                            
                            <tr id="line<?php echo $counter; ?>">
                              <td class="py-1">
                                <img src="<?php echo base_url(); ?>assets/images/faces/user.png" alt="image"/>
                              </td>
                              <td>
                              <?php echo $value["pessoa_nProc"]; ?>
                              </td>
                              
                              <td>
                              <input type="hidden" value="<?php echo $value["pessoa_id"] ?>" name="alunoId" />
                            <?php echo $value["pessoa_nome"]; ?>
                              </td>

                              <td>
                              <input type="text" name="1nota_<?php echo $value["pessoa_id"] ?>_<?php echo $notasId[1] ?>" value="<?php echo $notas[1]; ?>" maxlength="5" class="num1 input form-control form-control-sm" style="width:80px;">
                              </td>
                              <td>
                              <input type="text" name="2nota_<?php echo $value["pessoa_id"] ?>_<?php echo $notasId[2] ?>" value="<?php echo $notas[2]; ?>" maxlength="5" class="num2 input form-control form-control-sm" style="width:80px;">
                              </td>
                              <td>
                              <input type="text" name="3nota_<?php echo $value["pessoa_id"] ?>_<?php echo $notasId[3] ?>" class="num3 input form-control form-control-sm" value="<?php echo $notas[3]; ?>" style="width:80px;">
                              </td>
                              <td>
                              <input type="text" name="4nota_<?php echo $value["pessoa_id"] ?>_<?php echo $notasId[4] ?>" class="resultado input form-control form-control-sm" style="width:80px;" value="<?php echo ($notas[4] == '_') ? $media : $notas[4]; ?>">
                              </td>


                            </tr>


                          <?php
                          $counter++;
                        }
                        ?>
                        
                      </tbody>
                    </table>

                  </div>
              
               
                    <div class="row">

                      <div class="col-md-6">
                        <p>&nbsp;</p>
                        <p class="text-muted" style="padding-top:4px;">Total de Resultados (Estudantes): <?php echo $counter; ?></p>
                      </div>
                      
                      <!-- PAGINAÇÃO
                      <div class="col-md-6">
                          <div class="btn-group btn-sm" role="group" aria-label="Basic example" style="float:right;">
                              <button type="button" class="btn btn-outline-secondary">1</button>
                              <button type="button" class="btn btn-outline-secondary">2</button>
                              <button type="button" class="btn btn-outline-secondary">3</button>
                          </div>
                      </div>
                      -->

                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                          <button type="submit" class="btn btn-primary">Guardar Alterações</button>
                        </div>
                    </div>

                    </form>

                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
 <?php $this->load->view("footer"); ?>


 <script>


function verif_file_xls(a)
{
  $("#span-info-xls").hide();
  var d = $(a).val().split(".");
  var extensao = d[d.length -1];

  if((extensao != "xls") && (extensao != "xlsx"))
  {
    $("#span-info-xls").show();
    $(a).val("");
  }

}

$(document).ready(function(){

  $(".strim").change(function(){
    window.location = '<?php echo base_url(); ?>professor/pauta/<?php echo $idTurma; ?>/<?php echo $idDisciplina; ?>/'+$(this).val();
  });

});

 </script>