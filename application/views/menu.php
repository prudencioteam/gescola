 <?php

$_pessoaId = $this->session->userdata("pessoa_id");
$_pessoaNome = $this->session->userdata("pessoa_nome");
$_pessoaNome = getFormattedName($_pessoaNome);
$a_notificacoes = get_notifyForMe();

$countNewNotif = count_unviewedNotifyForMe();

?>

 <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo mr-5" href="<?php echo base_url(); ?>index.php"><img src="<?php echo base_url(); ?>assets/images/logo.svg" class="mr-2" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>index.php"><img src="<?php echo base_url(); ?>assets/images/logo-mini.svg" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="ti-view-list"></span>
        </button>
        <ul class="navbar-nav mr-lg-2">
          <li class="nav-item nav-search d-none d-lg-block">
            <div class="input-group">
              <div class="input-group-prepend hover-cursor" id="navbar-search-icon">
                <span class="input-group-text" id="search">
                  <i class="ti-search"></i>
                </span>
              </div>
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Procurar.." aria-label="search" aria-describedby="search">
            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
       
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="<?php echo base_url(); ?>#" data-toggle="dropdown">
              <i class="ti-bell mx-0"></i>
              <?php
              if($countNewNotif > 0)
              {
                echo '<span class="count mybellsign"></span>';
              }
              ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="notificationDropdown" style="min-width: 300px;">
              <p class="mb-0 font-weight-normal dropdown-header">Notificações</p>
              
              <?php
              
              foreach ($a_notificacoes as $key => $value) 
              {
                if($value["notif_criador"] == $_pessoaId) continue;

                $_mensagem = (strlen($value["notif_message"]) > 30) ? substr($value["notif_message"], 0, 30)."..." : $value["notif_message"];
                $_classNotif = ($value["nvista_id"] > 0) ? 'myitemnotif' : 'myuitemnotif';
                echo '        
                      <a idn="'.$value["notif_id"].'" idp="'.$_pessoaId.'" class="dropdown-item ajax-popup-link '.$_classNotif.'" href="'.base_url().'notificacao/ver/'.$value["notif_id"].'" style="padding: 7px 0px;">
                      <div class="item-thumbnail">
                          <img src="'.base_url().'assets/images/faces/user.png" alt="image" class="profile-pic" style="margin-left:12px">
                      </div>
                      <div class="item-content flex-grow">
                        <h6 class="font-weight-normal">'.$value["notif_titulo"].'
                        </h6>
                        <p class="font-weight-light small-text text-muted mb-0">
                        '.$_mensagem.'
                        </p>
                      </div>
                    </a>';
              }
              ?>

              <p class="small-text mb-0 text-muted" style="margin-top: 7px; text-align: center;">
                    <a href="<?php echo base_url(); ?>notificacao/listar/recebidas">Ver todas notificações</a>
                  </p>
            </div>
          
          </li>
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="<?php echo base_url(); ?>#" data-toggle="dropdown" id="profileDropdown">
              <img id="img-menu-profile-photo" src="<?php echo base_url(); ?>Files/get_profilePhoto/<?php echo md5($_pessoaId); ?>" alt="profile"/>&nbsp;&nbsp;<?php echo $_pessoaNome; ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item" href="<?php echo base_url(); ?>perfil/config">
                <i class="ti-settings text-primary"></i>
                Configurações
              </a>
              <a class="dropdown-item" href="<?php echo base_url(); ?>auth/logout">
                <i class="ti-power-off text-primary"></i>
                Sair
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="ti-view-list"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">