<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Declaração sem Notas</h4>
                  <p class="card-description">Ano Lectivo 2019 </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                        <input type="text" class="form-control" name="_search"  placeholder="Procurar Estudantes.." aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
 
                      <button type="button" href="<?php echo base_url(); ?>Secretario/decsnotas_modal" class="btn btn-inverse-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Nova Declaração
                        </button>

                      </div>
                </div>

                  <div class="table-responsive pt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                           Nº
                          </th>
                          
                          <th>
                           Estudante
                          </th>
                          
                          <th>
                            Data
                          </th>

                          <th>
                            Emitido Por
                          </th>
                       
                          <th>
                            Estado
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            BOL-Nº005
                          </td>
                          <td>
                          ABEDNEGO JOSÉ FIGUEIREDO MESSELE
                          </td>
                          <td>
                         26/10/2019
                          </td>
                          <td>
                          Secretário da Escola
                          </td>

                          <td>
                         Enviado
                          </td>
                        </tr>

                        <tr>
                          <td>
                            BOL-Nº006
                          </td>
                          <td>
                            ISABEL PEDRO PAULO 
                          </td>
                          <td>
                          26/10/2019
                          </td>
                          <td>
                          Secretário da Escola
                          </td>

                          <td>
                         Emitido Somente
                          </td>
                          
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>
