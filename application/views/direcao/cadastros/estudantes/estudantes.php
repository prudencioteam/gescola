<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_cursos = (array("" => "Qualquer curso")+lista("cursos"));
$a_generos = (array("" => "Qualquer genero")+lista("genero"));

?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Lista de Estudantes</h4>
                  <p class="card-description">Ano Lectivo 2019 </p>

                  <div class="row">
                  <div class="col-md-12">
                    <form method="post">
                      <div class="input-group" style="float: left; width: auto !important; margin-right: 10px;">
                        <input type="text" class="form-control" name="_search" value="<?php echo ($_POST["_search"]??""); ?>"  placeholder="Procurar Por Nº Processo / Nome" aria-label="Procurar Estudantes..">
                      </div>
                      <div class="input-group" style="float: left; width: auto !important; margin-right: 10px;">
                        <?php
                        echo form_dropdown("_genero", $a_generos, ($_POST["_genero"]??""), "class='form-control'");
                        ?>
                      </div>
                      <div class="input-group" style="float: left; width: auto !important; margin-right: 10px;">
                        <?php
                        echo form_dropdown("_curso", $a_cursos, ($_POST["_curso"]??""), "class='form-control'");
                        ?>
                      
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>


                  <div class="col-md-5">&nbsp;</div>

                  <div class="col-md-7">
                      <a href="#">
                      <button type="button" href="<?php echo base_url(); ?>direcao/cadastrar_estudantes" class="mybtnlink btn btn-primary btn-icon-text btn-sm" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Novo Estudante
                        </button>
                    </a>

                      </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                      
                      <?php

                      $_pesquisa = "";
                      if(isset($_POST["_search"]))
                      {
                        $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span> &nbsp;&nbsp;&nbsp;<span class="badge badge-info"><i>'.$a_generos[$_POST["_genero"]].'</i></span>&nbsp;&nbsp;&nbsp;<span class="badge badge-info"><i>'.$a_cursos[$_POST["_curso"]].'</i></span></h5>';
                        echo $_pesquisa;
                      }

                      ?>

                    </div>
                </div>


                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                           Nº de Processo
                          </th>
                          
                          <th>
                           Estudante
                          </th>
                        
                         <th>
                           Nascimento
                          </th>
            
                          <th>
                           Morada
                          </th>  
                          
                          <th>
                           Nº Telemóvel
                          </th>
                       
                        </tr>
                      </thead>
                      <tbody>

                      <?php

                      foreach ($dados as $key => $value) {

                        echo'<tr>
                          <td>
                            '.$value['pessoa_nProc'].'
                          </td>
                          <td>
                          '.$value['pessoa_nome'].'
                          </td>
                          <td>
                          '.$value['pessoa_dtNascimento'].'
                          </td>
                          <td>
                          '.$value['pessoa_morada'].'
                          </td>
                          
                          <td>
                          '.$value['pessoa_nTel'].'
                          </td>

                          <td>
                            
                            <button href="'.base_url().'direcao/delete_pessoa_modal/'.$value["pessoa_id"].'/estudantes" type="button" class="ajax-popup-link btn btn-danger btn-sm" style="float:right; margin-right:4px;">
                            <i class="ti-trash btn-icon-prepend"></i></button>

                            <button href="'.base_url().'direcao/editar_estudantes/'.$value["pessoa_id"].'" type="button" class="mybtnlink btn btn-success btn-sm" style="float:right; margin-right:4px;">
                            <i class="ti ti-pencil btn-icon-prepend"></i></button>
        
                            <!-- a href="#">
                            <button type="button" class="btn btn-warning btn-sm" style="float:right; margin-right:4px;">
                            <i class="ti-eye btn-icon-prepend"></i></button-->
                            </a>

                          </td>
                        </tr>';

                      } ?>
                      
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>
