<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_turmas = lista("turmas_curso");

$a_encarregadosH = lista("h_encarregados", "form");
$a_encarregadosM = lista("m_encarregados", "form");
$a_tipoDoc = lista("tipoDoc");

?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

          <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Cadastro de Estudantes</h4>
                
                    <p class="card-description">
                      Dados Pessoais
                    </p>
                    <div class="row">
                      <div class="col-md-6">
                      <form action="<?php echo base_url(); ?>direcao/estudantes_add" method="post">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nome Completo</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="nome"/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Morada</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="morada" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Genero</label>
                          <div class="col-sm-9">
                            
                              <div class="form-group">
                                
                                <div class="col-md-6">
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="radio" class="form-check-input" name="genero" value="M" required>
                                      Masculino
                                    <i class="input-helper"></i></label>
                                  </div>
                                </div>
                                
                                <div class="col-md-6">
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="radio" class="form-check-input" name="genero" value="F" required>
                                      Femenino
                                    <i class="input-helper"></i></label>
                                  </div>
                                </div>
                              </div>

                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nascimento</label>
                          <div class="col-sm-9">
                            <input class="form-control datepicker" autocomplete="off" placeholder="" name="dtnascimento"/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Doc. Identificação</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="tipo_doc">
                                <option value="">Selecione uma opção</option>
                                <?php
                                foreach ($a_tipoDoc as $key => $value)
                                {
                                  echo '<option value="'.$key.'">'.$value.'</option>';
                                }
                                ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <!--<div class="form-group row">
                          <label class="col-sm-3 col-form-label">Data Emissão</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" autocomplete="off" name="bi_emissao" />
                          </div>
                        </div>-->
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nº Documento</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="bi" required />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Data Emissão</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" autocomplete="off" name="bi_emissao" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Local Emissão</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="bi_local" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Data Expiração</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" autocomplete="off" name="bi_expiracao" />
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">E-mail</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" required />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Telemóvel</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control ntel" name="telemovel" required />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nome do Pai</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="pai">
                              <?php
                              foreach ($a_encarregadosH as $key => $value)
                              {
                                echo '<option value="'.$key.'">'.$value.'</option>';
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nome da Mãe</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="mae">
                              <?php
                              foreach ($a_encarregadosM as $key => $value)
                              {
                                echo '<option value="'.$key.'">'.$value.'</option>';
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-6">
                      
                      </div>
                   
                    </div>
                    <p class="card-description">
                    Dados Academicos
                    </p>
                    <div class="row">
                      
                      <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Turma</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="_turma">
                                <?php
                                foreach ($a_turmas as $key => $value)
                                {
                                  echo '<option value="'.$key.'">'.$value.'</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>  
                        </div>

                      <div class="col-md-6">&nbsp;</div>

                    </div>
                      
                      
                        
                       
                    <div class="row" style="float: right;">
                    <button type="submit" class="btn btn-primary mr-2">Cadastrar</button>
                    
                    </form>

                    <a href="<?php echo base_url(); ?>Direcao/lista_estudantes">
                    <button class="btn btn-light" >Cancelar</button>
                    </a>  


                    </div>
                                
                      </div>

                  </div>

                      </div>

                      </form>

                    </div>
                
                </div>
              </div>
            </div>
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

    