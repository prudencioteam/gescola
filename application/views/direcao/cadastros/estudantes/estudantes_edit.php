<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_turmas = lista("turmas_curso");

$a_encarregadosH = lista("h_encarregados", "form");
$a_encarregadosM = lista("m_encarregados", "form");

$a_genero = lista("genero");
$a_tipoDoc = lista("tipoDoc");

?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

          <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edição do Estudante</h4>
                
                    <p class="card-description">
                      Dados Pessoais
                    </p>
                    <div class="row">
                      <div class="col-md-6">
                      <form action="<?php echo base_url(); ?>direcao/pessoa_update/<?php echo $tipo; ?>" method="post">
                        <input type="hidden" name="id" value="<?php echo $dados["pessoa_id"]; ?>" />
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nome Completo</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $dados["pessoa_nome"] ?>" name="nome"/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Morada</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $dados["pessoa_morada"] ?>" name="morada" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Genero</label>
                          <div class="col-sm-9">
                            <?php

                              echo '<div class="form-group">';

                              foreach ($a_genero as $key => $value)
                              {
                                $_checked = ($dados["pessoa_genero"] == $key) ? 'checked="checked"' : "";
                                echo '<div class="col-md-6">
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="genero" value="'.$key.'" '.$_checked.' required>
                                            '.$value.'
                                          <i class="input-helper"></i></label>
                                        </div>
                                      </div>';
                              }

                              echo '</div>';

                            ?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nascimento</label>
                          <div class="col-sm-9">
                            <input class="form-control datepicker" value="<?php echo $dados["pessoa_dtNascimento"] ?>" autocomplete="off" placeholder="" name="dtnascimento"/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Doc. Identificação</label>
                          <div class="col-sm-9">
                              <?php
                              echo form_dropdown("tipo_doc", $a_tipoDoc, $dados["pessoa_docTipo"], "class='form-control'");
                              ?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <!--<div class="form-group row">
                          <label class="col-sm-3 col-form-label">Data Emissão</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" autocomplete="off" name="bi_emissao" />
                          </div>
                        </div>-->
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nº Documento</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $dados["pessoa_numeroDoc"] ?>" name="bi" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Data Emissão</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" value="<?php echo $dados["pessoa_docEmitido"] ?>" autocomplete="off" name="bi_emissao" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Local Emissão</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $dados["pessoa_docLocal"] ?>" name="bi_local" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Data Expiração</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" value="<?php echo $dados["pessoa_docExpiracao"] ?>" autocomplete="off" name="bi_expiracao" />
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">E-mail</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $dados["pessoa_email"] ?>" name="email" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Telemóvel</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control ntel" value="<?php echo $dados["pessoa_nTel"] ?>" name="telemovel" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nome do Pai</label>
                          <div class="col-sm-9">
                            <?php
                              echo form_dropdown("pai", $a_encarregadosH, $dados["pessoa_pai"], "class='form-control'")
                            ?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nome da Mãe</label>
                          <div class="col-sm-9">
                            <?php
                              echo form_dropdown("mae", $a_encarregadosM, $dados["pessoa_mae"], "class='form-control'")
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>

                      
                      
                        
                       
                    <div class="row" style="float: right;">
                    <button type="submit" class="btn btn-primary mr-2">Atualizar</button>
                    
                    </form>

                    <a href="<?php echo base_url(); ?>Direcao/lista_estudantes">
                    <button class="btn btn-light" >Cancelar</button>
                    </a>  


                    </div>
                                
                      </div>

                  </div>

                      </div>

                      </form>

                    </div>
                
                </div>
              </div>
            </div>
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

    