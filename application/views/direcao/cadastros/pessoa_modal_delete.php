<?php

$a_alunos = lista("allAlunos");
$a_trimestre = lista("trimestres");

?>

<div style="position: relative; background: #FFF; padding: 15px 10px; left: unset; width:98%; max-width: 450px; margin: 20px auto; min-height: 180px;">

<div class="content">
    <p>&nbsp;</p>
    <p style="font-size: 100px; color: #f8bb86; text-align:center;"><i class="ti ti-help-alt"></i></p>

    <p>&nbsp;</p>
    
    <h3 style="text-align: center;">Tens a certeza</h3>
    
    <p style="text-align: center;">Tens a certeza que queres remover?</p>

</div>

<div class="col-md-12">&nbsp;</div>


<div class="col-md-12 text-right">

    <button onclick="$.magnificPopup.close()" class="btn btn-primary">Nao</button>
    
    <a href="<?php echo base_url(); ?>direcao/delete_pessoa/<?php echo $_id; ?>/<?php echo $_op; ?>"><button class="btn btn-danger">Sim</button></a>

</div>


</div>

<?php

?>
