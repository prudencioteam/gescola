<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Lista de Encarregados</h4>
                  <p class="card-description">&nbsp; </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                      <input type="text" class="form-control" name="_search" value="<?php echo ($_POST["_search"]??""); ?>"  placeholder="Procurar Encarregados Por Nome / Email" aria-label="Procurar Encarregados..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
 
                      <button type="button" href="<?php echo base_url(); ?>direcao/cadastrar_encarregados" class="btn btn-primary btn-icon-text btn-sm mybtnlink" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Novo Encarregado
                        </button>

                      </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                      
                      <?php

                      $_pesquisa = "";
                      if(isset($_POST["_search"]))
                      {
                        $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
                        echo $_pesquisa;
                      }

                      ?>

                    </div>
                </div>


                  <div class="table-responsive pt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            Nº
                          </th>
                          
                          <th>
                            Nome
                          </th>
                          
                          <th>
                            Email
                          </th>
                       
                          <th>
                            Morada
                          </th>
                       
                          <th>
                            Nº Telefone
                          </th>
                       
                          <th>
                            &nbsp;
                          </th>

                        </tr>
                      </thead>
                      <tbody>
                        
                        <?php

                        $counter = 0;

                        foreach ($dados as $key => $value)
                        {
                          $counter++;

                          echo '<tr>
                                  <td>'.$counter.'</td>
                                  <td>'.$value["pessoa_nome"].'</td>
                                  <td>'.$value["pessoa_username"].'</td>
                                  <td>'.$value["pessoa_morada"].'</td>
                                  <td>'.$value["pessoa_nTel"].'</td>

                                  <td>
                              
                                    <button href="'.base_url().'direcao/delete_pessoa_modal/'.$value["pessoa_id"].'/encarregados" type="button" class="ajax-popup-link btn btn-danger btn-sm" style="float:right; margin-right:4px;">
                                    <i class="ti-trash btn-icon-prepend"></i></button>

                                    <button href="'.base_url().'direcao/editar_encarregados/'.$value["pessoa_id"].'" type="button" class="mybtnlink btn btn-success btn-sm" style="float:right; margin-right:4px;">
                                    <i class="ti ti-pencil btn-icon-prepend"></i></button>

                                  </td>

                                </tr>';
                        }

                        ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        function boletim_search(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_estudante_boletim/'+s,
              success:function(data){
                  console.log(data);
                  var dados = JSON.parse(data);

                  $("#bsearchresult").html("");
                  console.log(dados);
                  var xc = false;
                  for(var key in dados)
                  {
                    xc = true;
                    $("#bsearchresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }

                  if(xc)
                  {
                    $("#bsearchresult").show();
                  }
                  else
                  {
                    $("#bsearchresult").hide();
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function boletim_selectPessoa(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_pturmas_boletim/'+s,
              success:function(data){
                  var dados = JSON.parse(data);
                  $("#bselectresult").html("");
                  for(var key in dados)
                  {
                    $("#bselectresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function submit_boletim_modal()
        {
          var idpessoa = $("#bsearchresult").val();
          var turma = $("#bselectresult").val();
          var trimestre = $("#btrimestre").val();

          window.location = '<?php echo base_url(); ?>Secretario/declaracao_sem_notas/'+idpessoa+'/'+turma+'/'+trimestre;
        }

        </script>