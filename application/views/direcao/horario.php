<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$dadosTurma = get_turmaById($turma);
$dadosClasse = get_classeById($dadosTurma["turma_classeId"]);
$dadosCurso = get_cursoById($dadosTurma["turma_curso"]);

?>

<?php

$_pesquisa = "";
if(isset($_POST["_search"]))
{
  $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
}
?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Lista Nominal de Estudantes</h4>
        
                  <p class="card-description">
                    <b>Curso:</b> <?php echo $dadosCurso["curso_nome"]; ?> <span class="text-info"> <b>Turma: </b>  <?php echo $dadosTurma["turma_nome"]; ?> | <b> Turno: </b> <?php echo $dadosTurma["turma_turno"]; ?></span>
                    <br><b> Classe:</b> <?php echo $dadosClasse["classe_nome"]; ?> 
                  </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                        <input type="text" class="form-control" name="_search"  placeholder="Procurar Estudantes.." aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

              
                  <div class="col-md-7">
 
                        <button type="button" href="<?php echo base_url(); ?>Secretario/listar_alunos_turma_file/<?php echo $turma; ?>/pdf/<?php echo $_filtro; ?>" class="btn btn-danger btn-icon-text btn-sm mybtnlink" style="float:right;margin-left:3px">
                          <i class="ti ti-file btn-icon-prepend"></i>                                                    
                         Exportar PDF
                        </button>

                        <button type="button" href="<?php echo base_url(); ?>Secretario/listar_alunos_turma_file/<?php echo $turma; ?>/xls/<?php echo $_filtro; ?>" class="btn btn-danger btn-icon-text btn-sm mybtnlink" style="float:right;margin-left:3px">
                          <i class="ti ti-file btn-icon-prepend"></i>                                                    
                         Exportar XLS
                        </button>
                        
                        <button type="button" href="<?php echo base_url(); ?>Secretario/boletim_modal" class="btn btn-success btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                          <i class="ti-eye btn-icon-prepend"></i>                                                    
                         Visualizar
                        </button>

                      

                      </div>
                </div>


                <div class="row">
                  <div class="col-md-12">
                    <?php echo $_pesquisa; ?>
                  </div>
                </div>


                  <div class="table-responsive pt-3">
                    <table class="table table-striped table-hover">
                      <thead>
                        <tr>
                        <th>
                      Nº
                          </th>

                          <th>
                           Foto do Estudante
                          </th>

                          <th>
                           Nº de Processo
                          </th>
                          
                          <th>
                           Nome Estudante
                          </th>
                           
                          <th>
                           Data de Nascimento
                          </th>
                          
                          
                        </tr>
                      </thead>
                      <tbody>
                        
                      <?php $count = (($pagina - 1)*$rporPagina); ?>
                      <?php foreach ($dados as $value) { 
                        
                          $count++;
                      ?>
                    <tr>
                    <td><?php echo $count; ?></td>

                        <td class="py-1">
                                <img src="<?php echo base_url(); ?>assets/images/faces/user.png" alt="image"/>
                              </td>
                            
                          <td>
                         <?php echo $value["pessoa_nProc"] ;?>
                          </td>
                          <td>
                          <?php echo $value["pessoa_nome"] ;?>
                          </td>

                          <td>
                          <?php echo $value["pessoa_dtNascimento"] ;?>
                          </td>
                      
                        </tr>
                      <?php } ?>
                  
                      </tbody>
                    </table>
                  </div>
                  
                  <div class="row">&nbsp;</div>
                  <div class="row">&nbsp;</div>

                  <div class="row">

                    <div class="col-md-6">
                      <strong>Total</strong>: <?php echo $dadosTotal; ?>
                    </div>

                    <?php

                    $maxNumberPage = ($dadosTotal/$rporPagina);
                    $maxNumberPage = (($dadosTotal%$rporPagina) > 0) ? ((int)$maxNumberPage + 1) : (int)$maxNumberPage;
                    $maxNumberPage = ($maxNumberPage == 0) ? 1: $maxNumberPage;
                    
                    $arrPaginas = array();

                    for ($i= 1; $i < ($pagina + 5); $i++)
                    { 
                      if($i > $maxNumberPage) continue;
                      array_push($arrPaginas, $i);
                    }

                    ?>

                    <div class="col-md-6">
                      <div class="btn-group float-right" role="group" aria-label="Basic example">
                        <?php

                        if($pagina != 1) echo '<a href="'.base_url().'secretario/listar_alunos_turma/'.$turma.'/1" class="btn btn-outline-secondary">Primeiro</a>';
                        
                        $_cNPages = 0;
                        $constDiff = (($pagina + 2) > $maxNumberPage) ? 4 : 2;
                        foreach ($arrPaginas as $key => $value)
                        {
                          if($value <= ($pagina - $constDiff)) continue;

                          $_cNPages++;
                          if($_cNPages == 6) break;
                          $_url = ($pagina == $value) ? "#" : base_url().'secretario/listar_alunos_turma/'.$turma.'/'.$value;
                          echo '<a href="'.$_url.'" class="btn btn-outline-secondary '.(($pagina == $value) ? "active" : "").'">'.$value.'</a>';
                        }

                        if($pagina != $maxNumberPage) echo '<a href="'.base_url().'secretario/listar_alunos_turma/'.$turma.'/'.$maxNumberPage.'" class="btn btn-outline-secondary">Último</a>';
                        

                        ?>
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>