<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Editar Classes</h4>
                  <form action="<?php echo base_url(); ?>direcao/turnos_update" method="post">

                    <input type="hidden" name="_id" value="<?php echo $_id; ?>" />

                    <div class="form-group">

                      <label class="mt-2" for="title">Nome Turno</label>
                      <input type="text" class="form-control" name="_nome" placeholder="Turno.." value="<?php echo $_dados["turno_nome"]; ?>" required>

                      <label class="mt-2" for="message">Descrição</label>
                      <textarea placeholder="Descrição.." name="_descricao" class="form-control" required><?php echo $_dados["turno_descricao"]; ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Actualizar</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
