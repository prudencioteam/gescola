<?php

$a_alunos = lista("allAlunos");

$a_disciplinas = lista("disciplinas");

$_alunosDaTurma = get_alunosTurma($id);

$dadosTurma = get_turmaById($id);
$turmaDisc = json_decode($dadosTurma["turma_disciplinas"], TRUE);
$turmaDisc = is_array($turmaDisc) ? $turmaDisc : array();

$arr_Alunos = array();

foreach ($_alunosDaTurma as $key => $value)
{
    array_push($arr_Alunos, $value["pessoa_id"]);
}

$disc = json_decode($_turmaPessoa["turmaPessoa_disciplinas"], TRUE);
?>

<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Editar Disciplinas do Estudante</h4>
                  <form action="<?php echo base_url(); ?>direcao/turma_save_edited_aluno" method="post">

                    <input type="hidden" name="_id" value="<?php echo $id; ?>" />
                    <input type="hidden" name="_aluno" value="<?php echo $_aluno; ?>" />
                    <input type="hidden" name="_idTurmaPessoa" value="<?php echo $_turmaPessoa["turmaPessoa_id"]; ?>" />

                    <div class="form-group">

                      <label class="mt-2" for="title">Estudante</label>
                      <select class="form-control" name="_aluno" required="required">
                      <?php
                      echo '<option value="'.$_turmaPessoa["turmaPessoa_pessoa"].'">'.$a_alunos[$_turmaPessoa["turmaPessoa_pessoa"]].'</option>';
                      ?>
                      </select>

                      <label class="mt-2" for="title">Disciplina</label>
                      <select class="form-control" name="_disc[]" multiple="multiple" required="required">
                      <?php

                      $turmaDisc = array_merge(array("all" => "all"), $turmaDisc);
                      $a_disciplinas["all"] = "Todas";
                      foreach ($turmaDisc as $key => $value)
                      {
                        $selected = (in_array($value, $disc)) ? 'selected="selected"' : '';
                        echo '<option value="'.$value.'" '.$selected.'>'.$a_disciplinas[$value].'</option>';
                      }
                      ?>
                      </select>

                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Atualizar</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

