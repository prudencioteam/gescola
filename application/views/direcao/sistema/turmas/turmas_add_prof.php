<?php

$a_professores = lista("allProfessores");

$a_disciplinas = lista("disciplinas");

$_profsDaTurma = get_professoresTurma($id);

$dadosTurma = get_turmaById($id);
$turmaDisc = json_decode($dadosTurma["turma_disciplinas"], TRUE);
$turmaDisc = is_array($turmaDisc) ? $turmaDisc : array();

$arrProfs = array();

foreach ($_profsDaTurma as $key => $value)
{
    array_push($arrProfs, $value["pessoa_id"]);
}


?>

<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Adicionar Professor</h4>
                  <form action="<?php echo base_url(); ?>direcao/turma_add_professor" method="post">

                    <input type="hidden" name="_id" value="<?php echo $id; ?>" />

                    <div class="form-group">

                      <label class="mt-2" for="title">Professor</label>
                      <select class="form-control" name="_prof" required="required">
                      <?php
                      foreach ($a_professores as $key => $value)
                      {
                          if(!in_array($key, $arrProfs)) echo '<option value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Disciplina</label>
                      <select class="form-control" name="_disc[]" multiple="multiple" required="required">
                      <?php
                      foreach ($turmaDisc as $key => $value)
                      {
                        echo '<option value="'.$value.'">'.$a_disciplinas[$value].'</option>';
                      }
                      ?>
                      </select>

                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Adicionar</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

