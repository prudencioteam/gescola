<?php

$a_alunos = lista("allAlunos");

$a_disciplinas = lista("disciplinas");

$_alunosDaTurma = get_alunosTurma($id);

$dadosTurma = get_turmaById($id);
$turmaDisc = json_decode($dadosTurma["turma_disciplinas"], TRUE);
$turmaDisc = is_array($turmaDisc) ? $turmaDisc : array();

$arr_Alunos = array();

foreach ($_alunosDaTurma as $key => $value)
{
    array_push($arr_Alunos, $value["pessoa_id"]);
}


?>

<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Adicionar Estudantes</h4>
                  <form action="<?php echo base_url(); ?>direcao/turma_add_aluno" method="post">

                    <input type="hidden" name="_id" value="<?php echo $id; ?>" />

                    <div class="form-group">

                      <label class="mt-2" for="title">Estudante</label>
                      <select class="form-control" name="_aluno[]" multiple="multiple" required="required">
                      <?php
                      foreach ($a_alunos as $key => $value)
                      {
                          if(!in_array($key, $arr_Alunos)) echo '<option value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Disciplina</label>
                      <select class="form-control" name="_disc[]" multiple="multiple" required="required">
                      <option value="all" selected="selected">Todas</option>
                      <?php
                      foreach ($turmaDisc as $key => $value)
                      {
                        echo '<option value="'.$value.'">'.$a_disciplinas[$value].'</option>';
                      }
                      ?>
                      </select>

                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Adicionar</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

