<?php


$a_cursos = lista("cursos");
$a_classes = lista("classes");
$a_turnos = lista("turnos");
$a_anolectivo = lista("ano_lectivo");
$a_disciplinas = lista("disciplinas");

$turmaDisciplinas = json_decode($_dados["turma_disciplinas"], TRUE);
$turmaDisciplinas = is_array($turmaDisciplinas) ? $turmaDisciplinas : array();

?>


<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Editar Turmas</h4>
                  <form action="<?php echo base_url(); ?>direcao/turmas_update" method="post">

                    <input type="hidden" name="_id" value="<?php echo $_id; ?>" />

                    <div class="form-group">

                      <label class="mt-2" for="title">Nome Turma</label>
                      <input type="text" class="form-control" name="_nome" placeholder="Ano Lectivo.." value="<?php echo $_dados["turma_nome"]; ?>" required>
                      
                      <label class="mt-2" for="title">Turno</label>
                      <select class="form-control" name="_turno">
                      <?php
                      foreach ($a_turnos as $key => $value)
                      {
                        $sl = ($_dados["turma_turno"] == $key) ? 'selected="selected"' : "";
                        echo '<option '.$sl.' value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Curso</label>
                      <select class="form-control" name="_curso">
                      <?php
                      foreach ($a_cursos as $key => $value)
                      {
                        $sl = ($_dados["turma_curso"] == $key) ? 'selected="selected"' : "";
                        echo '<option '.$sl.' value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Classe</label>
                      <select class="form-control" name="_classe">
                      <?php
                      foreach ($a_classes as $key => $value)
                      {
                        $sl = ($_dados["turma_classeId"] == $key) ? 'selected="selected"' : "";
                        echo '<option '.$sl.' value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Ano Lectivo</label>
                      <select class="form-control" name="_ano">
                      <?php
                      foreach ($a_anolectivo as $key => $value)
                      {
                        $sl = ($_dados["turma_ano"] == $value) ? 'selected="selected"' : "";
                        echo '<option '.$sl.' value="'.$value.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Disciplinas</label>
                      <select class="form-control" name="_disciplinas[]" multiple="multiple">
                      <?php
                      foreach ($a_disciplinas as $key => $value)
                      {
                        $_s = in_array($key, $turmaDisciplinas) ? 'selected="selected"' : "";
                        echo '<option value="'.$key.'" '.$_s.'>'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="message">Descrição</label>
                      <textarea placeholder="Descrição.." name="_descricao" class="form-control" required><?php echo $_dados["turma_descricao"]; ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Actualizar</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
