<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");


$a_cursos = lista("cursos");
$a_classes = lista("classes");
$a_turnos = lista("turnos");
$a_disciplinas = lista("disciplinas");


$alunos = get_alunosTurma($_id);
$professores = get_professoresTurma($_id);

?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Professores e estudantes da turma <?php echo $_dados["turma_nome"]; ?></h4>
                  <p class="card-description">&nbsp; </p>

                  <div class="row">

                    <div class="col-md-12">

                      <div class="col-md-6" style="float: left;">
                        <h4>PROFESSORES</h4>
                        <br>
                        <div align="right">
                        <button href="<?php echo base_url(); ?>direcao/turma_add_professor_modal/<?php echo $_id; ?>" class="ajax-popup-link btn btn-primary btn-sm"><i class="ti-plus"></i>&nbsp;Adicionar</button>
                        <button href="<?php echo base_url(); ?>direcao/delete_turmapessoa_modal/<?php echo $_id; ?>/allprof" class="ajax-popup-link btn btn-danger btn-sm "><i class="ti-trash"></i>&nbsp;Remover Tudo</button>
                        </div>
                        <br>
                        <table class="table">
                          <tr>
                            <th>Nome</th>
                            <th>Disciplina</th>
                            <th>&nbsp;</th>
                          </tr>
                          <?php
                          
                          foreach ($professores as $key => $value)
                          {
                            echo '<tr>
                                    <td>'.$value["pessoa_nome"].'</td>
                                    <td>';

                                    $_disc = json_decode($value["turmaPessoa_disciplinas"], TRUE);
                                    $_disc = is_array($_disc) ? $_disc : array();

                                    foreach ($_disc as $k => $v)
                                    {
                                      echo '<span class="badge badge-success">'.($a_disciplinas[$v]??"").'</span>&nbsp;';
                                    }
                                    
                                    echo '</td>
                                    <td>
                                    <button href="'.base_url().'direcao/turma_edit_professor_modal/'.$value["turmaPessoa_id"].'/'.$_id.'" class="ajax-popup-link btn btn-success btn-xs"><i class="ti-pencil"></i></button>
                                    <button href="'.base_url().'direcao/delete_turmapessoa_modal/'.$value["turmaPessoa_id"].'/'.$_id.'" class="ajax-popup-link btn btn-danger btn-xs"><i class="ti-trash"></i></button>
                                    </td>
                                  </tr>';
                          }
                          ?>
                        </table>

                      </div>

                      <div class="col-md-6" style="float: left;">
                        <h4>ESTUDANTES</h4>
                        <br>
                        <div align="right">
                        <button href="<?php echo base_url(); ?>direcao/turma_add_aluno_modal/<?php echo $_id; ?>" class="ajax-popup-link btn btn-primary btn-sm "><i class="ti-plus"></i>&nbsp;Adicionar</button>
                        <button href="<?php echo base_url(); ?>direcao/delete_turmapessoa_modal/<?php echo $_id; ?>/all" class="ajax-popup-link btn btn-danger btn-sm "><i class="ti-trash"></i>&nbsp;Remover Tudo</button>
                        </div>
    
                        <br>
                        <table class="table">
                          <tr>
                            <th>Nome</th>
                            <th>Disciplina</th>
                            <th>&nbsp;</th>
                          </tr>
                          <?php
                          
                          foreach ($alunos as $key => $value)
                          {
                            echo '<tr>
                                    <td>'.$value["pessoa_nome"].'</td>
                                    <td>';

                                    $_disc = json_decode($value["turmaPessoa_disciplinas"], TRUE);
                                    $_disc = is_array($_disc) ? $_disc : array();

                                    if(in_array("all", $_disc))
                                    {
                                      echo '<span class="badge badge-success">Todas</span>&nbsp;';
                                    }
                                    else
                                    {
                                      foreach ($_disc as $k => $v)
                                      {
                                        echo '<span class="badge badge-success">'.($a_disciplinas[$v]??"").'</span>&nbsp;';
                                      }
                                    }
                                    
                                    echo '</td>
                                    <td>
                                    <button href="'.base_url().'direcao/turma_edit_aluno_modal/'.$value["turmaPessoa_id"].'/'.$_id.'" class="ajax-popup-link btn btn-success btn-xs"><i class="ti-pencil"></i></button>
                                    <button href="'.base_url().'direcao/delete_turmapessoa_modal/'.$value["turmaPessoa_id"].'/'.$_id.'" class="ajax-popup-link btn btn-danger btn-xs"><i class="ti-trash"></i></button>
                                    </td>
                                  </tr>';
                          }
                          ?>
                        </table>
                      </div>

                    </div>

                  </div>


                  
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>
