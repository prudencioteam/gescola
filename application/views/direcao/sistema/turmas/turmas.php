<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");


$a_cursos = lista("cursos");
$a_classes = lista("classes");
$a_turnos = lista("turnos");

?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Cadastro de Turmas</h4>
                  <p class="card-description">Ano Lectivo 2019 </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                      <input type="text" class="form-control" name="_search" value="<?php echo ($_POST["_search"]??""); ?>"  placeholder="Procurar Turmas.." aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
 
                      <button type="button" href="<?php echo base_url(); ?>direcao/turmas_modal" class="btn btn-inverse-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Nova Turma
                        </button>

                      </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                      
                      <?php

                      $_pesquisa = "";
                      if(isset($_POST["_search"]))
                      {
                        $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
                        echo $_pesquisa;
                      }

                      ?>

                    </div>
                </div>

                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                           #
                          </th>
                          
                          <th>
                           Nome da Turma
                          </th>
                          
                          <th>
                           Curso
                          </th>
                          
                          <th>
                           Classe
                          </th>
                          
                          <th>
                           Turno
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>
                   <?php

                  $count = 0;

                  foreach ($dados as $key => $value) {
                    $count++;
                    echo'<tr>
                      <td>
                      '.$count.'
                      </td>
                      <td>
                      '.$value["turma_nome"].'
                      </td>

                      <td>
                      '.($a_cursos[$value["turma_curso"]]??"").'
                      </td>

                      <td>
                      '.($a_classes[$value["turma_classeId"]]??"").'
                      </td>

                      <td>
                      '.($a_turnos[$value["turma_turno"]]??"").'
                      </td>

                      <td>
                      
                    <button href="'.base_url().'direcao/turmas_delete_modal/'.$value["turma_id"].'" type="button" class="ajax-popup-link btn btn-danger btn-sm" style="float:right;">
                    <i class="ti-trash btn-icon-prepend"></i></button>

                    <button href="'.base_url().'direcao/turmas_pessoas/'.$value["turma_id"].'" type="button" class="mybtnlink btn btn-primary btn-sm" style="float:right; margin-right:4px;">
                    <i class="ti-settings btn-icon-prepend"></i></button>

                    <button href="'.base_url().'direcao/turmas_edit/'.$value["turma_id"].'" type="button" class="ajax-popup-link btn btn-success btn-sm" style="float:right; margin-right:4px;">
                    <i class="ti ti-pencil btn-icon-prepend"></i></button>


                      </td>
                    </tr>';

                  } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>
