<?php

$a_alunos = lista("allAlunos");
$a_trimestre = lista("trimestres");
$a_notifGrupos = lista("notify_groups");
$a_pessoas = lista("pessoas");

$a_cursos = lista("cursos");
$a_classes = lista("classes");
$a_turnos = lista("turnos");
$a_anolectivo = lista("ano_lectivo");
$a_disciplinas = lista("disciplinas");

?>

<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Cadastro de Turmas</h4>
                  <form action="<?php echo base_url(); ?>direcao/turmas_add" method="post">

                    <div class="form-group">

                      <label class="mt-2" for="title">Nome Turma</label>
                      <input type="text" class="form-control" name="_nome" placeholder="Turma.." required>

                      <label class="mt-2" for="title">Turno</label>
                      <select class="form-control" name="_turno">
                      <?php
                      foreach ($a_turnos as $key => $value)
                      {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Curso</label>
                      <select class="form-control" name="_curso">
                      <?php
                      foreach ($a_cursos as $key => $value)
                      {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Classe</label>
                      <select class="form-control" name="_classe">
                      <?php
                      foreach ($a_classes as $key => $value)
                      {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Ano Lectivo</label>
                      <select class="form-control" name="_ano">
                      <?php
                      foreach ($a_anolectivo as $key => $value)
                      {
                        echo '<option value="'.$value.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="title">Disciplinas</label>
                      <select class="form-control" name="_disciplinas[]" multiple="multiple">
                      <?php
                      foreach ($a_disciplinas as $key => $value)
                      {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                      }
                      ?>
                      </select>

                      <label class="mt-2" for="message">Descrição</label>
                      <textarea placeholder="Descrição.." name="_descricao" class="form-control" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Cadastrar</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

