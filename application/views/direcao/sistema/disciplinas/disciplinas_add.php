<?php

$a_alunos = lista("allAlunos");
$a_trimestre = lista("trimestres");
$a_notifGrupos = lista("notify_groups");
$a_pessoas = lista("pessoas");

?>

<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Cadastro de Disciplinas</h4>
                  <form action="<?php echo base_url(); ?>direcao/disciplinas_add" method="post">

                    <div class="form-group">

                      <label class="mt-2" for="title">Nome Disciplina</label>
                      <input type="text" class="form-control" name="_nome" placeholder="Disciplina.." required>

                      <label class="mt-2" for="message">Descrição</label>
                      <textarea placeholder="Descrição.." name="_descricao" class="form-control" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Cadastrar</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

