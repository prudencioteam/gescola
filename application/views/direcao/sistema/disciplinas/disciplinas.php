<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Cadastro de Disciplinas</h4>
                  <p class="card-description">Ano Lectivo 2019 </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                      <input type="text" class="form-control" name="_search" value="<?php echo ($_POST["_search"]??""); ?>"  placeholder="Procurar Disciplinas.." aria-label="Procurar Disciplinas..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
 
                      <button type="button" href="<?php echo base_url(); ?>direcao/disciplinas_modal" class="btn btn-inverse-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Nova Disciplina
                        </button>

                      </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                      
                      <?php

                      $_pesquisa = "";
                      if(isset($_POST["_search"]))
                      {
                        $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
                        echo $_pesquisa;
                      }

                      ?>

                    </div>
                </div>

                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                           #
                          </th>
                          
                          <th>
                           Disciplina
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>
                   <?php

                  $count = 0;

                  foreach ($dados as $key => $value) {
                    $count++;
                    echo'<tr>
                      <td>
                      '.$count.'
                      </td>
                      <td>
                      '.$value["disciplina_nome"].'
                      </td>

                      <td>
                      
                    <button href="'.base_url().'direcao/disciplinas_delete_modal/'.$value["disciplina_id"].'" type="button" class="ajax-popup-link btn btn-danger btn-sm" style="float:right;">
                    <i class="ti-trash btn-icon-prepend"></i></button>

                    <button href="'.base_url().'direcao/disciplinas_edit/'.$value["disciplina_id"].'" type="button" class="ajax-popup-link btn btn-success btn-sm" style="float:right; margin-right:4px;">
                    <i class="ti ti-pencil btn-icon-prepend"></i></button>


                      </td>
                    </tr>';

                  } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>
