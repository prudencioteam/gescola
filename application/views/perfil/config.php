<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$_pessoaId = $this->session->userdata("pessoa_id");
$dadosUsuario = get_pessoaById($_pessoaId);
$a_estadoCivil = lista("estado_civil");
$a_userTipo = lista("tipo_usuario");

?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

          <div class="col-lg-3 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">FOTO DE PERFIL</h4>
                    <img id="img-pp-preview" src="<?php echo base_url(); ?>Files/get_profilePhoto/<?php echo md5($dadosUsuario["pessoa_id"]); ?>" class="card-img-top" alt="..."><br/><br/>
                    <div class="card-description">
                        <form id="formuploadpphoto" action="<?php echo base_url(); ?>Perfil/save_photo" method="post" enctype="multipart/form-data">
                            <input type="file" style="display: none;" name="_img" onchange="salvar_foto()" id="profilephotoinput" />
                            <input type="hidden" value="<?php echo $dadosUsuario["pessoa_id"]; ?>" name="idPessoa" />
                        </form>

                        <button id="btn-carregar-foto" class="btn btn-xs btn-primary btn-icon-text"><i class="ti-pencil-alt btn-icon-prepend"></i>Alterar</button>

                        <button class="btn btn-xs btn-danger btn-icon-text"><i class="ti-trash btn-icon-prepend"></i>Eliminar</button>
                    </div>
                </div>
              </div>
            </div>

            <div class="col-lg-4 grid-margin stretch-card">
              <div class="card">

                <div class="card-body">
                  <h4 class="card-title">INICIO DE SESSÃO</h4>
                  <p class="card-description">Tipo de Utilizador: <?php echo isset($a_userTipo[$dadosUsuario["pessoa_usertipo"]]) ? $a_userTipo[$dadosUsuario["pessoa_usertipo"]] : "<i>Undefined</i>"; ?></p>
                  
                  <p class="card-description">Utilizador: <?php echo ($dadosUsuario["pessoa_username"] != "") ? $dadosUsuario["pessoa_username"] : $dadosUsuario["pessoa_nProc"]; ?></p>
                  
                  <p class="card-description">Senha: <a class="btn btn-xs btn-link" data-toggle="modal" data-target="#alterarsenhaModal" href="<?php echo base_url(); ?>">Alterar senha</a></p>
                  
                  <p class="card-description">Autenticação via Google: <a class="btn btn-xs btn-link" href="<?php echo base_url(); ?>">Configurar</a></p>

                </div>
              </div>
            </div>



            <div class="col-lg-5 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">DADOS PESSOAIS</h4>
                  <p class="card-description">Nome: <?php echo $dadosUsuario["pessoa_nome"]; ?></p>
                  <p class="card-description">Nº BI: <?php echo $dadosUsuario["pessoa_numeroDoc"]; ?></p>
                  <p class="card-description">Morada: <?php echo $dadosUsuario["pessoa_morada"]; ?></p>
                  <p class="card-description">Data de Nascimento: <?php echo $dadosUsuario["pessoa_dtNascimento"]; ?></p>
                  <p class="card-description">Nº Telefone: <?php echo $dadosUsuario["pessoa_nTel"]; ?></p>
                  <p class="card-description">Estado Civil: <?php echo isset($a_estadoCivil[$dadosUsuario["pessoa_estadoCivil"]]) ? $a_estadoCivil[$dadosUsuario["pessoa_estadoCivil"]] : "--"; ?></p>
                  <p class="card-description">Nacionalidade: <?php echo $dadosUsuario["pessoa_nacionalidade"]; ?></p>
                  <p class="card-description">Naturalidade: <?php echo $dadosUsuario["pessoa_naturalidade"]; ?></p>

                </div>
              </div>
            </div>


            <!--div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Configurações</h4>
                  <p class="card-description">
                    
                  </p>
            

                </div>
              </div>
            </div-->

          </div>
        </div>
        <!-- content-wrapper ends -->

          <!-- Modal -->
          <div class="modal fade" id="alterarsenhaModal" tabindex="-1" role="dialog" aria-labelledby="alterarsenhaModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="alterarsenhaModalLabel">Alterar Senha</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  

                  <form id="form-alterar-senha" action="<?php echo base_url(); ?>Perfil/change_password" method="post">
                    <div class="form-group">
                      <label for="exampleInputUsername1">Senha atual</label>
                      <input type="password" class="form-control" id="as-atual" name="_atual" placeholder="Introduza a senha atual">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nova senha</label>
                      <input type="password" class="form-control" id="as-nova" name="_nova" placeholder="Introduza a nova senha">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Confirmar</label>
                      <input type="password" class="form-control" id="as-confirmar" name="_confirmar" placeholder="Confirme a nova senha">
                    </div>
                    
                    <button type="button" class="btn btn-primary mr-2 btnalterarsenha">Alterar senha</button>
                    <button class="btn btn-light" data-dismiss="modal">Cancel</button>
                  </form>
                  
                </div>
              </div>
            </div>
          </div>

        <!-- partial:partials/_footer.html -->
 <?php $this->load->view("footer"); ?>


 <script>

    function salvar_foto()
    {
        var myimgform = $("#formuploadpphoto");
        var formData = new FormData(document.getElementById("formuploadpphoto"));

        console.log(formData);

        $.ajax({
            type:'POST',
            url: myimgform.attr('action'),
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                //console.log(data);
                var dados = JSON.parse(data);
                if(dados['error'] == 0)
                {
                    $("#img-pp-preview").attr("src","<?php echo base_url(); ?>Files/get_profilePhoto/" + dados['img']);
                    menu_refreshProfilePhoto();
                }
            },
            error: function(data)
            {
                alert("Ocorreu um erro\nPor favor tente novamente");
                //console.log(data);
            }
        });
    }

    $(document).ready(function(){

        $("#btn-carregar-foto").click(function(){
            $("#profilephotoinput").trigger("click");
        });

        $(".btnalterarsenha").click(function(){
          
          var myform = $("#form-alterar-senha");
          var formData = new FormData(document.getElementById("form-alterar-senha"));

          $.ajax({
              type:'POST',
              url: myform.attr('action'),
              data: formData,
              cache:false,
              contentType: false,
              processData: false,
              success:function(data){
                  console.log(data);
                  var dados = JSON.parse(data);
                  if(dados['error'] == 0)
                  {
                    alert("Senha alterada");
                  }
                  else
                  {
                    alert("Não foi alterada");
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
          
        });
    });

 </script>