<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$_pessoaId = $this->session->userdata("pessoa_id");
$dadosUsuario = get_pessoaById($_pessoaId);
$a_estadoCivil = lista("estado_civil");
$a_userTipo = lista("tipo_usuario");

?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">


            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">

                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                

                <div class="card-body">

                    <div class="col-md-5 offset-3">

                        <?php

                        if($error != '')
                        {
                            $msgError = '';

                            switch($error)
                            {
                                case 1:
                                    $msgError = 'A palavra-passe antiga está incorreta';
                                    $color = 'danger';
                                    $titulo = 'Ocorreu um erro';
                                    break;
                                case 2:
                                    $msgError = 'A senha nova são diferentes';
                                    $color = 'danger';
                                    $titulo = 'Ocorreu um erro';
                                    break;
                                case 'info':
                                    $msgError = 'Por razões de segurança, deve alterar a sua password';
                                    $color = 'primary';
                                    $titulo = 'Mude a sua password';
                                    break;
                                default:
                                    $msgError = 'Por razões de segurança, deve alterar a sua password';
                                    $color = 'primary';
                                    $titulo = 'Mude a sua password';
                            }
                            echo '<blockquote class="blockquote blockquote-'.$color.'">
                                    <header class="text-'.$color.'"><b>'.$titulo.': </b></header>
                                    <p><br>'.$msgError.'</p>
                                    
                                </blockquote>';
                        }

                        ?>
                    

                        <form id="form-alterar-senha" action="<?php echo base_url(); ?>Perfil/change_mypassword" method="post">
                            <div class="form-group">
                            <label for="exampleInputUsername1">Senha atual</label>
                            <input type="password" class="form-control" id="as-atual" name="_atual" placeholder="Introduza a senha atual">
                            </div>
                            <div class="form-group">
                            <label for="exampleInputEmail1">Nova senha</label>
                            <input type="password" class="form-control" id="as-nova" name="_nova" placeholder="Introduza a nova senha">
                            </div>
                            <div class="form-group">
                            <label for="exampleInputPassword1">Confirmar</label>
                            <input type="password" class="form-control" id="as-confirmar" name="_confirmar" placeholder="Confirme a nova senha">
                            </div>
                            
                            <button type="submit" class="btn btn-primary mr-2">Alterar senha</button>
                        </form>

                    </div>
                    
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                    


                </div>
              </div>
            </div>



            

            <!--div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Configurações</h4>
                  <p class="card-description">
                    
                  </p>
            

                </div>
              </div>
            </div-->

          </div>
        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
 <?php $this->load->view("footer"); ?>