<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$meuid = $this->session->userdata("pessoa_id");
$minhasTurmas = lista("turma_estudante", $meuid);
$a_trimestres = lista("trimestres");
$a_disciplinas = lista("disciplinas");
$a_turmas = lista("turmas");

?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Lista de Notas</h4>
                  
                  <?php

                    foreach ($a_trimestres as $key => $value)
                    {
                      echo '<button href="'.base_url().'aluno/notas/'.$key.'" class="mybtnlink btn btn-primary btn-xs">'.$value.'</button>&nbsp;';
                    }
                  ?>
                  <br><br>
                  <p class="card-description"><?php echo strtoupper($a_trimestres[$trimestre]??"")."  &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; "; ?>Ano Lectivo 2019 </p>

                  <div class="row">
                    <!--
                    <div class="col-md-5">
                      <form method="post">
                        <div class="input-group">
                          <input type="text" class="form-control" name="_search"  placeholder="Procurar Disicplina.." aria-label="Procurar Estudantes..">
                          <div class="input-group-append">
                            <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                          </div>
                        </div>
                      </form>
                    </div>
                    -->

                  <div class="col-md-7">
                      <!--
                      <button type="button" href="<?php echo base_url(); ?>Secretario/decsnotas_modal" class="btn btn-inverse-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Nova Declaração
                        </button>
                      -->
                      </div>
                </div>

                  <div class="table-responsive pt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                          Disciplina
                          </th>
                          
                          <th>
                           1 PF
                          </th>
                          
                          <th>
                            2PF
                          </th>

                          <th>
                           AC
                          </th>
                       
                          <th>
                           Nota
                          </th>

                        </tr>
                      </thead>
                      <tbody>

                        <?php

                          $arrNotas = array();

                          foreach ($minhasTurmas as $MTkey => $MTvalue)
                          {
                            $dadosTurma = get_turmaById($MTkey);
                            $__disciplinas = json_decode($dadosTurma["turma_disciplinas"], TRUE);
                            
                            foreach ($__disciplinas as $Dkey => $Dvalue)
                            {
                              $dadosAvaliacoes = get_alunoAvaliacoes($dadosTurma["turma_id"], $meuid, $Dvalue, $trimestre);

                              $notas = array("", "", "", "", "", "", "", "");
                              $notasId = array("", "", "", "", "", "", "", "");

                              foreach ($dadosAvaliacoes as $k => $v)
                              {
                                $notas[$v["avaliacao_tipo"]] = $v["avaliacao_nota"];
                                $notasId[$v["avaliacao_tipo"]] = $v["avaliacao_id"];
                              }

                              $media = (((float)$notas[1] + (float)$notas[2] + (float)$notas[3])/3);
                              array_push($arrNotas, array("disciplina" => $Dvalue,
                                                          "turma" => $dadosTurma["turma_id"],
                                                          "nota1" => $notas[1],
                                                          "nota2" => $notas[2],
                                                          "nota3" => $notas[3],
                                                          "media" => $media));
                            }
                          }


                          foreach ($arrNotas as $key => $value)
                          {
                            echo '<tr>
                                    <td>'.($a_disciplinas[$value["disciplina"]]??"").'&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge badge-primary">'.($a_turmas[$value["turma"]]??"").'</span></td>
                                    <td>'.$value["nota1"].'</td>
                                    <td>'.$value["nota2"].'</td>
                                    <td>'.$value["nota3"].'</td>
                                    <td>'.number_format($value["media"], 2, ",",".").'</td>
                                  </tr>';
                          }
                        ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>