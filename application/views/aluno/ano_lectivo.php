<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php 
$meuid = $this->session->userdata("pessoa_id");
$dados = dados_academicos($meuid);
$a_userTipo = lista("tipo_usuario");

?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

     <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Informações Academicas</h4>
                
               <?php

               foreach ($dados as $value) {  
                echo'
                  <div class="row">
                    <div class="col-md-6">
                      <div id="dragula-event-left" class="py-2">
                        <div class="card rounded border mb-2">
                          <div class="card-body p-3">
                            <div class="media">
                              <i class="ti ti-pin-alt icon-sm text-primary align-self-center mr-3"></i>
                              <div class="media-body">
                                <h6 class="mb-1">Nome</h6>
                                <p class="mb-0 text-muted">
                                '.$value["pessoa_nome"].'                           
                                </p>
                              </div>                              
                            </div> 
                          </div>
                        </div>
                        <div class="card rounded border mb-2">
                          <div class="card-body p-3">
                            <div class="media">
                              <i class="ti ti-pin-alt icon-sm text-primary align-self-center mr-3"></i>
                              <div class="media-body">
                                <h6 class="mb-1">Tipo de Usuário</h6>
                                <p class="mb-0 text-muted">
                                '.$a_userTipo[$value["pessoa_usertipo"]].'                      
                                </p>
                              </div>                              
                            </div> 
                          </div>
                        </div>
                        <div class="card rounded border mb-2">
                          <div class="card-body p-3">
                            <div class="media">
                              <i class="ti ti-pin-alt icon-sm text-primary align-self-center mr-3"></i>
                              <div class="media-body">
                                <h6 class="mb-1">Nº de Processo</h6>
                                <p class="mb-0 text-muted">
                                '.$value["pessoa_nProc"].'                        
                                </p>
                              </div>                              
                            </div> 
                          </div>
                        </div>
                        <div class="card rounded border mb-2">
                          <div class="card-body p-3">
                            <div class="media">
                              <i class="ti ti-pin-alt icon-sm text-primary align-self-center mr-3"></i>
                              <div class="media-body">
                                <h6 class="mb-1">Curso</h6>
                                <p class="mb-0 text-muted">
                                '.$value["curso_nome"].'                                 
                                </p>
                              </div>                              
                            </div> 
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div id="dragula-event-right" class="py-2">
                        <div class="card rounded border mb-2">
                          <div class="card-body p-3">
                            <div class="media">
                              <i class="ti ti-time icon-sm text-success align-self-center mr-3"></i>
                              <div class="media-body">
                                <h6 class="mb-1">Turma</h6>
                                <p class="mb-0 text-muted">
                                '.$value["turma_nome"].'                        
                                </p>
                              </div>                              
                            </div> 
                          </div>
                        </div>
                        <div class="card rounded border mb-2">
                          <div class="card-body p-3">
                            <div class="media">
                              <i class="ti ti-time icon-sm text-success align-self-center mr-3"></i>
                              <div class="media-body">
                                <h6 class="mb-1">Classe</h6>
                                <p class="mb-0 text-muted">
                                '.$value["classe_nome"].'                           
                                </p>
                              </div>                              
                            </div> 
                          </div>
                        </div>
                        <div class="card rounded border mb-2">
                          <div class="card-body p-3">
                            <div class="media">
                              <i class="ti ti-time icon-sm text-success align-self-center mr-3"></i>
                              <div class="media-body">
                                <h6 class="mb-1">Turno</h6>
                                <p class="mb-0 text-muted">
                                '.$value["turno_nome"].'               
                                </p>
                              </div>                              
                            </div> 
                          </div>
                        </div>
                        <div class="card rounded border mb-2">
                          <div class="card-body p-3">
                            <div class="media">
                              <i class="ti ti-time icon-sm text-success align-self-center mr-3"></i>
                              <div class="media-body">
                                <h6 class="mb-1">Ano Lectivo</h6>
                                <p class="mb-0 text-muted">
                                '.date('Y').'                          
                                </p>
                              </div>                              
                            </div> 
                          </div>
                        </div>';
                       }
                        ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

