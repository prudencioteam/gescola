<?php

$a_alunos = lista("allAlunos");
$a_trimestre = lista("trimestres");
$a_notifGrupos = lista("notify_groups");
$a_pessoas = lista("pessoas");

?>

<div style="position: relative; padding: 20px; left: unset; width:98%; max-width: 600px; margin: 20px auto; min-height: 180px; background-color: #FFF;">
              <div>
                <div>
                  <h4 class="card-title">Pedido de Documentos</h4>
                  <form action="<?php echo base_url(); ?>aluno/documento_pedido_add" method="post">

                    <div class="form-group">

                      <label class="mt-2" for="title">Tipo Documento</label>
                      
                      <select class="form-control" name="_tipodoc" require>
                      <option value=""></option>
                        <?php

                        $docs = lista("docs_pedido");

                        foreach ($docs as $key => $value)
                        {
                          echo '<option value="'.$key.'">'.$value.'</option>';
                        }
                        ?>
                        </select>

                      <label class="mt-2" for="title">Efeito</label>
                      <select class="form-control" name="_efeito" require>
                      <option value=""></option>
                      <option value="Bilhete de Identidade">Bilhete de Identidade</option>
                      <option value="Passaporte">Passaporte</option>
                      <option value="Bolsa de Estudo">Bolsa de Estudo</option>
                      <option value="Bolsa de Estudo">Outro</option>
                      </select>

                      <label class="mt-2" for="message">Observação</label>
                      <textarea placeholder="Descrição.." name="_descricao" class="form-control" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Enviar Pedido</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

