<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$meuid = $this->session->userdata("pessoa_id");
$a_minhasTurmas = lista("turma_estudante", $meuid);

$a_classes = lista("classes");
$a_cursos = lista("cursos");
$a_disciplinas = lista("disciplinas");

?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                <?php
                
                $_escreveu = FALSE;

                  foreach ($a_minhasTurmas as $tkey => $tvalue)
                  {

                    $dadosTurma = get_turmaById($tkey);
                    $dadosHorario = get_horarioByTurma($dadosTurma["turma_id"]);
                    $hmodelo = get_horarioModeloById($dadosHorario["hr_modelo"]);

                    if(empty($dadosHorario))
                    {
                      continue;
                    }

                    $_escreveu = TRUE;
                    
                  ?>

                  <div class="row">
                    <!-- col-md-6 -->
                    
                    <div class="col-md-12">
                      <center>
                        <img src="<?php echo base_url(); ?>assets/images/insignia.png" width="70px" heigth="40px" /><br/><br/>
                        <h5 class="subtitle mb5"><?php echo mb_strtoupper(get_nomeInstituicao(), "utf8"); ?></h5>
                        <h5 class="subtitle mb5">HORÁRIO DA TURMA <?php echo $tvalue; ?> - <?php echo mb_strtoupper(($a_classes[$dadosTurma["turma_classeId"]]??""), "utf8"); ?></h5>
                        <h5 class="subtitle mb5">CURSO: <?php echo mb_strtoupper(($a_cursos[$dadosTurma["turma_curso"]]??""), "utf8"); ?> </h5>
                        <h5 class="subtitle mb5" align="right" style="margin-top:-20px">Ano Lectivo: <?php echo $dadosTurma["turma_ano"]; ?> </h5>
                      </center>
                      <div class="table-responsive">
                        <table class="table table-bordered mb30" style="text-align: center;">
                          <thead>
                            <tr>
                              <th style="color:#FFF; background-color:#339966;">Tempo</th>
                              <th style="color:#FFF; background-color:#339966;">Segunda-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                              <th style="color:#FFF; background-color:#339966;">Terça-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                              <th style="color:#FFF; background-color:#339966;">Quarta-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                              <th style="color:#FFF; background-color:#339966;">Quinta-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                              <th style="color:#FFF; background-color:#339966;">Sexta-Feira</th>
                              <th style="color:#FFF; background-color:#339966;">Sala</th>
                            </tr>
                          </thead>
                          <tbody>
                            
                            <?php

                            $a_modelo = json_decode($hmodelo["hmod_body"], TRUE);
                            $a_dados = json_decode($dadosHorario["hr_body"], TRUE);

                            foreach ($a_modelo as $key => $value)
                            {
                              echo '<tr>
                                      <td style="color:#FFF; background-color:#339966;">'.$value["inicio"].' - '.$value["fim"].'</td>
                                      <td>'.($a_disciplinas[$a_dados["seg_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["seg_sala"][$key].'</td>
                                      <td>'.($a_disciplinas[$a_dados["ter_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["ter_sala"][$key].'</td>
                                      <td>'.($a_disciplinas[$a_dados["qua_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["qua_sala"][$key].'</td>
                                      <td>'.($a_disciplinas[$a_dados["qui_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["qui_sala"][$key].'</td>
                                      <td>'.($a_disciplinas[$a_dados["sex_disciplina"][$key]]??"").'</td>
                                      <td>'.$a_dados["sex_sala"][$key].'</td>
                                    </tr>';
                            }
                            ?>

                          </tbody>
                      </table>
                      </div><!-- table-responsive -->
                    </div><!-- col-md-6 -->
                    
                  </div><!-- row -->

                  <div class="row">&nbsp;</div>

                  <div class="row">
                    <div class="col-md-12">
                      <button type="button" href="<?php echo base_url(); ?>aluno/horario_pdf/<?php echo $tkey; ?>/download" class="mybtnlink btn btn-danger btn-icon-text btn-sm">
                        <i class="ti-file btn-icon"></i>
                        &nbsp;PDF                                                                              
                      </button>
                    </div>
                  </div>

        <?php
        }

        if(!$_escreveu)
        {
          echo '
                <div class="row"><div class="col-md-12"><h4>HORÁRIO ESCOLAR</h4></div></div>
                <div class="row">&nbsp;</div>

                <div class="alert alert-info" role="alert">
                  <i class="ti ti-info-alt"></i>&nbsp;&nbsp;O <strong>Horário escolar</strong> não está disponível, por favor contacte a secretaria da instituição
                </div>';
        }
        ?>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>