<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$docs = lista("documentos");
$estado = lista("estado");
?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Documentos</h4>
                  <p class="card-description">Ano Lectivo 2019 </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                        <input type="text" class="form-control" name="_search"  placeholder="Procurar Documentos.." aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">

                         
                      <button type="button" href="<?php echo base_url(); ?>aluno/pedido_documento" class="btn btn-inverse-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                        Fazer Pedido
                        </button>

                      </div>
                </div>

                  <div class="table-responsive pt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                          Tipo Documento
                          </th>
                          
                          <th>
                           Efeito
                          </th>
                          
                          <th>
                            Data de Pedido
                          </th>

                          <th>
                            Estado
                          </th>
                       
                        </tr>
                      </thead>

                      
                      <tbody>
                     <?php foreach ($dados as $value) {       
                        echo'
                          <tr>
                          <td>
                          '.$docs[$value["tipo_documento"]].'
                          </td> 
                          <td>
                          '.$value["efeito_documento"].'
                          </td>
                          <td>
                         '.date("d-m-Y", strtotime($value["data_pedido_documento"])).'
                          </td>
                          <td>
                          '.$estado[$value["estado_documento"]].'
                          </td>
                        <td>';
                        ?>
                        
                        <?php if ($value["estado_documento"] == 3){
                        echo'<a href="'.base_url().'secretario/'?><?php if($value["tipo_documento"] == 2){echo 'declaracao_sem_notas';}elseif($value["tipo_documento"] == 3){echo 'declaracao_com_notas';}elseif($value["tipo_documento"] ==4) {echo 'certificado'; } ?><?php echo'/'.$value["pedido_por"].'/'.$value["id_documento"].'/download">
                        <button type="button" class="btn btn-success btn-sm mybtnlink" style="float:right; margin-right:4px;">
                        <i class="ti-import btn-icon-prepend"></i></button>
                        </a>';
                        }
                        ?>

                        <?php echo'</td> </tr>'; } ?>
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

 