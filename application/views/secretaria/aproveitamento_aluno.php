<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Aproveitamento de Alunos</h4>
                  <p class="card-description">Ano Lectivo 2019 </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                        <input type="text" class="form-control" name="_search"  placeholder="Procurar Estudantes.." aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
 
                      <button type="button" href="" class="btn btn-inverse-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Novo Boletim
                        </button>

                      </div>
                </div>

                  <div class="table-responsive pt-3">
                    <table class="table table-striped table-hover">
                      <thead>
                        <tr>
                        <th>
                        Nº
                          </th>

                          <th>
                           Foto do Estudante
                          </th>

                          <th>
                           Nº de Processo
                          </th>
                          
                          <th>
                           Nome Estudante
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        
                    <tr>
                    <td>1</td>

                        <td class="py-1">
                                <img src="<?php echo base_url(); ?>assets/images/faces/user.png" alt="image"/>
                              </td>
                            
                          <td>
                         2226
                          </td>
                          <td>
                          ABEDNEGO JOSÉ FIGUEIREDO MESSELE
                          </td>
                      
                        </tr>
                  
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        function boletim_search(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_estudante_boletim/'+s,
              success:function(data){
                  console.log(data);
                  var dados = JSON.parse(data);

                  $("#bsearchresult").html("");
                  console.log(dados);
                  var xc = false;
                  for(var key in dados)
                  {
                    xc = true;
                    $("#bsearchresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }

                  if(xc)
                  {
                    $("#bsearchresult").show();
                  }
                  else
                  {
                    $("#bsearchresult").hide();
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function boletim_selectPessoa(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_pturmas_boletim/'+s,
              success:function(data){
                  var dados = JSON.parse(data);
                  $("#bselectresult").html("");
                  for(var key in dados)
                  {
                    $("#bselectresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function submit_boletim_modal()
        {
          var idpessoa = $("#bsearchresult").val();
          var turma = $("#bselectresult").val();
          var trimestre = $("#btrimestre").val();

          window.location = '<?php echo base_url(); ?>Secretario/boletim_de_notas/'+idpessoa+'/'+turma+'/'+trimestre;
        }

        </script>