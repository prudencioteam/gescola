<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php $a_turnos = lista("turnos"); ?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

                <div class="col-lg-12">
                  <h4 class="font-weight-bold mb-0">Turmas</h4>
                </div>
                
                <div class="col-lg-12">&nbsp;</div>
                <div class="col-lg-12">&nbsp;</div>
          
            <?php foreach ($dados as $key => $value) { 
              
              $dadosTurma = get_turmaById($value["turma_id"]);
              $dadosClasse = get_classeById($dadosTurma["turma_classeId"]);
              $dadosCurso = get_cursoById($dadosTurma["turma_curso"]);
            
            ?>
            
            <div class="col-lg-3 grid-margin stretch-card">
              <div class="card">

                <div class="card-body">
                  <h4 class="card-title"> <b>TURMA: </b> <?php echo $value["turma_nome"]; ?></h4>
                  <p class="card-description"><b>Curso: </b> <?php echo $dadosCurso["curso_nome"]; ?>  </p>
                  <p class="card-description"> <b> Classe: </b><?php echo $dadosClasse["classe_nome"]; ?> </p>
                  <div class="card-description">

                    <a href="<?php echo base_url(); ?>secretario/listar_alunos_turma/<?php echo $value["turma_id"]; ?>" class="btn btn-xs btn-primary btn-icon-text"><i class="ti ti-user btn-icon-prepend"></i>Lista Estudantes</a>
                    <a href="<?php echo base_url(); ?>secretario/disciplinas_turma/<?php echo $value["turma_id"]; ?>" class="btn btn-xs btn-warning btn-icon-text"><i class="ti ti-bookmark-alt btn-icon-prepend"></i>Disciplinas</a>

                  </div>
                </div>
              </div>
            </div>
               <?php
              }
            ?>
            </div>
        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
 <?php $this->load->view("footer"); ?>
