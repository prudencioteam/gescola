<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$a_pessoas = lista("pessoas");
?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Boletim de Notas</h4>
                  <button class="btn btn-sm btn-primary mybtnlink" href="<?php echo base_url(); ?>Secretario/lista_tboletim_notas">Emitir por Turmas</button><br><br>
                  <p class="card-description">Ano Lectivo 2019 </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                      <input type="text" class="form-control" name="_search" value="<?php echo ($_POST["_search"]??""); ?>"  placeholder="Procurar Boletim Por Aluno" aria-label="Procurar Boletim..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="col-md-7">
 
                      <button type="button" href="<?php echo base_url(); ?>Secretario/boletim_modal" class="btn btn-primary btn-icon-text btn-sm ajax-popup-link" style="float:right;">
                          <i class="ti-plus btn-icon-prepend"></i>                                                    
                         Novo Boletim
                        </button>

                      </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                      
                      <?php

                      $_pesquisa = "";
                      if(isset($_POST["_search"]))
                      {
                        $_pesquisa = '<br><h5>Pesquisando por: <span class="label label-primary text-primary"><i>'.$_POST["_search"].'</i></span></h5>';
                        echo $_pesquisa;
                      }

                      ?>

                    </div>
                </div>
                
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                           Nº BOLETIM
                          </th>
                          
                          <th>
                           Estudante
                          </th>
                          
                          <th>
                            Data
                          </th>

                          <th>
                            Emitido Por
                          </th>
                       
                          <th>
                            Estado
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>

                      <?php
                          foreach ($dados as $key => $value)
                          {
                            echo '<tr>
                            <td>BOL-Nº00'.$value["id_documento"].'</td>
                            <td>'.$a_pessoas[$value["usuario_documento"]].'</td>
                            <td>'.$value["data_documento"].'</td>
                            <td>'.$a_pessoas[$value["emitido_por"]].'</td>
  
                            <td>
                        
                            
                        <button href="'.base_url().'secretario/delete_documento_modal/1/'.$value["id_documento"].'" type="button" class="ajax-popup-link btn btn-danger btn-sm" style="float:right;">
                        <i class="ti-trash btn-icon-prepend"></i></button>
                        
                        <a href="'.base_url().'secretario/boletim_de_notas/'.$value["usuario_documento"].'/'.$value["turma_documento"].'/'.$value["trimestre_documento"].'/download">
                        <button type="button" class="btn btn-success btn-sm" style="float:right; margin-right:4px;">
                        <i class="ti-import btn-icon-prepend"></i></button>
                        </a>
        
                        <a href="'.base_url().'secretario/boletim_de_notas/'.$value["usuario_documento"].'/'.$value["turma_documento"].'/'.$value["trimestre_documento"].'">
                        <button type="button" class="btn btn-warning btn-sm" style="float:right; margin-right:4px;">
                        <i class="ti-eye btn-icon-prepend"></i></button>
                        </a>

                            </td>
                            </tr>';
                          }
                      ?>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        function boletim_search(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_estudante_boletim/'+s,
              success:function(data){
                  console.log(data);
                  var dados = JSON.parse(data);

                  $("#bsearchresult").html("");
                  console.log(dados);
                  var xc = false;
                  for(var key in dados)
                  {
                    xc = true;
                    $("#bsearchresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }

                  if(xc)
                  {
                    $("#bsearchresult").show();
                  }
                  else
                  {
                    $("#bsearchresult").hide();
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function boletim_selectPessoa(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_pturmas_boletim/'+s,
              success:function(data){
                  var dados = JSON.parse(data);
                  $("#bselectresult").html("");
                  for(var key in dados)
                  {
                    $("#bselectresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function submit_boletim_modal()
        {
          var idpessoa = $("#bsearchresult").val();
          var turma = $("#bselectresult").val();
          var trimestre = $("#btrimestre").val();

          window.location = '<?php echo base_url(); ?>Secretario/emitir_boletim_de_notas/'+idpessoa+'/'+turma+'/'+trimestre;
        }

        </script>