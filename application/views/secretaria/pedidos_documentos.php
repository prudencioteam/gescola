<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");

$docs = lista("documentos");
$estado = lista("estado");
$usuario = lista("pessoas");

?>

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Documentos Solicitados</h4>
                  <p class="card-description">Ano Lectivo 2019 </p>

                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                        <input type="text" class="form-control" name="_search"  placeholder="Procurar Documentos.." aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

               </div>

                  <div class="table-responsive pt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                          Estudante
                          </th>
                          
                          <th>
                           Documento Solicitado
                          </th>

                          <th>
                           Efeito
                          </th>
                          
                          <th>
                            Data de Pedido
                          </th>

                          <th>
                            Estado
                          </th>
                       
                        </tr>
                      </thead>

                      
                      <tbody>
                     <?php foreach ($dados as $value) {       
                        echo'

                        <td>
                          '.$usuario[$value["pedido_por"]].'
                          </td> 

                          <td>
                          '.$docs[$value["tipo_documento"]].'
                          </td> 
                          <td>
                          '.$value["efeito_documento"].'
                          </td>
                          <td>
                         '.date("d-m-Y", strtotime($value["data_pedido_documento"])).'
                          </td>
                          <td>
                          '.$estado[$value["estado_documento"]].'
                          </td>';

                       ?>
                           
                        <td>
                        
                        <?php if($value["estado_documento"] == 2) {?>
                        <?php echo'<a href="'.base_url().'secretario/documento_pedido_negar/'.$value["id_documento"].'" title="Recusar Pedido">
                        <button  type="button" class="btn btn-danger btn-sm mybtnlink" style="float:right; margin-right:4px;">
                        <i class="ti-close btn-icon-prepend"></i></button>
                        </a>
                  
                        <a href="'.base_url().'secretario/documento_pedido_aceitar/'.$value["id_documento"].'"  title="Aceitar Pedido">
                        <button type="button" class="btn btn-success btn-sm mybtnlink" style="float:right; margin-right:4px;">
                        <i class="ti-check btn-icon-prepend"></i></button>
                        </a>'; ?>

                       <?php } elseif($value["estado_documento"] == 3 || $value["estado_documento"] == 4) { ?>
                       
                       <?php 
                       echo'<button href="'.base_url().'secretario/documento_pedido_apagar/'.$value["id_documento"].'" type="button" class="btn btn-danger btn-sm mybtnlink" style="float:right;">
                        <i class="ti-trash btn-icon-prepend"></i></button>
                        </td>';
                        ?>
                       
                        <?php } ?>
                       
                      </tr>
              
                     <?php } ?>
                          
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>