<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php

$dadosTurma = get_turmaById($turma);

$dadosClasse = get_classeById($dadosTurma["turma_classeId"]);
$dadosCurso = get_cursoById($dadosTurma["turma_curso"]);

?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Lista de Disciplinas</h4>

                  <p class="card-description">
                    <b>Curso:</b> <?php echo $dadosCurso["curso_nome"]; ?> <span class="text-info"> <b>Turma: </b>  <?php echo $dadosTurma["turma_nome"]; ?> | <b> Turno: </b> <?php echo $dadosTurma["turma_turno"]; ?></span>
                    <br><b> Classe:</b> <?php echo $dadosClasse["classe_nome"]; ?> 
                  </p>


                  <div class="row">
                  <div class="col-md-5">
                    <form method="post">
                      <div class="input-group">
                        <input type="text" class="form-control" name="_search"  placeholder="Procurar Disciplinas.." aria-label="Procurar Estudantes..">
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-primary" type="submit">Procurar</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="table-responsive pt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                           Nº
                          </th>
                          
                          <th>
                           Nome
                          </th>
                           
                          <th>
                           Docente
                          </th>

                          <th>
                       
                          </th>

                        </tr>
                      </thead>
                      <tbody>
                        

                        <?php

                        $count = 0;
                        $dadosLista = json_decode($dados["turma_disciplinas"], TRUE);

                        foreach ($dadosLista as $key => $value)
                        {
                          $count++;
                          $list_item = get_disciplinaById($value);

                          $dataProf = get_profTurmaDisciplina($turma, $list_item["disciplina_id"]);
                          
                          $nomeProfessor = (!empty($dataProf)) ? $dataProf[0]["pessoa_nome"] : "";
                          
                          echo '<tr>
                                  <td>
                                  '.$count.'
                                  </td>
                                  <td>'.$list_item["disciplina_nome"].'</td>
                                  <td>'.$nomeProfessor.'</td>   
                                  <td align="right">
                                  <button href="'.base_url().'Secretario/pauta_pdf/'.$turma.'/'.$list_item["disciplina_id"].'/" class="mybtnlink btn btn-sm btn-warning"><i class="ti ti-eye"></i> </button>
                                  <!--button class="btn btn-sm btn-primary"><i class="ti ti-info-alt"></i> </button-->
                                  </td>                        
                                </tr>';
                        }

                        ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
          
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php $this->load->view("footer"); ?>

        <script>

        function boletim_search(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_estudante_boletim/'+s,
              success:function(data){
                  console.log(data);
                  var dados = JSON.parse(data);

                  $("#bsearchresult").html("");
                  console.log(dados);
                  var xc = false;
                  for(var key in dados)
                  {
                    xc = true;
                    $("#bsearchresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }

                  if(xc)
                  {
                    $("#bsearchresult").show();
                  }
                  else
                  {
                    $("#bsearchresult").hide();
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function boletim_selectPessoa(a)
        {
          var s = $(a).val();

          $.ajax({
              type:'POST',
              url: '<?php echo base_url(); ?>Secretario/get_pturmas_boletim/'+s,
              success:function(data){
                  var dados = JSON.parse(data);
                  $("#bselectresult").html("");
                  for(var key in dados)
                  {
                    $("#bselectresult").append('<option value="' + key + '">' + dados[key] + '</option>');
                  }
              },
              error: function(data)
              {
                  alert("Ocorreu um erro\nPor favor tente novamente");
                  //console.log(data);
              }
          });
        }

        function submit_boletim_modal()
        {
          var idpessoa = $("#bsearchresult").val();
          var turma = $("#bselectresult").val();
          var trimestre = $("#btrimestre").val();

          window.location = '<?php echo base_url(); ?>Secretario/declaracao_sem_notas/'+idpessoa+'/'+turma+'/'+trimestre;
        }

        </script>