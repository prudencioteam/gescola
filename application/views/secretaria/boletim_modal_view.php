<?php

$a_alunos = lista("allAlunos");
$a_trimestre = lista("trimestres");

?>

<div style="position: relative; background: #FFF; padding: 15px 10px; left: unset; width:98%; max-width: 450px; margin: 20px auto; min-height: 180px;">

<h4 class="text-muted">NOVO DOCUMENTO</h4>
<br>
<div class="content">
    

    <form action="<?php echo base_url(); ?>" method="post" enctype="multipart/form-data">

        <input type="text" class="form-control" onkeyup="boletim_search(this)" placeholder="Pesquisar" required />

        <select id="bsearchresult" class="form-control" multiple="multiple" style="display: none;"  onchange="boletim_selectPessoa(this)" required="required">
            <?php
            foreach ($a_alunos as $key => $value)
            {
                echo '<option value="'.$key.'">'.$value.'</option>';
            }
            ?>
        </select>

        <br>

        <?php

        if(!isset($ocultar))
        {
            ?>

        <select id="bselectresult" class="form-control">
            
        </select>

        <br>
        
        <select id="btrimestre" class="form-control">
            <?php
            foreach ($a_trimestre as $key => $value)
            {
                echo '<option value="'.$key.'" required>'.$value.'</option>';
            }
            ?>
        </select>

            <?php
        }
        ?>

        <br>

        <button type="button" onclick="submit_boletim_modal()" class="btn btn-success">EMITIR</button>

    </form>

</div>

<div class="row">&nbsp;</div>

</div>

<?php

?>
