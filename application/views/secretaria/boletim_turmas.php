<?php $this->load->view("header"); ?>

<?php  $this->load->view("menu"); ?>

<?php $this->load->view("v_menu");?>

<?php $a_turnos = lista("turnos"); ?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
                

                <div class="col-lg-12" style="background: #FFF;">

                
                    <div class="col-lg-12">&nbsp;</div>
                    <div class="col-lg-12">&nbsp;</div>
        
                    <h4 class="card-title">Boletim de Notas</h4>
                    <p class="card-description">Ano Lectivo 2019 </p>
                    <button class="btn btn-sm btn-primary mybtnlink" href="<?php echo base_url(); ?>Secretario/lista_boletim_notas">Emitir por Estudante</button><br><br>
                    
                    <div class="col-lg-12">&nbsp;</div>
                    <div class="col-lg-12">&nbsp;</div>

                    <div class="row">

                        
            
                <?php 
                
                $a_trimestres = lista("trimestres");
                
                foreach ($dados as $key => $value) { 
                
                $dadosTurma = get_turmaById($value["turma_id"]);
                $dadosClasse = get_classeById($dadosTurma["turma_classeId"]);
                $dadosCurso = get_cursoById($dadosTurma["turma_curso"]);
                
                ?>
                
                    <div class="col-lg-3 grid-margin stretch-card">
                        <div class="card">

                            <div class="card-body">
                                <h4 class="card-title"> <b>TURMA: </b> <?php echo $value["turma_nome"]; ?></h4>
                                <p class="card-description"><b>Curso: </b> <?php echo $dadosCurso["curso_nome"]; ?>  </p>
                                <p class="card-description"> <b> Classe: </b><?php echo $dadosClasse["classe_nome"]; ?> </p>
                                <p>&nbsp;</p>
                                
                                <h4 class="card-description"><b>Baixar:</b></h4>

                                <div class="card-description">

                                    <?php

                                    foreach ($a_trimestres as $tkey => $tvalue)
                                    {
                                        ?>
                                        <a href="<?php echo base_url(); ?>secretario/turma_boletim/<?php echo $value["turma_id"]; ?>/<?php echo $tkey; ?>/download" class="btn btn-sm btn-danger btn-icon-text" style="margin-bottom: 5px;"><i class="ti ti-file btn-icon-prepend"></i><?php echo $tvalue; ?></a>
                                        <?php
                                    }

                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>

                </div>
              </div>


            </div>
        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
 <?php $this->load->view("footer"); ?>
