<?php $this->load->view("header"); ?>


<?php

$_eType = base64_decode($type);
$_eType = ($_eType == "") ? "__" : $_eType;

$_iMessage = "";

if($param == "error")
{
  switch ($_eType[0])
  {
    case '1':
      $_iMessage = '<div class="alert alert-danger" role="alert">
                          <h4 class="alert-heading">Erro!</h4>
                          <p>Usuário Inexistente.</p>
                        </div>';
      break;

      case '2':
      $_iMessage = '<div class="alert alert-danger" role="alert">
                          <h4 class="alert-heading">Erro!</h4>
                          <p>Senha Incorreta.</p>
                        </div>';
      break;
    
    default:
      # code...
      break;
  }
}

?>

<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="<?php echo base_url(); ?>assets/images/logo.svg" alt="logo">
              </div>
              
              <h4>Entrar...</h4>
              <h6 class="font-weight-light">Insira os dados para entrar.</h6>

              <?php echo $_iMessage; ?>

              <form action="<?php echo base_url(); ?>auth/logar" method="post" class="pt-3">
                <div class="form-group">
                  <input type="text" name="user" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Utilizador">
                </div>
                <div class="form-group">
                  <input type="password" name="pass" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Palavra-passe">
                </div>
                <div class="mt-3">
                  <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">ENTRAR</button>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" name="keepconnected" class="form-check-input">
                      Mantenha-me conectado
                    <i class="input-helper"></i></label>
                  </div>
                  <a href="<?php echo base_url(); ?>auth/password_recovery/choose_method" class="auth-link text-black">Esqueceu a senha?</a>
                </div>
                <div class="mb-2">
                  <button type="button" class="btn btn-block btn-google auth-form-btn">
                    <i class="ti-google mr-2"></i>Conectar utilizando o Google
                  </button>
                </div>
                
                <!--
                <div class="text-center mt-4 font-weight-light">
                  Don't have an account? <a href="register.html" class="text-primary">Create</a>
                </div>
                -->

              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>



 <?php $this->load->view("footer"); ?>