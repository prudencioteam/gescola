<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Sistema de Gestão Escolar</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/base/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/bootstrap-datepicker.min.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/#" />

  
  <link href="<?php echo base_url(); ?>assets/css/magnific-popup.css" rel="stylesheet">



  <!-- jQuery Version 1.11.1 -->
  <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/vanilla-masker.js"></script>
  

</head>
<body class="sidebar-fixed">