<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {

	public function base()
	{
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $data = array();
		$data["dados"] = get_baseConfig();
		$this->load->view('config/base', $data);
    }

	public function base_save()
	{
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		
		$password = get_defaultPassword();

		$this->mdlGeral->save_base($_POST["nomeInstituicao"], $_POST["siglaInstituicao"], $_POST["email"], "", $password, "", $_POST["endereco"], $_POST["nTel"], $_POST["dirAdministrativo"], $_POST["dirPedagogico"], $_POST["nProcess"]);
		
		if($_FILES["img"]["name"] != '')
		{
			$path = APPPATH."data/img/system/logo.jpg";

			require(APPPATH.'classes/wideimage/WideImage.php');
			$image = wideImage::load($_FILES["img"]["tmp_name"]);
			//$tamanho = 400;
			//$tamanho = ($image->getWidth() > $image->getHeight()) ? $image->getHeight() : $image->getWidth();
			//$image = $image->crop("center" ,"center" , $tamanho, $tamanho) ;
            $image->saveToFile($path);
		}
		
		redirect(base_url()."config/base");
    }


}
