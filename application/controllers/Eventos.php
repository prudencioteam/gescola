<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller {

	public function index()
	{
		verificar_sessao();
		$this->load->view('home');
    }
    
    public function horario_modelos($op = 'listar', $param = '')
	{
		verificar_sessao();
        $data["dados"] = array();

        switch ($op) {
            case 'add':
                $this->load->view('eventos/horario_addmodelos_view.php', $data);
                break;

            case 'ver':
                $this->load->model("Mdl_geral", "mdlGeral");
                $data["dados"] = $this->mdlGeral->get_modeloHorarioById($param);
                $this->load->view('eventos/horario_vermodelos_view.php', $data);
                break;
            
            default:
                $this->load->model("Mdl_geral", "mdlGeral");
                $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
                $data["dados"] = $this->mdlGeral->get_modeloHorarios($filtro);
                $this->load->view('eventos/horario_modelos_view.php', $data);
                break;
        }
    }

    public function horario($op = "todos", $ano = '', $turma = '')
    {
        $ano = ($ano == "") ? date("Y") : $ano;
        verificar_sessao();
        $data["dados"] = array();

        switch ($op) {
            case 'ver':
                $data["_turma"] = $turma;
                $this->load->view('eventos/horario_ver_view.php', $data);
                break;
            
            default:
                $this->load->model("Mdl_geral", "mdlGeral");
                $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
                $data["dados"] = $this->mdlGeral->get_horariosByYear($ano, $filtro);
                $this->load->view('eventos/horario_list_view.php', $data);
                break;
        }
    }

    public function delete_horario_modal($id)
    {
        $data["_id"] = $id;
        $this->load->view("eventos/horario_modal_delete.php", $data);
    }

    public function delete_horario($id)
    {
        $this->load->model("Mdl_geral", "mdlGeral");
        $this->mdlGeral->delete_horario($id);
        redirect(base_url()."eventos/horario/todos");
    }

    public function delete_hmodelo_modal($id)
    {
        $data["_id"] = $id;
        $this->load->view("eventos/hmodelo_modal_delete.php", $data);
    }

    public function delete_hmodelo($id)
    {
        $this->load->model("Mdl_geral", "mdlGeral");
        $this->mdlGeral->delete_hmodelo($id);
        redirect(base_url()."eventos/horario_modelos");
    }
    
    public function horario_modelos_save()
    {
		verificar_sessao();
        $arrResult = array();

        foreach ($_POST["_horaInicio"] as $key => $value)
        {
            $arr = array();
            $arr["inicio"] = $_POST["_horaInicio"][$key];
            $arr["fim"] = $_POST["_horaFim"][$key];

            array_push($arrResult, $arr);
        }

        $this->load->model("Mdl_geral", "mdlGeral");
        $this->mdlGeral->save_modeloHorarios($_POST["_ref"], json_encode($arrResult));

        redirect(base_url()."Eventos/horario_modelos/todos");

    }

    public function criar_horario_turma_modal($id)
    {
		verificar_sessao();
        $data["id"] = $id;
        $this->load->view('eventos/horario_escolha_turma_modal_view.php', $data);
    }

    public function horario_from_modelo($id, $turma)
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $data["dados"] = $this->mdlGeral->get_modeloHorarioById($id);
        $data["modelo"] = $id;
        $data["turma"] = $turma;
        $this->load->view('eventos/horario_from_modelo_view.php', $data);
    }

    public function horario_edit($id)
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $_dados = $this->mdlGeral->get_horarioById($id);
        $data["dados"] = $this->mdlGeral->get_modeloHorarioById($_dados["hr_modelo"]);
        $data["dados_hr"] = $_dados;
        $data["modelo"] = $_dados["hr_modelo"];
        $data["turma"] = $_dados["hr_turma"];
        $data["idHorario"] = $id;
        $this->load->view('eventos/horario_edit_view.php', $data);
    }

    public function save_turma()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $arrDados = array();

        foreach ($_POST as $key => $value)
        {
            if(substr($key, 0, 1) == "_") continue;
            $arrDados[$key] = $value;
        }

        $this->mdlGeral->save_horarios($_POST["_turma"], $_POST["_modelo"], json_encode($arrDados));

        redirect(base_url()."Eventos/horario_modelos");
    }

    public function update_turma()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $arrDados = array();

        foreach ($_POST as $key => $value)
        {
            if(substr($key, 0, 1) == "_") continue;
            $arrDados[$key] = $value;
        }

        $this->mdlGeral->update_horario($_POST["_id"], json_encode($arrDados));

        redirect(base_url()."Eventos/horario/todos");
    }

}
