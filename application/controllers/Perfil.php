<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

	public function config()
	{
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $data = array();
		$this->load->view('perfil/config', $data);
    }

    public function change_password_view($error = '')
    {
        $this->load->model("Mdl_geral", "mdlGeral");
        $data = array();
        $data["error"] = $error;
        $this->load->view("perfil/change_password", $data);
        
    }

    public function change_password()
    {
		verificar_sessao();
        
        $this->load->model("Mdl_geral", "mdlGeral");

        $id = $this->session->userdata("pessoa_id");
        $dados = $this -> mdlGeral -> get_userdata_byId($id);

		if($_POST["_atual"] != $dados['pessoa_password'])
		{
            ob_clean();
            echo json_encode(array("error" => 1));
            die();
		}
		
		if($_POST["_nova"] != $_POST["_confirmar"])
		{
            ob_clean();
            echo json_encode(array("error" => 2));
            die();
        }
        
        $this -> mdlGeral -> update_password($id, $_POST["_nova"]);
        
        ob_clean();
        echo json_encode(array("error" => 0));
    }

    public function change_mypassword()
    {        
        $this->load->model("Mdl_geral", "mdlGeral");

        $id = $this->session->userdata("pessoa_id");
        $dados = $this -> mdlGeral -> get_userdata_byId($id);

		if($_POST["_atual"] != $dados['pessoa_password'])
		{
            redirect(base_url()."Perfil/change_password_view/1");
		}
		
		if($_POST["_nova"] != $_POST["_confirmar"])
		{
            redirect(base_url()."Perfil/change_password_view/2");
        }
        
        $this -> mdlGeral -> update_password($id, $_POST["_nova"]);

        if(!isset($_SESSION["pessoa_password"]))
        {
            session_start();
        }
        $_SESSION["pessoa_password"] = $_POST["_nova"];
        
        redirect(base_url());
    }

	public function save_photo()
	{
        if($_FILES["_img"]["name"] != '')
		{
			$path = APPPATH."data/img/profile/".md5($_POST["idPessoa"]).".jpg";

			require(APPPATH.'classes/wideimage/WideImage.php');
			$image = wideImage::load($_FILES["_img"]["tmp_name"]);
			$tamanho = 400;
			$tamanho = ($image->getWidth() > $image->getHeight()) ? $image->getHeight() : $image->getWidth();
			$image = $image->crop("center" ,"center" , $tamanho, $tamanho) ;
            $image->saveToFile($path);
            
            ob_clean();
            echo json_encode(array("img" => md5($_POST["idPessoa"]), "error" => 0));
            die();//NECESSÁRIO
        }

        ob_clean();
        echo json_encode(array("img" => "", "error" => 1));
    }


}
