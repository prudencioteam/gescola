<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Professor extends CI_Controller {

	public function index()
	{
		verificar_sessao();
		$this->load->view('home');
    }

    public function turmas($ano = '')
    {
        $ano = ($ano == '') ? date("Y") : $ano;
        verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $professorId = $this->session->userdata("pessoa_id");
        $tmpDados = $this->mdlGeral->get_professorTurmas($professorId, $ano);

        $data = array();
        $data["dados"] = $tmpDados;
        $data["_ano"] = $ano;
        $this->load->view('professor/turmas', $data);
    }
    
	public function pauta($turma = "", $disciplina = "", $trimestre = "1")
	{
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");

        $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
        $tmpDados = $this->mdlGeral->get_estudantesByTurma($turma, $trimestre, $disciplina);
        
        $data["dados"] = array();
        if($filtro != "")
        {
            foreach ($tmpDados as $key => $value)
            {                
                $__nproc = (int)stripos("_".$value["pessoa_nProc"], $filtro);
                $__nome = (int)stripos("_".$value["pessoa_nome"], $filtro);
                
                if(($__nproc > 0) || ($__nome > 0))
                {
                    array_push($data["dados"], $value);
                }
            }
        }
        else
        {
            $data["dados"] = $tmpDados;
        }


		$data["idTurma"] = $turma;
		$data["idDisciplina"] = $disciplina;
		$data["trimestre"] = $trimestre;
		$data["filtro"] = $filtro;
		$this->load->view('professor/pautas', $data);
    }
    
	public function lista_alunos($turma = "", $disciplina = "")
	{
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");

        $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
        $tmpDados = $this->mdlGeral->get_estudantesByTurma($turma);
        
        $data["dados"] = array();
        if($filtro != "")
        {
            foreach ($tmpDados as $key => $value)
            {                
                $__nproc = (int)stripos("_".$value["pessoa_nProc"], $filtro);
                $__nome = (int)stripos("_".$value["pessoa_nome"], $filtro);
                
                if(($__nproc > 0) || ($__nome > 0))
                {
                    array_push($data["dados"], $value);
                }
            }
        }
        else
        {
            $data["dados"] = $tmpDados;
        }

		$data["idTurma"] = $turma;
		$data["idDisciplina"] = $disciplina;
		$data["filtro"] = $filtro;
		$this->load->view('professor/lista_alunos', $data);
    }
    
    public function save_pauta()
    {
		verificar_sessao();
        $professor = $this->session->userdata("pessoa_id");
        foreach ($_POST as $key => $value)
        {
            if(substr($key, 1, 5) == "nota_")
            {
                $tipoAvaliacao = substr($key, 0, 1);
                $valorAvaliacao = $value;
                $_var = explode("_", $key);

                $this->load->model("Mdl_geral", "mdlGeral");
                
                if($_var[2] != "")
                {
                    $idAvaliacao = $_var[2];
                    $this->mdlGeral->update_avaliacao($idAvaliacao, $valorAvaliacao);
                }
                else
                {
                    $estudante = $_var[1];
                    $this->mdlGeral->save_avaliacao($_POST["trimestre"], $_POST["disciplinaId"], $_POST["turmaId"], $tipoAvaliacao, (float)$valorAvaliacao, $estudante, $professor);
                }
                
            }
        }
            
        redirect(base_url()."professor/pauta/".$_POST["turmaId"]."/".$_POST["disciplinaId"]."/".$_POST["trimestre"]);
    }

    public function limpar_pauta_modal($turma, $disciplina, $trimestre = "1")
    {
        $data = array();
        $data["turma"] = $turma;
        $data["disciplina"] = $disciplina;
        $data["trimestre"] = $trimestre;
        
        $this->load->view('professor/limpar_pauta_modal_view', $data);
    }

    public function limpar_pauta($turma, $disciplina, $trimestre = "1")
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");      
        $this->mdlGeral->limpar_pauta($turma, $disciplina, $trimestre);
        redirect(base_url()."professor/pauta/".$turma."/".$disciplina."/".$trimestre);
    }

    public function importar_pauta_xls_modal($turma, $disciplina, $trimestre = "1")
    {
        $data = array();
        $data["turma"] = $turma;
        $data["disciplina"] = $disciplina;
        $data["trimestre"] = $trimestre;
        
        $this->load->view('professor/importar_xls_pauta_modal_view', $data);
    }

    public function importar_pauta_xls($turma, $disciplina, $trimestre = "1")
    {
		verificar_sessao();
        $resultado = array();
        $resultado["error"] = "0";
        $resultado["error_tipo"] = "-1";
        $resultado["message"] = "";
        //=========================================

        if(!isset($_FILES["pauta"]))
        {
            $resultado["error"] = "1";
            $resultado["error_tipo"] = "no-file";
            $resultado["message"] = "Nenhum ficheiro enviado";
        }
        else if(!isset($_FILES["pauta"]))
        {
            $resultado["error"] = "1";
            $resultado["error_tipo"] = "no-file";
            $resultado["message"] = "Nenhum ficheiro enviado";
        }
        
        $this->load->model("Mdl_geral", "mdlGeral");

        $_destino = APPPATH."data/system/tmp/pauta_import.xls";
        $professor = $this->session->userdata("pessoa_id");
        $a_pessoas_nProcId = lista("pessoas_nprocId");
        
        move_uploaded_file($_FILES["pauta"]["tmp_name"], $_destino);

        require_once APPPATH."classes/PHPExcel/PHPExcel.php";
        $objPHPExcel = PHPExcel_IOFactory::load($_destino);

        $highestRow         = $objPHPExcel->getActiveSheet()->getHighestRow();
        $highestColumn      = $objPHPExcel->getActiveSheet()->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
       
            $dadosImportar = array();
            $arrLetras = range("A", "Z");

            $indiceNotas = 4 +(($trimestre - 1)*6);

            $_podeComecar = false;
            for ($i=1; $i < $highestRow; $i++)
            {
                $_linha = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
                if($_linha == 1)
                {
                    $_podeComecar = true;
                }

                if(!$_podeComecar) continue;

                if($_linha == NULL) continue;

                $nProc = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
                $_nome = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();

                $nota1 = (float)$objPHPExcel->getActiveSheet()->getCell($arrLetras[$indiceNotas].$i)->getCalculatedValue();
                $nota2 = (float)$objPHPExcel->getActiveSheet()->getCell($arrLetras[$indiceNotas + 1].$i)->getCalculatedValue();
                $avaliacao = (float)$objPHPExcel->getActiveSheet()->getCell($arrLetras[$indiceNotas + 2].$i)->getCalculatedValue();
                $media = (float)$objPHPExcel->getActiveSheet()->getCell($arrLetras[$indiceNotas + 3].$i)->getCalculatedValue();

                $arr["linha"] = $_linha;
                $arr["nome"] = $_nome;
                $arr["nproc"] = $nProc;
                $arr["nota1"] = ($nota1 > 20) ? 20 : $nota1;
                $arr["nota2"] = ($nota2 > 20) ? 20 : $nota2;
                $arr["avaliacao"] = ($avaliacao > 20) ? 20 : $avaliacao;
                $arr["media"] = ($media > 20) ? 20 : $media;
                
                array_push($dadosImportar, $arr);
            }
            
            foreach ($dadosImportar as $key => $value)
            {
                $estudanteId = isset($a_pessoas_nProcId[$value["nproc"]]) ? $a_pessoas_nProcId[$value["nproc"]] : "";
                if($estudanteId != "")
                {
                    $this->mdlGeral->del_avaliacao($trimestre, $disciplina, $turma, $estudanteId, "1");
                    $this->mdlGeral->del_avaliacao($trimestre, $disciplina, $turma, $estudanteId, "2");
                    $this->mdlGeral->del_avaliacao($trimestre, $disciplina, $turma, $estudanteId, "3");
                    $this->mdlGeral->del_avaliacao($trimestre, $disciplina, $turma, $estudanteId, "4");

                    //============================================================================================

                    $this->mdlGeral->save_avaliacao($trimestre, $disciplina, $turma, "1", abs($value["nota1"]), $estudanteId, $professor);
                    $this->mdlGeral->save_avaliacao($trimestre, $disciplina, $turma, "2", abs($value["nota2"]), $estudanteId, $professor);
                    $this->mdlGeral->save_avaliacao($trimestre, $disciplina, $turma, "3", abs($value["avaliacao"]), $estudanteId, $professor);
                    $__media = (abs($value["nota1"])+abs($value["nota2"])+abs($value["avaliacao"]))/3;

                    //COLOCA DOIS ALGARISMOS APÓS A VIRGULA |||| SOMENTE SE EXISTIREM MAIS DE DOIS ALGARISMOS
                    $_tt = explode(",", $__media."");
                    if(isset($__tt[1]))
                    {
                        if(strlen($_tt[1]) > 2)
                        {
                            $__media = number_format($__media, 2, ",", ".");
                        }
                    }
                    //=======================================================================================
                    $this->mdlGeral->save_avaliacao($trimestre, $disciplina, $turma, "4", $__media, $estudanteId, $professor);
                }
                else
                {
                    var_dump("NÃO IMPORTADO");
                }
            }
        
        unlink($_destino);

        redirect(base_url()."professor/pauta/".$turma."/".$disciplina."/".$trimestre);

    }

	public function pauta_xls($turma, $disciplina, $trimestre = "1")
	{
		verificar_sessao();
        $trimestre = ($trimestre == "all") ? array(1,2,3) : array($trimestre);
        
        include APPPATH."classes/PHPExcel/PHPExcel.php";
        
        $this->load->model("Mdl_geral", "mdlGeral");
        $dadosXLS = $this->mdlGeral->get_estudantesByTurma($turma);

        $dadosTurma = get_turmaById($turma);
        $dadosCurso = get_cursoById($dadosTurma["turma_curso"]);
        $dadosDisciplina = get_disciplinaById($disciplina);
        $dadosClasse = get_classeById($dadosTurma["turma_classeId"]);

        $dadosProfessor = get_profTurmaDisciplina($turma, $disciplina);

        $lista_dados = array();

        $dadosLista = array();

        foreach ($dadosXLS as $key => $value)
        {
            $arr = array();
            $arr["dados_pessoa"] = $value;
            $arr["dados_nota"] = array();

            $arr["dados_nota"][1] = array();
            $arr["dados_nota"][2] = array();
            $arr["dados_nota"][3] = array();

            for ($i=1; $i <= 3; $i++)
            { 
                for ($k=1; $k <= 3; $k++)
                { 
                    $arr["dados_nota"][$i][$k] = "";
                }
            }
            
            if(count($trimestre) > 1)
            {
                $dadosAvaliacoes = get_alunoAvaliacoes_all($turma, $value["pessoa_id"], $disciplina);
            }
            else
            {
                $dadosAvaliacoes = get_alunoAvaliacoes($turma, $value["pessoa_id"], $disciplina, $trimestre[0]);
            }

            foreach ($dadosAvaliacoes as $k => $v)
            {
                $arr["dados_nota"][$v["avaliacao_trimestre"]][$v["avaliacao_tipo"]] = $v["avaliacao_nota"];
            }
            array_push($dadosLista, $arr);
        }

        //ESTILOS
        $_style_center = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

        $_style_bordered = array(
            'borders' => array(
              'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
              )
            )
          );

        $_style_color_red = array(
            'font'  => array(
                'color' => array('rgb' => 'FF0000')
            ));


        $_style_color_discip = array(
            'font'  => array(
                'color' => array('rgb' => 'FF00FF')
            ));
        

        $_style_color_blue = array(
            'font'  => array(
                'color' => array('rgb' => '2d3aff')
            ));
        

        $_style_bolded = array(
            'font'  => array(
                'bold' => true
            ));
        
        $_style_unbold = array(
            'font'  => array(
                'bold' => false
            ));


        $objPHPExcel = new PHPExcel();

        $gdImage = imagecreatefrompng(get_pathLogoInstituicao());
        
        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing->setName('insignia');$objDrawing->setDescription('insignia');
        $objDrawing->setImageResource($gdImage);
        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing->setHeight(80);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        $objDrawing->setCoordinates('I1');

        foreach (range("A", "Z") as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($value)->setWidth(5);
        }

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("PRUNI SOFTWARE")
									->setLastModifiedBy("PRUNI SOFTWARE")
									->setTitle("Office 2007 XLSX Mini-Pauta")
									->setSubject("Office 2007 XLSX Mini-Pauta")
									->setDescription("Documento XLSX, gerado por PRUNI SOFTWARE.")
									->setKeywords("office 2007 openxml pruni mini-pauta pauta")
									->setCategory("Mini-Pauta");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(40);

        $objPHPExcel->setActiveSheetIndex(0)->getStyle('B1')->applyFromArray($_style_center);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('B2')->applyFromArray($_style_center);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('B4')->applyFromArray($_style_center);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Visto');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', 'O Sub-Director Pedagógico');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', '{Nome Sub-Director Pedagogico}');
        
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(4);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:Y5');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:Y6');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:Y7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A8:Y8');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A9:Y9');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A10:Y10');

        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A10')->applyFromArray($_style_center);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A11:Y16')->applyFromArray($_style_center);

        $objPHPExcel->getActiveSheet()->getStyle("A11:Y16")->getFont()->setSize(9);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A11:Y16')->applyFromArray($_style_bordered);
        $objPHPExcel->getActiveSheet()->getStyle('A11:Y16')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A5', 'REPÚBLICA DE ANGOLA');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A6', 'MINISTÉRIO DA EDUCAÇÃO');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A7', 'INSTITUTO MÉDIO {XXXXXXXXXXXXX}');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A8', 'SUB-DIRECÇÃO PEDAGÓGICA');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A9', 'MAPA DE AVALIAÇÃO TRIMESTRAL/'.$dadosTurma["turma_ano"]);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A10', 'CURSO: '.mb_strtoupper($dadosCurso["curso_nome"], 'UTF-8'));
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension(10)->setRowHeight(25);
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A11:D11');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A12:D13');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A11', 'DISCIPLINA');
        $objPHPExcel->getActiveSheet()->getStyle("A12")->getFont()->setSize(16);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A12', $dadosDisciplina["disciplina_nome"]);


        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E11:F11');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('G11:H11');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('I11:J13');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E11', 'MASCULINO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G11', 'FEMENINO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I11', 'FALTAS');

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E12', 'POS');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F12', 'NEG');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G12', 'POS');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H12', 'NEG');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('K11:L11');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('M11:N11');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('O11:P13');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K11', 'MASCULINO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M11', 'FEMENINO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O11', 'FALTAS');


        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K12', 'POS');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L12', 'NEG');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M12', 'POS');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N12', 'NEG');


        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q11:R11');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('S11:T11');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('U11:V13');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q11', 'MASCULINO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S11', 'FEMENINO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U11', 'FALTAS');

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q12', 'POS');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R12', 'NEG');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S12', 'POS');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T12', 'NEG');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('W11:Y13');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W11', $dadosTurma["turma_nome"]);
        $objPHPExcel->getActiveSheet()->getStyle("W11")->getFont()->setSize(18);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A14:A16');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B14:B16');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C14:C16');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D14:D16');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E14:V14');


        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E15:H15');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('I15:I16');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J15:J16');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('K15:N15');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('O15:O16');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('P15:P16');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q15:T15');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('U15:U16');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('V15:V16');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('X15:X16');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('Y15:Y16');

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A14', 'Nº');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B14', 'NOME COMPLETO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C14', 'PROC.');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D14', 'SEXO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E14', 'CLASSIFICAÇÃO DE FREQUÊNCIA (C.F.)');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E15', 'I TRIMESTRE');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K15', 'II TRIMESTRE');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q15', 'III TRIMESTRE');

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E16', '1P');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F16', '2P');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G16', 'MP');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H16', 'MT I');

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I15', 'JUSTIF.');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J15', 'INJUSTIF.');
        $objPHPExcel->getActiveSheet()->getStyle('I15')->getAlignment()->setTextRotation(90);
        $objPHPExcel->getActiveSheet()->getStyle('J15')->getAlignment()->setTextRotation(90);
        $objPHPExcel->getActiveSheet()->getStyle('I15')->applyFromArray($_style_color_blue);
        $objPHPExcel->getActiveSheet()->getStyle('J15')->applyFromArray($_style_color_red);

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K16', '1P');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L16', '2P');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M16', 'MP');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N16', 'MT II');

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O15', 'JUSTIF.');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P15', 'INJUSTIF.');
        $objPHPExcel->getActiveSheet()->getStyle('O15')->getAlignment()->setTextRotation(90);
        $objPHPExcel->getActiveSheet()->getStyle('P15')->getAlignment()->setTextRotation(90);
        $objPHPExcel->getActiveSheet()->getStyle('O15')->applyFromArray($_style_color_blue);
        $objPHPExcel->getActiveSheet()->getStyle('P15')->applyFromArray($_style_color_red);

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q16', '1P');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R16', '2P');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S16', 'MP');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T16', 'MT III');

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U15', 'JUSTIF.');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V15', 'INJUSTIF.');
        $objPHPExcel->getActiveSheet()->getStyle('U15')->getAlignment()->setTextRotation(90);
        $objPHPExcel->getActiveSheet()->getStyle('V15')->getAlignment()->setTextRotation(90);
        $objPHPExcel->getActiveSheet()->getStyle('U15')->applyFromArray($_style_color_blue);
        $objPHPExcel->getActiveSheet()->getStyle('V15')->applyFromArray($_style_color_red);

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W15', '0,0%');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W16', '');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X15', 'OBS.');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y15', 'Nº');

        $objPHPExcel->getActiveSheet()->getStyle('A11')->applyFromArray($_style_bolded);
        $objPHPExcel->getActiveSheet()->getStyle('A12')->applyFromArray($_style_bolded);
        $objPHPExcel->getActiveSheet()->getStyle('A12')->applyFromArray($_style_color_discip);
        $objPHPExcel->getActiveSheet()->getStyle('W11')->applyFromArray($_style_bolded);
        $objPHPExcel->getActiveSheet()->getStyle('A14:Y16')->applyFromArray($_style_bolded);

        $objPHPExcel->getActiveSheet()->getStyle('I11')->applyFromArray($_style_color_red);
        $objPHPExcel->getActiveSheet()->getStyle('O11')->applyFromArray($_style_color_red);
        $objPHPExcel->getActiveSheet()->getStyle('U11')->applyFromArray($_style_color_red);

        $objPHPExcel->getActiveSheet()->getStyle('B1:B4')->applyFromArray($_style_bolded);
        $objPHPExcel->getActiveSheet()->getStyle('A9')->applyFromArray($_style_bolded);
        $objPHPExcel->getActiveSheet()->getStyle('A10')->applyFromArray($_style_bolded);
        $objPHPExcel->getActiveSheet()->getStyle('A10')->applyFromArray($_style_color_red);
        $objPHPExcel->getActiveSheet()->getStyle("A10")->getFont()->setSize(18);
        //===========
        $objPHPExcel->getActiveSheet()->getStyle('I15:J15')->applyFromArray($_style_unbold);
        $objPHPExcel->getActiveSheet()->getStyle('O15:P15')->applyFromArray($_style_unbold);
        $objPHPExcel->getActiveSheet()->getStyle('U15:V15')->applyFromArray($_style_unbold);

        $objPHPExcel->getActiveSheet()->getStyle("I15:J15")->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle("O15:P15")->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle("U15:V15")->getFont()->setSize(8);

        $_counter = 0;
        $_lineCounter = 16;

        foreach ($dadosLista as $key => $value)
        {
            $_counter++;
            $_lineCounter++;

            $media1 = ((float)$value["dados_nota"][1][1] + (float)$value["dados_nota"][1][2] + (float)$value["dados_nota"][1][3])/3;
            $media2 = ((float)$value["dados_nota"][2][1] + (float)$value["dados_nota"][2][2] + (float)$value["dados_nota"][2][3])/3;
            $media3 = ((float)$value["dados_nota"][3][1] + (float)$value["dados_nota"][3][2] + (float)$value["dados_nota"][3][3])/3;
 
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$_lineCounter, $_counter);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$_lineCounter, $value["dados_pessoa"]["pessoa_nome"]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$_lineCounter, $value["dados_pessoa"]["pessoa_nProc"]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$_lineCounter, $value["dados_pessoa"]["pessoa_genero"]);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$_lineCounter, $value["dados_nota"][1][1]);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][1][1]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$_lineCounter, $value["dados_nota"][1][2]);
            $objPHPExcel->getActiveSheet()->getStyle('F'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][1][2]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$_lineCounter, $value["dados_nota"][1][3]);
            $objPHPExcel->getActiveSheet()->getStyle('G'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][1][3]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$_lineCounter, $media1);
            $objPHPExcel->getActiveSheet()->getStyle('H'.$_lineCounter)->applyFromArray((((float)$media1) < 10) ? $_style_color_red : $_style_color_blue);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$_lineCounter, $value["dados_nota"][2][1]);
            $objPHPExcel->getActiveSheet()->getStyle('K'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][2][1]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$_lineCounter, $value["dados_nota"][2][2]);
            $objPHPExcel->getActiveSheet()->getStyle('L'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][2][2]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$_lineCounter, $value["dados_nota"][2][3]);
            $objPHPExcel->getActiveSheet()->getStyle('M'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][2][3]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$_lineCounter, $media2);
            $objPHPExcel->getActiveSheet()->getStyle('N'.$_lineCounter)->applyFromArray((((float)$media2) < 10) ? $_style_color_red : $_style_color_blue);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$_lineCounter, $value["dados_nota"][3][1]);
            $objPHPExcel->getActiveSheet()->getStyle('Q'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][3][1]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$_lineCounter, $value["dados_nota"][3][2]);
            $objPHPExcel->getActiveSheet()->getStyle('R'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][3][2]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$_lineCounter, $value["dados_nota"][3][3]);
            $objPHPExcel->getActiveSheet()->getStyle('S'.$_lineCounter)->applyFromArray((((float)$value["dados_nota"][3][3]) < 10) ? $_style_color_red : $_style_color_blue);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$_lineCounter, $media3);
            $objPHPExcel->getActiveSheet()->getStyle('T'.$_lineCounter)->applyFromArray((((float)$media3) < 10) ? $_style_color_red : $_style_color_blue);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$_lineCounter, "CF");

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$_lineCounter, $_counter);
        }

        $objPHPExcel->setActiveSheetIndex(0)->getStyle('C17:C'.$_lineCounter)->applyFromArray($_style_center);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('D17:D'.$_lineCounter)->applyFromArray($_style_center);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('W17:W'.$_lineCounter)->applyFromArray($_style_center);


        $objPHPExcel->setActiveSheetIndex(0)->getStyle('C17:Y'.$_lineCounter)->applyFromArray($_style_center);//CENTRALIZA TUDO
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('E17:W'.$_lineCounter)->applyFromArray($_style_bolded);
        $objPHPExcel->getActiveSheet()->getStyle('E17:W'.$_lineCounter)->getFont()->setSize(10);


        $_lcFooter = $_lineCounter + 7;

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$_lcFooter.':C'.$_lcFooter);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('O'.$_lcFooter.':X'.$_lcFooter);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$_lcFooter, "O PROFESSOR");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$_lcFooter, "O PROFESSOR");
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('O'.$_lcFooter)->applyFromArray($_style_center);
        
        $_lcFooter++;

        $objPHPExcel->setActiveSheetIndex(0)->getStyle('B'.$_lcFooter)->applyFromArray($_style_color_blue);
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($_lcFooter)->setRowHeight(30);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$_lcFooter)->getFont()->setSize(18);

        $_nomeProfessor = isset($dadosProfessor[0]) ? $dadosProfessor[0]["pessoa_nome"] : "undefined";

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$_lcFooter, $_nomeProfessor);

        $_lcFooter++;

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$_lcFooter, "(NOME COMPLETO)");
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('B'.$_lcFooter)->applyFromArray($_style_bolded);

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle($dadosTurma["turma_nome"].' - '.$dadosClasse["classe_nome"]);

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

        $_sClass = (explode(" ", str_replace(array("º", "ª"), array("",""), $dadosClasse["classe_nome"]))[0]);
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Mini-Pauta_'.$dadosTurma["turma_nome"].'_'.$_sClass.'_'.date("dmY").'_'.date("His").'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}

	public function pauta_pdf($turma, $disciplina, $trimestre = "1", $output = "")
	{
		verificar_sessao();
        $this->load->model("Mdl_pauta", "mdlPauta");
        $this->mdlPauta->pdf($turma, $disciplina, $trimestre, $output);
    }
    
    public function nova_notificacao()
	{
		verificar_sessao();
		$this->load->view('direcao/notificacao_new_modal.php');
	}
    
    public function notificacoes()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('professor/notificacoes.php');
    }
}
