<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends CI_Controller {

	public function get_profilePhoto($img = "default")
	{
        $filename = APPPATH."data/img/profile/".$img.".jpg";
		
		if(!file_exists($filename))
		{
			$filename = APPPATH."data/img/profile/default.jpg";
		}

		header("Content-type: image/jpeg");
		readfile($filename);
	}
	
	public function get_imgLogoInstituicao()
	{
		$filename = APPPATH."data/img/system/logo.jpg";

		header("Content-type: image/jpeg");
		readfile($filename);
	}


}
