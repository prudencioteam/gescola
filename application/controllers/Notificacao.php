<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacao extends CI_Controller {

	public function index()
	{
		verificar_sessao();
		$this->load->view('home');
    }

	public function nova_notificacao()
	{
		verificar_sessao();
		$this->load->view('notificacao/notificacao_new_modal.php');
	}
    
    public function listar($op = "recebidas")
    {
        verificar_sessao();
        $meuid = $this->session->userdata("pessoa_id");
        if(!temAcesso("notificacoes")) redirect(base_url());

        $this->load->model("Mdl_geral", "mdlGeral");
        
        $filtro = (isset($_POST["_search"])) ? $_POST["_search"] : "";
        
        switch ($op) {
            case 'enviadas':
                $data["type"] = "out";
                $data["dados"] = $this->mdlGeral->get_outNotificacoes($filtro);
                break;

            case 'recebidas':
                $data["type"] = "in";
                $data["dados"] = $this->mdlGeral->get_inNotificacoes($filtro);
                foreach ($data["dados"] as $key => $value)
                {
                    if($value["nvista_id"] == NULL)
                    {
                        $idNvista = $this->markAsViewed($value["notif_id"], $meuid);
                        $data["dados"][$key]["nvista_id"] = $idNvista;
                    }
                }
                break;
            
            default:
                //$data["dados"] = $this->mdlGeral->get_allNotificacoes();
                redirect(base_url()."notificacao/listar/recebidas");
                $data["dados"] = array();
                break;
        }
        $this->load->view('notificacao/notificacoes.php', $data);
    }

    public function ver($id)
    {
        verificar_sessao();
        $meuid = $this->session->userdata("pessoa_id");
        $this->load->model("Mdl_geral", "mdlGeral");
        $data["dados"] = $this->mdlGeral->get_notificacaoById($id);
        $this->mdlGeral->notif_markAsViewed($data["dados"]["notif_id"], $meuid);
        $this->load->view('notificacao/notificacao_view_modal.php', $data);
    }

    public function markAsViewed($id, $pessoa)
    {
        $this->load->model("Mdl_geral", "mdlGeral");
        return $this->mdlGeral->notif_markAsViewed($id, $pessoa);
    }
    
    public function add_notificacao()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $especific = json_encode($_POST["_group"]);
        $this->mdlGeral->add_notificacao($_POST["_title"], $_POST["_msg"], $_POST["_for"], $especific);
        redirect(base_url()."notificacao/listar");
    }

    public function nova_notificacao_turma($id)
	{
        verificar_sessao();
        $data["id"] = $id;
		$this->load->view('notificacao/notificacao_turma_new_modal.php', $data);
	}

    public function add_notificacao_turma()
    {
        verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $alunos = get_alunosTurma($_POST["_for"]);

        $especific = array();

        foreach ($alunos as $key => $value)
        {
            array_push($especific, $value["pessoa_id"]);
        }

        $especific = json_encode($especific);

        $turma = $_POST["_for"];
        $para = "class";
        $this->mdlGeral->add_notificacao_turma($_POST["_title"], $_POST["_msg"], $para, $especific, $turma);
        redirect(base_url()."professor/turmas");
    }

    public function delete_notificacao_modal($id)
    {
        $data["_id"]= $id;
        $this->load->view("notificacao/notificacao_delete_modal.php", $data);
    }

    public function delete_notificacao($id)
    {
        $this->load->model("Mdl_geral", "mdlGeral");
        $this->mdlGeral->delete_notificacao($id);
        redirect(base_url()."notificacao/listar/enviadas");
    }

    public function hide_notificacao_modal($id)
    {
        $data["_id"]= $id;
        $this->load->view("notificacao/notificacao_hide_modal.php", $data);
    }

    public function hide_notificacao($id)
    {
        $this->load->model("Mdl_geral", "mdlGeral");
        $this->mdlGeral->hide_notificacao($id);
        redirect(base_url()."notificacao/listar/recebidas");
    }

}
