<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Direcao extends CI_Controller {

	public function index()
	{
		verificar_sessao();
		$this->load->view('home');
	}
		
    public function lista_estudantes()
    {
		verificar_sessao();
		$this->load->model("Mdl_estudantes", "mdlEstudantes");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlEstudantes->get_allEstudantes($filtro);
		$this->load->view('direcao/cadastros/estudantes/estudantes.php', $data);   
	}

	public function cadastrar_estudantes()
    {
		verificar_sessao();
		$this->load->model("Mdl_estudantes", "mdlEstudantes");
		$this->load->view('direcao/cadastros/estudantes/estudantes_add.php');   
	}

	public function editar_estudantes($id)
    {
		verificar_sessao();
		$data["dados"] = get_pessoaById($id);
		$data["titulo"] = "Edição do Estudante";
		$data["tipo"] = "estudantes";
		$this->load->view('direcao/cadastros/estudantes/estudantes_edit.php', $data);   
	}

	public function editar_admins($id)
    {
		verificar_sessao();
		$data["dados"] = get_pessoaById($id);
		$data["titulo"] = "Edição do Administrador";
		$data["tipo"] = "admins";
		$this->load->view('direcao/cadastros/pessoa_edit.php', $data);   
	}

	public function editar_professores($id)
    {
		verificar_sessao();
		$data["dados"] = get_pessoaById($id);
		$data["titulo"] = "Edição do Professor";
		$data["tipo"] = "professores";
		$this->load->view('direcao/cadastros/pessoa_edit.php', $data);   
	}

	public function editar_encarregados($id)
    {
		verificar_sessao();
		$data["dados"] = get_pessoaById($id);
		$data["titulo"] = "Edição do Encarregado";
		$data["tipo"] = "encarregados";
		$this->load->view('direcao/cadastros/pessoa_edit.php', $data);   
	}

	public function estudantes_add()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$_nProc = get_newNProc();
		$idEstudante = $this->mdlGeral->set_estudantes($_POST["nome"], $_POST["morada"], $_POST["genero"],$_POST["dtnascimento"],$_POST["bi"],$_POST["bi_emissao"],$_POST["bi_local"],$_POST["bi_expiracao"],$_POST["email"],$_POST["telemovel"],$_nProc,$_POST["pai"],$_POST["mae"], $_POST["tipo_doc"]);
		$this->mdlGeral->set_turmaPessoa($idEstudante, $_POST["_turma"]);

        redirect(base_url()."direcao/lista_estudantes");
    }

	public function pessoa_update($op = "estudantes")
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$pai = (isset($_POST["pai"])) ? $_POST["pai"] : "";
		$mae = (isset($_POST["mae"])) ? $_POST["mae"] : "";
		$nproc = (isset($_POST["nproc"])) ? $_POST["nproc"] : "";
		$idEstudante = $this->mdlGeral->update_pessoa($_POST["id"], $_POST["nome"], $_POST["morada"], $_POST["genero"],$_POST["dtnascimento"],$_POST["bi"],$_POST["bi_emissao"],$_POST["bi_local"],$_POST["bi_expiracao"],$_POST["email"],$_POST["telemovel"],$nproc, $pai, $mae, $_POST["tipo_doc"]);
        redirect(base_url()."direcao/lista_".$op);
    }

	public function lista_admins()
    {
		verificar_sessao();
		$this->load->model("Mdl_admins", "mdlAdmins");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlAdmins->get_allAdmins($filtro);
        $this->load->view('direcao/cadastros/admins/admins.php', $data);
	}

	public function cadastrar_admins()
    {
		verificar_sessao();
		$this->load->model("Mdl_admins", "mdlAdmins");
		$this->load->view('direcao/cadastros/admins/admins_add.php');   
	}

	public function admins_add()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->set_admins($_POST["nome"], $_POST["morada"], $_POST["genero"],$_POST["dtnascimento"],$_POST["bi"],$_POST["bi_emissao"],$_POST["bi_local"],$_POST["bi_expiracao"],$_POST["email"],$_POST["telemovel"], $_POST["tipo_doc"]);
        redirect(base_url()."direcao/lista_admins");
	}

	public function lista_professores()
    {
		verificar_sessao();
		$this->load->model("Mdl_professores", "mdlProfessores");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlProfessores->get_allProfessores($filtro);
        $this->load->view('direcao/cadastros/professores/professores.php', $data);
	}

	public function cadastrar_professores()
    {
		verificar_sessao();
			$this->load->model("Mdl_estudantes", "mdlEstudantes");
			$this->load->view('direcao/cadastros/professores/professores_add.php');   
	}

	public function cadastrar_encarregados()
    {
		verificar_sessao();
			$this->load->model("Mdl_estudantes", "mdlEstudantes");
			$this->load->view('direcao/cadastros/encarregados/encarregados_add.php');   
	}

	public function professores_add()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->set_professores($_POST["nome"], $_POST["morada"], $_POST["genero"],$_POST["dtnascimento"],$_POST["bi"],$_POST["bi_emissao"],$_POST["bi_local"],$_POST["bi_expiracao"],$_POST["email"],$_POST["telemovel"], $_POST["tipo_doc"]);
        redirect(base_url()."direcao/lista_professores");
	}

	public function encarregados_add()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->set_encarregados($_POST["nome"], $_POST["morada"], $_POST["genero"],$_POST["dtnascimento"],$_POST["bi"],$_POST["bi_emissao"],$_POST["bi_local"],$_POST["bi_expiracao"],$_POST["email"],$_POST["telemovel"], $_POST["tipo_doc"]);
        redirect(base_url()."direcao/lista_encarregados");
	}
	
	public function delete_pessoa_modal($id, $op = '')
	{
		verificar_sessao();
		$data["_id"] = $id;
		$data["_op"] = $op;
		$this->load->view("direcao/cadastros/pessoa_modal_delete.php", $data);
	}
	
	public function delete_pessoa($id, $op)
	{
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->delete_pessoa($id);
		redirect(base_url()."direcao/lista_".$op);
	}

	public function lista_encarregados()
    {
		verificar_sessao();
		$this->load->model("Mdl_encarregados", "mdlEncarregados");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlEncarregados->get_allEncarregados($filtro);
        $this->load->view('direcao/cadastros/encarregados/encarregados.php', $data);
	}
	
	public function ano_lectivo()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlGeral->get_anoLectivo($filtro);
        $this->load->view('direcao/sistema/ano_lectivo/ano_lectivo.php', $data);
	}

	public function ano_lectivo_modal()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->load->view('direcao/sistema/ano_lectivo/ano_lectivo_add.php',$data);
	}

	public function ano_lectivo_add()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->set_anoLectivo($_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/ano_lectivo");
	}

	public function ano_lectivo_update()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->update_anoLectivo($_POST["_id"], $_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/ano_lectivo");
	}

	public function ano_lectivo_edit($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data["_id"] = $id;
		$data["_dados"] = $this->mdlGeral->get_anoLectivoById($id);
        $this->load->view('direcao/sistema/ano_lectivo/ano_lectivo_edit.php', $data);
	}
	
	public function ano_lectivo_delete_modal($id)
    {
		$data["_id"] = $id;
		$this->load->view('direcao/sistema/ano_lectivo/ano_lectivo_delete.php', $data);
    }
	
	public function ano_lectivo_delete($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->delete_ano_lectivo($id);
		redirect(base_url()."direcao/ano_lectivo");
    }
	
	public function cursos()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlGeral->get_cursos($filtro);
		$this->load->view('direcao/sistema/cursos/cursos.php', $data);
	}

	public function cursos_modal()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->load->view('direcao/sistema/cursos/cursos_add.php',$data);
	}

	public function cursos_add()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->set_cursos($_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/cursos");
	}

	public function cursos_update()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->update_cursos($_POST["_id"], $_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/cursos");
	}

	public function cursos_edit($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data["_id"] = $id;
		$data["_dados"] = $this->mdlGeral->get_cursosById($id);
		$this->load->view('direcao/sistema/cursos/cursos_edit.php', $data);
	}

	public function cursos_delete_modal($id)
    {
		$data["_id"] = $id;
		$this->load->view('direcao/sistema/cursos/cursos_delete.php', $data);
	}
	
	public function cursos_delete($id)
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->delete_cursos($id);
		redirect(base_url()."direcao/cursos");
	}

	public function turmas()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlGeral->get_turmas($filtro);
		$this->load->view('direcao/sistema/turmas/turmas.php', $data);
	}

	public function turma_add_aluno_modal($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$data["id"] = $id;
		$this->load->view('direcao/sistema/turmas/turmas_add_aluno.php',$data);
	}

	public function turma_add_aluno()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$disciplinas = (in_array("all", $_POST["_disc"])) ? json_encode(array("all")) : json_encode($_POST["_disc"]);
		
		foreach ($_POST["_aluno"] as $key => $value)
		{
			$this->mdlGeral->turmaAddAluno($value, $_POST["_id"], $disciplinas);
		}

		redirect(base_url()."direcao/turmas_pessoas/".$_POST["_id"]);
	}
	
	public function turma_edit_aluno_modal($idaluno, $id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$data["id"] = $id;
		$data["_aluno"] = $idaluno;
		$data["_turmaPessoa"] = $this->mdlGeral->get_turmaPessoa($idaluno);
		$this->load->view('direcao/sistema/turmas/turmas_edit_aluno.php',$data);
	}

	public function turma_save_edited_aluno()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$disciplinas = (in_array("all", $_POST["_disc"])) ? json_encode(array("all")) : json_encode($_POST["_disc"]);
		
		$this->mdlGeral->turmaUpdateAluno($_POST["_idTurmaPessoa"], $disciplinas);

		redirect(base_url()."direcao/turmas_pessoas/".$_POST["_id"]);
	}

	public function turma_add_professor_modal($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$data["id"] = $id;
		$this->load->view('direcao/sistema/turmas/turmas_add_prof.php',$data);
	}

	public function turma_add_professor()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$disciplinas = json_encode($_POST["_disc"]);
		$this->mdlGeral->turmaAddProfessor($_POST["_prof"], $_POST["_id"], $disciplinas);
		redirect(base_url()."direcao/turmas_pessoas/".$_POST["_id"]);
	}

	public function turma_edit_professor_modal($idprof, $id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$data["id"] = $id;
		$data["_prof"] = $idprof;
		$data["_turmaPessoa"] = $this->mdlGeral->get_turmaPessoa($idprof);
		$this->load->view('direcao/sistema/turmas/turmas_edit_prof.php',$data);
	}

	public function turma_save_edited_professor()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$disciplinas = json_encode($_POST["_disc"]);
		$this->mdlGeral->turmaUpdateProfessor($_POST["_idTurmaPessoa"], $disciplinas);
		redirect(base_url()."direcao/turmas_pessoas/".$_POST["_id"]);
	}

	public function turmas_modal()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->load->view('direcao/sistema/turmas/turmas_add.php',$data);
	}

	public function turmas_add()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$disciplinas = json_encode($_POST["_disciplinas"]);
		$this->mdlGeral->set_turmas($_POST["_nome"], $_POST["_descricao"], $_POST["_curso"], $_POST["_classe"], $_POST["_turno"], $_POST["_ano"], $disciplinas);
		redirect(base_url()."direcao/turmas");
	}

	public function turmas_update()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$disciplinas = json_encode($_POST["_disciplinas"]);
		$this->mdlGeral->update_turmas($_POST["_id"], $_POST["_nome"], $_POST["_descricao"], $_POST["_curso"], $_POST["_classe"], $_POST["_turno"], $_POST["_ano"], $disciplinas);
		redirect(base_url()."direcao/turmas");
	}

	public function turmas_pessoas($id)
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$data["_id"] = $id;
		$data["_dados"] = $this->mdlGeral->get_turmasById($id);
		$this->load->view('direcao/sistema/turmas/turmas_pessoas.php', $data);
	}

	public function turmas_edit($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data["_id"] = $id;
		$data["_dados"] = $this->mdlGeral->get_turmasById($id);
		$this->load->view('direcao/sistema/turmas/turmas_edit.php', $data);
	}

	public function turmas_delete_modal($id)
    {
		$data["_id"] = $id;
		$this->load->view('direcao/sistema/turmas/turmas_delete.php', $data);
	}
	
	public function delete_turmapessoa_modal($id, $op = '')
    {
		$data["_id"] = $id;
		$data["_op"] = $op;
		$this->load->view('direcao/sistema/turmas/turmapessoa_delete.php', $data);
	}
	
	public function delete_turmapessoa($id, $op = '')
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");

		switch ($op)
		{
			case 'all':
				$arr_pessoas = get_pessoasTurma($id, $tipo = 4);
				foreach ($arr_pessoas as $key => $value)
				{
					$this->mdlGeral->delete_turmapessoa($value);
				}
				break;

			case 'allprof':
				$arr_pessoas = get_pessoasTurma($id, $tipo = 3);
				foreach ($arr_pessoas as $key => $value)
				{
					$this->mdlGeral->delete_turmapessoa($value);
				}
				break;
			
			default:
				$this->mdlGeral->delete_turmapessoa($id);
				break;
		}
		
		redirect(base_url()."direcao/turmas_pessoas/".$id);
	}
	
	public function turmas_delete($id)
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->delete_turmas($id);
		redirect(base_url()."direcao/turmas");
	}

	public function disciplinas()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlGeral->get_disciplinas($filtro);
		$this->load->view('direcao/sistema/disciplinas/disciplinas.php', $data);
	}

	public function disciplinas_modal()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->load->view('direcao/sistema/disciplinas/disciplinas_add.php',$data);
	}

	public function disciplinas_add()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->set_disciplinas($_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/disciplinas");
	}

	public function disciplinas_update()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->update_disciplinas($_POST["_id"], $_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/disciplinas");
	}

	public function disciplinas_edit($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data["_id"] = $id;
		$data["_dados"] = $this->mdlGeral->get_disciplinasById($id);
		$this->load->view('direcao/sistema/disciplinas/disciplinas_edit.php', $data);
	}

	public function disciplinas_delete_modal($id)
    {
		$data["_id"] = $id;
		$this->load->view('direcao/sistema/disciplinas/disciplinas_delete.php', $data);
	}
	
	public function disciplinas_delete($id)
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->delete_disciplinas($id);
		redirect(base_url()."direcao/disciplinas");
	}
	
	public function classes()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlGeral->get_classes($filtro);
		$this->load->view('direcao/sistema/classes/classes.php', $data);
	}

	public function classes_modal()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->load->view('direcao/sistema/classes/classes_add.php',$data);
	}

	public function classes_add()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->set_classes($_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/classes");
	}

	public function classes_update()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->update_classes($_POST["_id"], $_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/classes");
	}

	public function classes_edit($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data["_id"] = $id;
		$data["_dados"] = $this->mdlGeral->get_classeById($id);
		$this->load->view('direcao/sistema/classes/classes_edit.php', $data);
	}

	public function classes_delete_modal($id)
    {
		$data["_id"] = $id;
		$this->load->view('direcao/sistema/classes/classes_delete.php', $data);
	}
	
	public function classes_delete($id)
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->delete_classes($id);
		redirect(base_url()."direcao/classes");
	}

	public function turnos()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlGeral->get_turnos($filtro);
		$this->load->view('direcao/sistema/turnos/turnos.php', $data);
	}

	public function turnos_modal()
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->load->view('direcao/sistema/turnos/turnos_add.php',$data);
	}

	public function turnos_add()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->set_turnos($_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/turnos");
	}

	public function turnos_update()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$data = array();
		$this->mdlGeral->update_turnos($_POST["_id"], $_POST["_nome"], $_POST["_descricao"]);
		redirect(base_url()."direcao/turnos");
	}

	public function turnos_edit($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$data["_id"] = $id;
		$data["_dados"] = $this->mdlGeral->get_turnoById($id);
		$this->load->view('direcao/sistema/turnos/turnos_edit.php', $data);
	}

	public function turnos_delete_modal($id)
    {
		$data["_id"] = $id;
		$this->load->view('direcao/sistema/turnos/turnos_delete.php', $data);
	}
	
	public function turnos_delete($id)
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->delete_turnos($id);
		redirect(base_url()."direcao/turnos");
	}

	
	}