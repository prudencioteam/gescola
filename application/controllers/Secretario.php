<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secretario extends CI_Controller {

	public function index()
	{
		verificar_sessao();
		$this->load->view('home');
    }

    public function emitir_declaracao_sem_notas($_idp = "4404", $trimestre = '1')
    {
        $this->load->model("Mdl_geral", "mdlGeral");
        $dadosSessao = verificar_sessao();
        $myId = $dadosSessao["pessoa_id"];
        $dados = $this->mdlGeral->emitir_declaracao_sem_notas($_idp, $myId, $trimestre);
        redirect(base_url()."secretario/lista_declaracao_sem_notas");
    }

	public function declaracao_sem_notas($_idp = "4404", $idDoc = '1', $output = 'view')
	{
		verificar_sessao();
		$nproc = get_pessoaById($_idp)["pessoa_nProc"];
		$this->load->model("Mdl_geral", "mdlGeral");
		
		$dados = $this->mdlGeral->get_dadosEstudanteDeclaracao($nproc);

        $dadosTurma = get_turmaById($dados["turmaPessoa_turma"]);
        $a_classes = lista("classes");
		$dadosCurso = get_cursoById($dados["turmaPessoa_turma"]);
		$a_turno = lista("turnos");
        $a_meses = lista("meses");
        $a_tipoDoc = lista("tipoDoc");

include APPPATH."classes/TCPDF/tcpdf.php";

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, "A4", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nome do Secretario');
$pdf->SetTitle('Declaração Sem Notas');
$pdf->SetSubject('Declaração');
$pdf->SetKeywords('TCPDF, PDF, Declaração, Notas');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);


$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetFont('helvetica', '', 12);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
// add a page
$pdf->AddPage();

// Set some content to print
$tbl='

<table border="0">
<tr>
<td style="text-align: center; font-size: 10px;">
'.get_imgLogoInstituicao().'<br/>
REPÚBLICA DE ANGOLA<br/>
MINISTERIO DA EDUCACAO<br/>
'.get_nomeInstituicao().'<br/>																						
</td>
</tr>
</table>

<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>

<table>
<tr>
<td></td>
<td></td>
<td style="text-align: center;">
O Director Geral da Escola<br>_________________________<br>'.get_nomeDireitorGeral().'
</td>
</tr>
</table>


<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>


<h3 Align="center"><br>Declaração Sem Notas nº '.$idDoc.' / '.date("Y").' </h3>

<table><tr><td></td></tr></table>

<p align="justify" >Para efeitos julgados e convenientes, declara-se que <b>'.mb_strtoupper($dados["pessoa_nome"], "utf8").'</b>
 filho de '.$dados["pessoa_pai"].' e de '.$dados["pessoa_mae"].', portador do '.($a_tipoDoc[$dados["pessoa_docTipo"]]??"Documento de Identificação").' nº '.$dados["pessoa_numeroDoc"].', passado pelo Arquivo de 
 Identificação de '.$dados["pessoa_docEmitido"].', aos '.$dados["pessoa_docVal"].' sob o nº do processo '.$dados["pessoa_nProc"].' frequenta 
 o '.strtolower(($a_classes[$dadosTurma["turma_classeId"]]??"")).' do Curso de 
 '.$dadosCurso["curso_nome"].' na Turma '.$dadosTurma["turma_nome"].', em regime '.($a_turno[$dadosTurma["turma_turno"]]).', no ano lectivo de '.date("Y").'</p>
<p align="justify">Por ser verdade, e me ter sido solicitada, mandei pasar a presente Declaração, que vai por mim 
assinada e autenticada com o carimbo a óleo, em uso nesta Instituição de Ensino.</p>
<p></p>
<p align="left"><b>'.get_nomeInstituicao().' - LUANDA, AOS '.date("d").' DE '.mb_strtoupper($a_meses[(int)date("m")], "utf8").' DE '.date("Y").'</b></p>
<p></p><p></p>

<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>


<p align="center">Secretaria Académica</p>
<p align="center">______________________________</p>
';


// output the HTML content
$pdf->writeHTML($tbl, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// ---------------------------------------------------------

$filename = 'Declaração sem notas - '.$dados["pessoa_nome"].'.pdf';

//Close and output PDF document
if(in_array($output, array("download")))
{
    $pdf->Output($filename, 'D');
}
else
{
    $pdf->Output($filename, 'I');
}

//============================================================+
// END OF FILE
//============================================================+        
}	
    public function emitir_declaracao_com_notas($_idp = "4404", $trimestre = '1')
    {
        $trimestre = (int)$trimestre;
        $this->load->model("Mdl_geral", "mdlGeral");
        $dadosSessao = verificar_sessao();
        $myId = $dadosSessao["pessoa_id"];
        $dados = $this->mdlGeral->emitir_declaracao_com_notas($_idp, $myId, $trimestre);
        redirect(base_url()."secretario/lista_declaracao_com_notas");
    }

    public function declaracao_com_notas($_idp = '',  $idDoc = '1', $output = '')
  	{
        verificar_sessao();
		$nproc = get_pessoaById($_idp)["pessoa_nProc"];
		$this->load->model("Mdl_geral", "mdlGeral");
		
		$dados = $this->mdlGeral->get_dadosEstudanteDeclaracao($nproc);

        $dadosTurma = get_turmaById($dados["turmaPessoa_turma"]);
        $a_classes = lista("classes");
		$dadosCurso = get_cursoById($dados["turmaPessoa_turma"]);
		$a_turno = lista("turnos");
        $a_meses = lista("meses");
        
		verificar_sessao();
		$nproc = get_pessoaById($_idp)["pessoa_nProc"];
        $pessoaDados = get_pessoaByNproc($nproc);
        
        $_decDados = $this->mdlGeral->get_decComNotas($idDoc);
        $dt = date("Y", strtotime($_decDados["data_documento"]));
        
        $_a = get_alunoDeclaracaoComNotas($pessoaDados["pessoa_id"], $dt);
        
		
		$turmaDados = get_turmaById($dadosTurma["turma_id"]);

include APPPATH."classes/TCPDF/tcpdf.php";

$pdf = new TCPDF("P", PDF_UNIT, "A4", true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nome do Secretario');
$pdf->SetTitle('Declaração Com Notas');
$pdf->SetSubject('Declaração');
$pdf->SetKeywords('TCPDF, PDF, Declaração, Notas');
$pdf->SetMargins(15, 10, 15);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetFont('helvetica', '', 11);
$pdf->AddPage();

$a_turnos = lista("turnos");
$a_classes = lista("classes");
$a_cursos = lista("cursos");


$tbl='

<table border="0">
<tr>
<td style="text-align: center; font-size: 11px;">
'.get_imgLogoInstituicao().'<br/>
REPÚBLICA DE ANGOLA<br/>
MINISTERIO DA EDUCACAO<br/>
'.get_nomeInstituicao().'<br/>																						
</td>
</tr>
</table>

<h3 Align="center">Declaração Com Notas Nº '.$idDoc.' / 2019</h3>
<br>
<strong>Nº De Processo: </strong>'.$pessoaDados["pessoa_nProc"].'<br>
<span style="text-align: justify;"><strong>Nome: </strong>'.$pessoaDados["pessoa_nome"].'</span>
<br>
<span style="text-align: justify;"><strong>Curso: </strong>'.($a_cursos[$turmaDados["turma_curso"]]??"").'</span>
<br>
<table border="0">
<tr>
<td><strong>Classe: </strong> '.($a_classes[$turmaDados["turma_classeId"]]??"").'</td>
<td><strong>Turma: </strong>'.$turmaDados["turma_nome"].'</td>
<td><strong>Turno:</strong> '.($a_turnos[$turmaDados["turma_turno"]]??"").'</td>
<td><strong>Ano Lectivo:</strong> '.$turmaDados["turma_ano"].'</td>
</tr>
<br><br>


<table border="1" style="font-size: 11px; line-height: 20px;">

<tr>
<td><strong Align="center">Nº</strong></td>
<td><strong Align="center">DISCIPLINA</strong></td>
<td><strong Align="center">CLASSIFICAÇÃO</strong></td>
</tr>
';


$arr_notasFinais = array();
    //var_dump($_a);

    foreach ($_a as $key => $value)
    {
        if(!isset($arr_notasFinais[$value["avaliacao_disciplina"]]))
        {
            $arr_notasFinais[$value["avaliacao_disciplina"]] = array();
        }

        if(!isset($arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]]))
        {
            $arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]] = array();
        }

        array_push($arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]], $value["avaliacao_nota"]);
    }

    $arrNotas = array();

    foreach ($arr_notasFinais as $rkey => $rvalue)
    {
        $__disciplina = $rkey;
        $___arrMedia = array();
        foreach ($rvalue as $rk => $rv)
        {
            $__turma = $rk;
            $___soma = array_sum($rv);
            $___quantidade = sizeof($rv);
            $___media = $___soma/$___quantidade;
            array_push($___arrMedia, $___media);
        }
        
        $___somaT = array_sum($___arrMedia);
        $___quantidadeT = sizeof($___arrMedia);
        $___mediaD = $___somaT/$___quantidadeT;
        $arrNotas[$__disciplina] = $___mediaD;

    }

$a_disciplinas = lista("disciplinas");

$counter = 1;
foreach ($arrNotas as $key => $value)
{
  $color = ($value >= 10) ? "blue" : "red";
  $tbl .= '<tr>';

  $tbl .= '<td>'.($counter++).'</td>';

  $tbl .= '<td>'.$a_disciplinas[$key].'</td>';
  $tbl .= '<td style="color: '.$color.';">'.number_format($value, 2, ",", ".").'</td>';
  
  $tbl .= '</tr>';
}


$tbl .= '</table>

<table border="0"><tr><td></td></tr></table>
<table border="0"><tr><td></td></tr></table>
<table border="0"><tr><td></td></tr></table>

<table border="0" style="text-align: center;">
<tr>
<td>_______________________<br><br>O Responsável</td>
</tr>
</table>

';

// output the HTML content
$pdf->writeHTML($tbl, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// ---------------------------------------------------------

//Close and output PDF document
$filename = 'Declaração com notas - '.$dados["pessoa_nome"].'.pdf';

//Close and output PDF document
if(in_array($output, array("download")))
{
    $pdf->Output($filename, 'D');
}
else
{
    $pdf->Output($filename, 'I');
}

//============================================================+
// END OF FILE
//============================================================+       

}

public function declaracao_com_notas2($nproc = "4408")
{
    verificar_sessao();

	$this->load->model("Mdl_geral", "mdlGeral");
	$dados = $this->mdlGeral->get_dadosEstudanteDeclaracao($nproc);
	$dadosTurma = get_turmaById($dados["turmaPessoa_turma"]);
	$dadosCurso = get_cursoById($dados["turmaPessoa_turma"]);
	$a_turno = lista("turnos");
    $a_meses = lista("meses");
    $a_tipoDoc = lista("tipoDoc");

include APPPATH."classes/TCPDF/tcpdf.php";

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, "A4", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nome do Secretario');
$pdf->SetTitle('Declaração Sem Notas');
$pdf->SetSubject('Declaração');
$pdf->SetKeywords('TCPDF, PDF, Declaração, Notas');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetFont('helvetica', '', 12);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
require_once(dirname(__FILE__).'/lang/eng.php');
$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// add a page
$pdf->AddPage();

// Set some content to print
$tbl='

<table border="0">
<tr>
<td style="text-align: center; font-size: 10px;">
'.get_imgLogoInstituicao().'<br/>
REPÚBLICA DE ANGOLA<br/>
MINISTERIO DA EDUCACAO<br/>
INSTITUTO NACIONAL DE TELECOMUNICAÇÕES<br/>																						
</td>
</tr>
</table>

<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>

<table>
<tr>
<td></td>
<td></td>
<td style="text-align: center;">
O Director Geral da Escola<br>_________________________<br>{Nome Director da Escola}
</td>
</tr>
</table>

<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>

<h3 Align="center"><br>Declaração de Notas nº 522 / '.date("Y").' </h3>

<table><tr><td></td></tr></table>

<table>
<tr>
<td style="width: 5px; border: 0px solid #000;"></td>
<td style="line-height: 20px; margin-bottom: 0px;">
<b>PROCESSO Nº '.$dados["pessoa_nProc"].'</b><br>
<b>Nome:</b> <b>'.mb_strtoupper($dados["pessoa_nome"], "utf8").'</b>
filho de '.$dados["pessoa_pai"].' de '.$dados["pessoa_mae"].', portador do '.($a_tipoDoc[$dados["pessoa_docTipo"]]??"Documento de Identificação").' nº '.$dados["pessoa_numeroDoc"].', passado pelo Arquivo de 
Identificação de '.$dados["pessoa_docEmitido"].', aos '.get_formattedDate($dados["pessoa_docVal"]).'.
<br><br>
<b>Curso: '.$dadosCurso["curso_nome"].'</b>
</td>
</tr>
</table>

<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>
<table><tr><td></td></tr></table>


<p align="center">Secretaria Académica</p>
<p align="center">______________________________</p>
';

// output the HTML content
$pdf->writeHTML($tbl, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+        
}

public function emitir_boletim_de_notas($_idp = '', $turma, $trimestre = '1')
{
$this->load->model("Mdl_geral", "mdlGeral");
$dadosSessao = verificar_sessao();
$myId = $dadosSessao["pessoa_id"];
$dados = $this->mdlGeral->emitir_boletim($_idp, $myId, $turma, $trimestre);
redirect(base_url()."secretario/lista_boletim_notas");
}

public function boletim_de_notas($_idp = '', $turma, $trimestre = '1', $output = "")
{
    verificar_sessao();
$nproc = get_pessoaById($_idp)["pessoa_nProc"];
$pessoaDados = get_pessoaByNproc($nproc);
$turmaDados = get_turmaById($turma);
$dadosCurso = get_cursoById($turmaDados["turma_curso"]); 
$a_turno = lista("turnos");
 $_a = get_alunoAvaliacoesBoletim($turma, $pessoaDados["pessoa_id"], $trimestre);

include APPPATH."classes/TCPDF/tcpdf.php";

$pdf = new TCPDF("P", PDF_UNIT, array(170, 120), true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nome do Secretario');
$pdf->SetTitle('Declaração Sem Notas');
$pdf->SetSubject('Declaração');
$pdf->SetKeywords('TCPDF, PDF, Declaração, Notas');
$pdf->SetMargins(15, 10, 15);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetFont('helvetica', '', 10);
$pdf->AddPage();

$tbl='

<table border="0">
<tr>
<td style="text-align: center; font-size: 9px;">
'.get_imgLogoInstituicao().'<br/>
REPÚBLICA DE ANGOLA<br/>
MINISTERIO DA EDUCACAO<br/>
INSTITUTO NACIONAL DE TELECOMUNICAÇÕES<br/>																						
</td>
</tr>
</table>

<h3 Align="center">Boletim de Notas nº 522 / 2019 </h3>

<p><strong>Nome: </strong>'.$pessoaDados["pessoa_nome"].'
<br/>
<strong>Curso: </strong>'.$dadosCurso["curso_nome"].'
<br/>
<strong>Turma: </strong>'.$turmaDados["turma_nome"].' &nbsp; &nbsp; <strong>Trimestre: </strong>'.$trimestre.'º Trimestre &nbsp; <strong>Turno: </strong>Diurno</p>

<table border="1" style="font-size: 9px; line-height: 20px;">
<tr>
<td><strong><center>DISCIPLINA</center></strong></td>
<td><strong>MEDIA</strong></td>
</tr>
';

$notas = array();
foreach ($_a as $key => $value)
{
if(!isset($notas[$value["avaliacao_disciplina"]]))
{
  $notas[$value["avaliacao_disciplina"]]["notas"][1] = 0;
  $notas[$value["avaliacao_disciplina"]]["notas"][2] = 0;
  $notas[$value["avaliacao_disciplina"]]["notas"][3] = 0;
  $notas[$value["avaliacao_disciplina"]]["trimestre"] = $value["avaliacao_trimestre"];
}
else
{
  if($value["avaliacao_trimestre"] != $notas[$value["avaliacao_disciplina"]]["trimestre"]) continue;
}
$notas[$value["avaliacao_disciplina"]]["notas"][$value["avaliacao_tipo"]] = $value["avaliacao_nota"];
}

$a_disciplinas = lista("disciplinas");

foreach ($notas as $key => $value)
{
$media = ($value["notas"][1] + $value["notas"][2] + $value["notas"][3])/3;
$color = ($media >= 10) ? "blue" : "red";
$tbl .= '<tr>';

$tbl .= '<td>'.$a_disciplinas[$key].'</td>';
$tbl .= '<td style="color: '.$color.';">'.number_format($media, 2, ",", ".").'</td>';

$tbl .= '</tr>';
}

$tbl .= '</table>

<table border="0"><tr><td></td></tr></table>

<table border="0" style="text-align: center;">
<tr>
<td>_______________________<br><br>O Responsável</td>
</tr>
</table>

';

// output the HTML content
$pdf->writeHTML($tbl, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// ---------------------------------------------------------

$filename = 'Boletim '.$pessoaDados["pessoa_nome"].'.pdf';

//Close and output PDF document
if(in_array($output, array("download")))
{
$pdf->Output($filename, 'D');
}
else
{
$pdf->Output($filename, 'I');
}

//============================================================+
// END OF FILE
//============================================================+        
}

public function turma_boletim_de_notas($turma, $trimestre = '1', $output = "")
{
    verificar_sessao();

    $lista_estudantes = get_estudantesByTurma($turma);
    
    include APPPATH."classes/TCPDF/tcpdf.php";

    $pdf = new TCPDF("P", PDF_UNIT, array(170, 120), true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Nome do Secretario');
    $pdf->SetTitle('Declaração Sem Notas');
    $pdf->SetSubject('Declaração');
    $pdf->SetKeywords('TCPDF, PDF, Declaração, Notas');
    $pdf->SetMargins(15, 10, 15);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetFont('helvetica', '', 10);
    

    $iCount = 0;
    $turmaNome = '';
    $aAtual = date("Y");

    foreach ($lista_estudantes as $__idEst => $dadosEst)
    {
        $iCount ++;
        $_idp = $__idEst;
        $nproc = get_pessoaById($_idp)["pessoa_nProc"];
        $pessoaDados = get_pessoaByNproc($nproc);
        $turmaDados = get_turmaById($turma);
        $dadosCurso = get_cursoById($turmaDados["turma_curso"]); 
        $a_turno = lista("turnos");
        $_a = get_alunoAvaliacoesBoletim($turma, $pessoaDados["pessoa_id"], $trimestre);
        $turmaNome = $turmaDados["turma_nome"];
        $aAtual = $turmaDados["turma_ano"];

        
        $pdf->AddPage();

        $tbl ='

        <table border="0">
        <tr>
        <td style="text-align: center; font-size: 9px;">
        '.get_imgLogoInstituicao().'<br/>
        REPÚBLICA DE ANGOLA<br/>
        MINISTERIO DA EDUCACAO<br/>
        INSTITUTO NACIONAL DE TELECOMUNICAÇÕES<br/>																						
        </td>
        </tr>
        </table>

        <h3 Align="center">Boletim de Notas - '.$turmaDados["turma_nome"].' - '.substr((10000+$iCount), -3).' / 2019 </h3>

        <p><strong>Nome: </strong>'.$pessoaDados["pessoa_nome"].'
        <br/>
        <strong>Curso: </strong>'.$dadosCurso["curso_nome"].'
        <br/>
        <strong>Turma: </strong>'.$turmaDados["turma_nome"].' &nbsp; &nbsp; <strong>Trimestre: </strong>'.$trimestre.'º Trimestre &nbsp; <strong>Turno: </strong>Diurno</p>

        <table border="1" style="font-size: 9px; line-height: 20px;">
        <tr>
        <td><strong><center>DISCIPLINA</center></strong></td>
        <td><strong>MEDIA</strong></td>
        </tr>
        ';

        $notas = array();
        foreach ($_a as $key => $value)
        {
        if(!isset($notas[$value["avaliacao_disciplina"]]))
        {
        $notas[$value["avaliacao_disciplina"]]["notas"][1] = 0;
        $notas[$value["avaliacao_disciplina"]]["notas"][2] = 0;
        $notas[$value["avaliacao_disciplina"]]["notas"][3] = 0;
        $notas[$value["avaliacao_disciplina"]]["trimestre"] = $value["avaliacao_trimestre"];
        }
        else
        {
        if($value["avaliacao_trimestre"] != $notas[$value["avaliacao_disciplina"]]["trimestre"]) continue;
        }
        $notas[$value["avaliacao_disciplina"]]["notas"][$value["avaliacao_tipo"]] = $value["avaliacao_nota"];
        }

        $a_disciplinas = lista("disciplinas");

        foreach ($notas as $key => $value)
        {
        $media = ($value["notas"][1] + $value["notas"][2] + $value["notas"][3])/3;
        $color = ($media >= 10) ? "blue" : "red";
        $tbl .= '<tr>';

        $tbl .= '<td>'.$a_disciplinas[$key].'</td>';
        $tbl .= '<td style="color: '.$color.';">'.number_format($media, 2, ",", ".").'</td>';

        $tbl .= '</tr>';
        }

        $tbl .= '</table>

        <table border="0"><tr><td></td></tr></table>

        <table border="0" style="text-align: center;">
        <tr>
        <td>_______________________<br><br>O Responsável</td>
        </tr>
        </table>';

        // output the HTML content
        $pdf->writeHTML($tbl, true, false, true, false, '');
        
    }




    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // ---------------------------------------------------------

    $filename = 'Boletins_'.$turmaNome.'_'.$trimestre.'trimestre_'.$aAtual.'.pdf';

    //Close and output PDF document
    if(in_array($output, array("download")))
    {
        $pdf->Output($filename, 'D');
    }
    else
    {
        $pdf->Output($filename, 'I');
    }

//============================================================+
// END OF FILE
//============================================================+        
}

public function emitir_certificado($_idp = "4404", $trimestre = '1')
{
    $this->load->model("Mdl_geral", "mdlGeral");
    $dadosSessao = verificar_sessao();
    $myId = $dadosSessao["pessoa_id"];
    $dados = $this->mdlGeral->emitir_certificado($_idp, $myId, $trimestre);
    redirect(base_url()."secretario/lista_certificados");
}

public function certificado($_idp = '', $ndoc = '1', $output = 'view')
{
    $trimestre = '1';
    verificar_sessao();
	$nproc = get_pessoaById($_idp)["pessoa_nProc"];
    $pessoaDados = get_pessoaByNproc($nproc);
    $_a = get_alunoCertificado($pessoaDados["pessoa_id"]);
    
    $a_encarregados = lista("allEncarregados");

    $_ultimoAnoDoEstudante = date("Y");
    $_cursoDoEstudante = "1";


	include APPPATH."classes/TCPDF/tcpdf.php";
	
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, "A4", true, 'UTF-8', false);
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nome do Secretario');
	$pdf->SetTitle('Certificado de Conclusão de Curso');
	$pdf->SetSubject('Cerficado');
	$pdf->SetKeywords('TCPDF, PDF, Certificado, Notas');
	
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	
	$pdf->SetFont('helvetica', '', 12);
	
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}
	
	// ---------------------------------------------------------
	
	// add a page
    $pdf->AddPage();
    $a_tipoDoc = lista("tipoDoc");
	
	// Set some content to print
	$tbl='
	
	<table border="0">
	<tr>
	<td width="134px;"></td>
	<td style="text-align: center; font-size: 10px;" width="270px;">
	'.get_imgLogoInstituicao().'<br/>
	REPÚBLICA DE ANGOLA<br/>
	MINISTERIO DA EDUCACAO<br/>
	'.get_nomeInstituicao().'<br/>																						
	</td>
	<td width="134px;"></td>
	</tr>
    </table>
    
    <table border="0"><tr><td></td></tr></table>

    <h3 align="center">CERTIFICADO DE CONCLUSÃO DO CURSO Nº '.$ndoc.'</h3>

    <table border="0"><tr><td></td></tr></table>

	
    <p align="justify" >O Director do Instituto, '.get_nomeDireitorGeral().', e o Sub-director Pedagócico '.get_nomeDireitorPedagogico().', certificam de 
    acordo com o artº 25 e 17 dos Estatutos do Subsistema do Ensino Técnico Profissional, aprovado pelo Decreto nº 90/04, de 3 de 
    Dezembro de 2004, que, <strong>'.trim($pessoaDados["pessoa_nome"]).'</strong>, filho 
    de '.mb_strtoupper(($a_encarregados[$pessoaDados["pessoa_pai"]]??""), "utf8").' e de '.mb_strtoupper(($a_encarregados[$pessoaDados["pessoa_pai"]]??""), "utf8").', natural 
    de '.$pessoaDados["pessoa_naturalidade"].', Província de 
    '.$pessoaDados["pessoa_provincia"].', nascido aos '.date("d/m/Y", strtotime($pessoaDados["pessoa_dtNascimento"])).', portador do '.($a_tipoDoc[$pessoaDados["pessoa_docTipo"]]??"Documento de Identificação").' nº '.$pessoaDados["pessoa_numeroDoc"].', passado pelo Arquivo de Identificação 
    de Luanda aos '.date("d/m/Y", strtotime($pessoaDados["pessoa_docEmitido"])).', conclui em regime regular, no ano lectivo de '.$_ultimoAnoDoEstudante.' o curso '.$_cursoDoEstudante.' 
    tendo obtido aos seguintes classificações, conforme consta do processo individual nº '.$pessoaDados["pessoa_nProc"].' do 
    Livro de Termos:</p>

	<br><br><span><strong>DISCIPLINAS</strong></span><br>
	<table border="0" style="font-size:11px; line-height: 25px;">';

    $arr_notasFinais = array();
    //var_dump($_a);

    foreach ($_a as $key => $value)
    {
        if(!isset($arr_notasFinais[$value["avaliacao_disciplina"]]))
        {
            $arr_notasFinais[$value["avaliacao_disciplina"]] = array();
        }

        if(!isset($arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]]))
        {
            $arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]] = array();
        }

        array_push($arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]], $value["avaliacao_nota"]);
    }

    $arrNotas = array();

    foreach ($arr_notasFinais as $rkey => $rvalue)
    {
        $__disciplina = $rkey;
        $___arrMedia = array();
        foreach ($rvalue as $rk => $rv)
        {
            $__turma = $rk;
            $___soma = array_sum($rv);
            $___quantidade = sizeof($rv);
            $___media = $___soma/$___quantidade;
            array_push($___arrMedia, $___media);
        }
        
        $___somaT = array_sum($___arrMedia);
        $___quantidadeT = sizeof($___arrMedia);
        $___mediaD = $___somaT/$___quantidadeT;
        $arrNotas[$__disciplina] = $___mediaD;

    }

	$a_disciplinas = lista("disciplinas");

	$counter = 0;
	foreach ($arrNotas as $key => $value)
	{
		$counter++;
		$color = ($value >= 10) ? "blue" : "red";
		$tbl .= '<tr>';

		$tbl .= '<td style="width: 90px;">'.$a_disciplinas[$key].'</td>';
		$tbl .= '<td style="width: 400px;"> ............................................................................................................................</td>';
		$tbl .= '<td style="color: '.$color.';">'.number_format($value, 2, ",", ".").'</td>';
		
		$tbl .= '</tr>';
    }
    
    $a_meses = lista("meses");


	$tbl .= '</table>

    <p align="justify" >Pelo que, para efeitos legais e harmonia com a legislação em vigor, lhe mandamos passar o presente Certificado, que vai por nós assinado e autenticado pelo selo branco em uso nesta Escola.
    <br><br>Luanda, aos '.date("d").' de '.$a_meses[((int)date("m"))].' de '.date("Y").'</p>
    
    <table border="0"><tr><td></td></tr></table>
    <table border="0"><tr><td></td></tr></table>

    <table border="0" style="text-align: center;">
    <tr>
    <td width="230px">
    O Sub-Director Pedagógico<br>
    _____________________________<br>'.get_nomeDireitorPedagogico().'
    </td>

    <td width="79px"></td>

    <td width="230px">
    O Director da Escola<br>
    _____________________________<br>'.get_nomeDireitorGeral().'
    </td>
    </tr>
    </table>
	';
	
	// output the HTML content
	$pdf->writeHTML($tbl, true, false, true, false, '');
	
	
    $filename = 'Certificado - '.$pessoaDados["pessoa_nome"].'.pdf';

    //Close and output PDF document
    if(in_array($output, array("download")))
    {
        $pdf->Output($filename, 'D');
    }
    else
    {
        $pdf->Output($filename, 'I');
    }
	
	//============================================================+
	// END OF FILE
	//============================================================+	
	}

	public function disciplinas_turma($turma = "")
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $data["dados"] = $this->mdlGeral->get_Turma_byId($turma);
        $data["turma"] = $turma;
        $this->load->view('secretaria/disciplinas_turma.php',$data);
    }

	public function mostrar_turmas($turma = "")
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$AnoActual = Date('Y');
		$data["dados"] = $this->mdlGeral->get_turmasPorAno($AnoActual);
		$this->load->view('secretaria/turmas.php',$data);
	}

	public function listar_alunos_turma($turma = "", $pagina = '1')
    {
		verificar_sessao();
		
		$rPorPagina = 10;
		$pagina = ($pagina < 1) ? 1 : $pagina;

        $data["turma"] = $turma;
        $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
        $totalAlunos = get_alunosTurmaPorPagina_total($turma);
        
        $mnp = ($totalAlunos/$rPorPagina);
        $mnp = (($totalAlunos%$rPorPagina) > 0) ? ((int)$mnp + 1) : (int)$mnp;
        if($pagina > $mnp)
        {
            redirect(base_url()."secretario/listar_alunos_turma/".$turma."/".$mnp);
        }


        $tmpDados = get_alunosTurmaPorPagina($turma, ($pagina-1), $rPorPagina);
        $data["dadosTotal"] = $totalAlunos;
        $data["dados"] = array();
        
        if($filtro != "")
        {
            foreach ($tmpDados as $key => $value)
            {                
                $__nproc = (int)stripos("_".$value["pessoa_nProc"], $filtro);
                $__nome = (int)stripos("_".$value["pessoa_nome"], $filtro);
                
                if(($__nproc > 0) || ($__nome > 0))
                {
                    array_push($data["dados"], $value);
                }
            }
        }
        else
        {
            $data["dados"] = $tmpDados;
        }

        $data["turma_id"] = $turma;
	
        
		$data["pagina"] = $pagina;
        $data["rporPagina"] = $rPorPagina;
        $data["_filtro"] = $filtro;

		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('secretaria/listar_alunos.php', $data);
    }
    
    public function listar_alunos_turma_file($turma = "", $type = "pdf", $output = 'view',$search = '')
    {
		verificar_sessao();

        $data["turma"] = $turma;
        $filtro = $search;

        $a_turmas = lista("turmas");


        $tmpDados = get_alunosTurma($turma);
        $data["dados"] = array();
        
        if($filtro != "")
        {
            foreach ($tmpDados as $key => $value)
            {                
                $__nproc = (int)stripos("_".$value["pessoa_nProc"], $filtro);
                $__nome = (int)stripos("_".$value["pessoa_nome"], $filtro);
                
                if(($__nproc > 0) || ($__nome > 0))
                {
                    array_push($data["dados"], $value);
                }
            }
        }
        else
        {
            $data["dados"] = $tmpDados;
        }

        $data["turma_id"] = $turma;

        //=================================================================

        if($type == "pdf")
        {

            include APPPATH."classes/TCPDF/tcpdf.php";
            $pdf = new TCPDF("L", PDF_UNIT, "A4", true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Nome do Secretario');
            $pdf->SetTitle('Lista de Estudantes');
            $pdf->SetSubject('Declaração');
            $pdf->SetKeywords('TCPDF, PDF, Lista, Estudantes');
            $pdf->SetMargins(20, 10, 20, true);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetFont('helvetica', '', 12);
            $pdf->AddPage();
            $tbl='

            <table border="0">
            <tr>
            <td style="text-align: center; font-size: 10px;">
            '.get_imgLogoInstituicao().'<br/>
            REPÚBLICA DE ANGOLA<br/>
            MINISTERIO DA EDUCACAO<br/>
            '.get_nomeInstituicao().'<br/>																						
            </td>
            </tr>
            </table>

            <h3 Align="center"><br>LISTA DE ESTUDANTES <br>Turma: '.($a_turmas[$turma]??"").' </h3>
            <table><tr><td></td></tr></table>

            <table border="1" style="font-size: 11px; vertical-align: center; line-height: 25px;">
            <tr style="text-align:center; font-weight: bold;">
            <td width="40px">#</td>
            <td width="100px">N Proc</td>
            <td width="100px">Genero</td>
            <td width="380px">Nome</td>
            <td width="120px">Data de Nascimento</td>
            </tr>';

            $count = 0;

            $a_genero = array("M" => "Masculino", "F" => "Femenino");

            foreach ($data["dados"] as $key => $value)
            {
                $count++;
                $tbl .= '<tr>
                <td style="text-align: center;">'.$count.'</td>
                <td style="text-align: center;">'.$value["pessoa_nProc"].'</td>
                <td style="text-align: center;">'.((isset($a_genero[$value["pessoa_genero"]])? $a_genero[$value["pessoa_genero"]] : $value["pessoa_genero"])).'</td>
                <td>'.$value["pessoa_nome"].'</td>
                <td style="text-align: center;">'.$value["pessoa_dtNascimento"].'</td>
                </tr>';
            }

            $tbl .='</table>

            <table><tr><td></td></tr></table>';

            $pdf->writeHTML($tbl, true, false, true, false, '');

            $filename = 'lista_estudantes.pdf';

            //Close and output PDF document
            if(in_array($output, array("download")))
            {
                $pdf->Output($filename, 'D');
            }
            else
            {
                $pdf->Output($filename, 'I');
            }
            
        }
        else if($type == "xls")
        {

            include APPPATH."classes/PHPExcel/PHPExcel.php";

            $objPHPExcel = new PHPExcel();

            $_style_center = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

            $_style_bordered = array(
                'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
                )
            );

            $_style_bolded = array(
                'font'  => array(
                    'bold' => true
                ));
            
            $_style_unbold = array(
                'font'  => array(
                    'bold' => false
                ));

            $gdImage = imagecreatefrompng(APPPATH.'data/img/system/ensignia.png');
            
            $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
            $objDrawing->setName('insignia');$objDrawing->setDescription('insignia');
            $objDrawing->setImageResource($gdImage);
            $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
            $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
            $objDrawing->setHeight(80);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objDrawing->setCoordinates('C1');

            
            foreach (range("A", "Z") as $key => $value) {
                $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($value)->setWidth(5);
            }

            $objPHPExcel->getProperties()->setCreator("PRUNI SOFTWARE")
                                        ->setLastModifiedBy("PRUNI SOFTWARE")
                                        ->setTitle("Office 2007 XLSX")
                                        ->setSubject("Office 2007 XLSX")
                                        ->setDescription("Documento XLSX, gerado por PRUNI SOFTWARE.")
                                        ->setKeywords("office 2007 openxml pruni")
                                        ->setCategory("Lista");
            
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:E5');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:E6');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:E7');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A8:E8');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A9:E9');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A10:E10');

            $objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A10')->applyFromArray($_style_center);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle('A11:E11')->applyFromArray($_style_center);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle('C1')->applyFromArray($_style_center);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle('A11:E11')->applyFromArray($_style_bordered);
            $objPHPExcel->getActiveSheet()->getStyle('A11:E11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A5', 'REPÚBLICA DE ANGOLA');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A6', 'MINISTÉRIO DA EDUCAÇÃO');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A7', get_nomeInstituicao());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A8', 'SUB-DIRECÇÃO PEDAGÓGICA');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A9', 'LISTA DE ESTUDANTES');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A10', 'TURMA: A1');
            $objPHPExcel->setActiveSheetIndex(0)->getRowDimension(10)->setRowHeight(25);

            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(4);
            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setWidth(100);
            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('E')->setWidth(30);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A11', '#');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B11', 'N Proc');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C11', 'Nome Completo');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D11', 'Genero');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E11', 'Data de Nascimento');
            

            $_counter = 0;
            $_lineCounter = 11;
            $a_genero = array("M" => "Masculino", "F" => "Femenino");

            foreach ($data["dados"] as $key => $value)
            {
                $_counter++;
                $_lineCounter++;
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$_lineCounter, $_counter);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$_lineCounter, $value["pessoa_nProc"]);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$_lineCounter, $value["pessoa_nome"]);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$_lineCounter, ((isset($a_genero[$value["pessoa_genero"]])? $a_genero[$value["pessoa_genero"]] : $value["pessoa_genero"])));
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$_lineCounter, $value["pessoa_dtNascimento"]);
            }

            $objPHPExcel->getActiveSheet()->setTitle("saida xls");
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Lista_estudantes.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;
            
        }

	}

	/*===================== DOCUMENTOS ===================== */
	
	public function lista_boletim_notas()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
		$data["dados"] = $this->mdlGeral->get_AllBoletim($filtro);	
        $this->load->view('secretaria/boletim_de_notas.php', $data);
    }
    
	public function lista_tboletim_notas()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $AnoActual = Date('Y');
		$data["dados"] = $this->mdlGeral->get_turmasPorAno($AnoActual);
		$this->load->view('secretaria/boletim_turmas.php',$data);
    }

    public function turma_boletim($turma, $trimestre = '', $output = 'download')
    {
        $this->turma_boletim_de_notas($turma, $trimestre, $output);
    }
	
	public function boletim_modal()
	{
        $data["doc"] = "bol";
		$this->load->view('secretaria/boletim_modal_view.php');
	}
	
	public function deccnotas_modal()
	{
		$data["ocultar"] = "1";
        $data["doc"] = "dec";
		$this->load->view('secretaria/boletim_modal_view.php', $data);
	}
	
	public function decsnotas_modal()
	{
		$data["ocultar"] = "1";
        $data["doc"] = "dec";
		$this->load->view('secretaria/boletim_modal_view.php', $data);
	}
	
	public function certificado_modal()
	{
        $data["ocultar"] = "1";
        $data["doc"] = "certificado";
		$this->load->view('secretaria/boletim_modal_view.php', $data);
	}
	
	public function get_estudante_boletim($search = "")
	{
		$dados = lista("filtered_estudantes", $search);
		$dd = array();

        foreach ($dados as $key => $value)
        {
            if(temTurma($key)) $dd[$key] = $value;
        }
		// ob_clean();
		echo json_encode($dd);
	}
	
	public function get_estudante_certificado($search = "")
	{
        $dados = lista("filtered_estudantes", $search);
        $dd = array();

        foreach ($dados as $key => $value)
        {
            if(podeEmitirCertificado($key)) $dd[$key] = $value;
        }
		// ob_clean();
		echo json_encode($dd);
	}
	
	public function get_pturmas_boletim($idestudante = "")
	{
		$dados = lista("turma_estudante", $idestudante);
		// ob_clean();
		echo json_encode($dados);
	}

	public function cadastro_alunos()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('secretaria/cadastro_alunos.php');
    }
	
	public function lista_declaracao_sem_notas()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
        $data["dados"] = $this->mdlGeral->get_AllDecSemNotas($filtro);	
        $this->load->view('secretaria/declaracao_sem_notas.php', $data);
    }
	
	public function lista_declaracao_com_notas()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
        $data["dados"] = $this->mdlGeral->get_AllDecComNotas($filtro);
        $this->load->view('secretaria/declaracao_com_notas.php', $data);
    }
	
	public function lista_certificados()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $filtro = isset($_POST["_search"]) ? $_POST["_search"] : "";
        $data["dados"] = $this->mdlGeral->get_AllCertificados($filtro);
        $this->load->view('secretaria/certificado.php', $data);
	}

    public function documento_pedido_aceitar($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->documento_aceitar_pedido($id);
		redirect(base_url()."secretario/documentos_solicitados");
    }

    public function documento_pedido_negar($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->documento_recusar_pedido($id);
		redirect(base_url()."secretario/documentos_solicitados");
    }

    public function documento_pedido_apagar($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->documento_apagar($id);
		redirect(base_url()."secretario/documentos_solicitados");
    }

	public function aproveitamento_aluno()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('secretaria/aproveitamento_aluno.php');
	}

    public function nova_notificacao()
	{
		verificar_sessao();
		$this->load->view('secretaria/notificacao_new_modal.php');
	}
    
    public function notificacoes_enviadas()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $data["dados"] = $this->mdlGeral->get_allNotificacoes();
        $this->load->view('secretaria/notificacoes.php', $data);
    }
    
    public function notificacoes_recebidas()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('secretaria/notificacoes_recebidas.php');
    }
    
    public function add_notificacao()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $especific = json_encode($_POST["_group"]);
        $this->mdlGeral->add_notificacao($_POST["_title"], $_POST["_msg"], $_POST["_for"], $especific);
        redirect(base_url()."secretario/notificacoes_enviadas");
    }

    public function pauta_pdf($turma, $disciplina, $trimestre = "1", $output = "")
	{
		verificar_sessao();
        $this->load->model("Mdl_pauta", "mdlPauta");
        $this->mdlPauta->pdf($turma, $disciplina, $trimestre = "1", $output = "");
    }

    public function delete_documento_modal($tipo, $id)
    {
        $data["_id"] = $id;
        $data["_tipo"] = $tipo;
        $this->load->view('secretaria/documento_modal_delete.php', $data);
    }

    public function delete_documento($tipo, $id)
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $this->mdlGeral->delete_documento($id);

        $url = "";

        switch ($tipo) {
            case '1':
                $url = "secretario/lista_boletim_notas";
                break;
            case '2':
                $url = "secretario/lista_declaracao_sem_notas";
                break;
            case '3':
                $url = "secretario/lista_declaracao_com_notas";
                break;
            case '4':
                $url = "secretario/lista_certificados";
                break;
            
            default:
                # code...
                break;
        }
        redirect(base_url().$url);
    }

    public function documentos_solicitados()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
        $data["dados"] = $this->mdlGeral->get_docs_pedidos();	
        $this->load->view('secretaria/pedidos_documentos.php', $data);
     
    }

}
