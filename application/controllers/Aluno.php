<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aluno extends CI_Controller {

	public function index()
	{
		verificar_sessao();
		$this->load->view('home');
	}
	
	public function ano_lectivo()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('aluno/ano_lectivo.php');
	}
	
	public function horario()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('aluno/horario.php');
	}
	
	public function horario_pdf($turma, $output = 'download')
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->load->model("Mdl_horario", "mdlHorario");
        $this->mdlHorario->pdf($turma, $output);
	}
	
	public function notas($trimestre = '1')
    {
		verificar_sessao();
		$data["trimestre"] = $trimestre;
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('aluno/notas.php', $data);
	}

	public function aproveitamento()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('aluno/aproveitamento.php');
	}
	
	public function documentos()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
		$data["dados"] = $this->mdlGeral->get_todosDocumentos();
        $this->load->view('aluno/documentos.php',$data);
	}

	public function pedido_documento()
    {
		verificar_sessao();
		$this->load->model("Mdl_geral", "mdlGeral");
        $this->load->view('aluno/documentos_pedido_modal.php');
	}
	
	public function documento_pedido_add()
    {
		verificar_sessao();
        $this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->documento_pedido_add($_POST["_tipodoc"], $_POST["_efeito"], $_POST["_descricao"]);
		var_dump($this->mdlGeral);
		redirect(base_url()."aluno/documentos");
	}
	
	
	public function documento_pedido_delete($id)
    {
		$data["_id"] = $id;
		$this->load->view('aluno/documento_pedido_delete.php', $data);
    }
		
	public function pedido_delete($id)
    {
		$this->load->model("Mdl_geral", "mdlGeral");
		$this->mdlGeral->delete_documento($id);
		redirect(base_url()."aluno/documentos");
    }
}