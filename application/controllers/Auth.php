<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}
	
	public function login($param = "error")
	{
		$data["param"] = $param;
		$data["type"] = $this->input->cookie('loginError',true);

		$this->load->view('login', $data);
	}
	
	public function logout()
	{
		if(!isset($_SESSION))
		{
			session_start();
		}
		session_destroy();

		redirect(base_url()."auth/login","refresh");
	}

	public function password_recovery($op = "choose_method")
	{
		$data["type"] = array();

		switch ($op)
		{
			case 'choose_method':
				$this->load->view('password_recovery_method', $data);
				break;

			case 'sendmail':
				$this->load->view('password_recovery_email', $data);
				break;
			
			default:
				# code...
				break;
		}

	}

	public function recovery_sendmail()
	{
		$config = $this->config->config;
		var_dump($config);
		$this->load->library('email', $config);

		$this->email->from("pruni.project@gmail.com", 'Pruni Email Account');
		$this->email->subject("Recuperação de senha");

		$email = "tarciotuty@gmail.com";

		$this->email->to($email);


		$nomeUsuario = "Teste muito Bom";
		$nova_senha = "";

		$mensagem = '<div>
		<h2>WEBSITE AENEAS - Recupera&ccedil;&atilde;o de Senha</h2>
		<p>Caro utilizador,&nbsp;<br /> <br /> A seu pedido, enviamos a nova password de acesso ao nosso portal.</p>
		<p><strong>Utilizador:</strong> <a href="mailto:'.$email.'">'.$nomeUsuario.'</a></p>
		<p><strong>Senha:</strong> '.$nova_senha.' <span style="font-style: italic; color: #a0a0a0;">(gerada automaticamente)</span></p>
		<p>&Eacute; recomendado alterar a senha ap&oacute;s fazer o login</p>
		</div>';

		$this->email->message($mensagem);
		if($this->email->send())
		{
			echo "A sua senha foi enviada para o endereço";
		}
		else
		{
			echo "Ocorreu um erro, por favor, tente mais tarde";
			echo ($this->email->print_debugger());
		}
	}
	
	public function logar()
	{
		$url = $this->session->flashdata('init_url');


		if(isset($_POST["keepconnected"]))
		{
			$sck = $this->input->cookie('ci_session',true);
			$scookie= array('name'   => 'ci_session', 'value'  => $sck, 'expire' => 7776000);
			$this->input->set_cookie($scookie);
		}
		else
		{
			$sck = $this->input->cookie('ci_session',true);
			$scookie= array('name'   => 'ci_session', 'value'  => $sck, 'expire' => 7200);
			$this->input->set_cookie($scookie);
		}

		$this->load->helper('cookie');

		$this->load->model("Mdl_geral", "mdlGeral");
		if(!isset($_POST["user"]))
		{
			redirect(base_url()."admin/index","refresh");
		}
		
		$username=$_POST["user"];
		$password=$_POST["pass"];

		//$session_time = (isset($_POST['remember'])) ? 60*60*24*15 : 60*60*2;

		$dados = $this -> mdlGeral -> get_userdata_byLogin($username);
		
		if(empty($dados))
		{
			$cookie= array(
				'name'   => 'loginError',
				'value'  => base64_encode('1'.rand(100, 1000000)),
				'expire' => '60',
			);
			$this->input->set_cookie($cookie);

			redirect(base_url()."auth/login/error","refresh");//usuario inesistente
		}

		if($password != $dados['pessoa_password'])
		{
			$cookie= array(
				'name'   => 'loginError',
				'value'  => base64_encode('2'.rand(100, 1000000)),
				'expire' => '60',
			);
			$this->input->set_cookie($cookie);

			redirect(base_url()."auth/login/error","refresh"); //password invalida
		}
		
		if(!isset($_SESSION))
		{
			session_start();
		}

		$userdata = $dados;

		$this->session->set_userdata($userdata);

		if($url == "")
		{
			redirect(base_url(),"refresh");
		}
		else
		{
			redirect(base_url().$url,"refresh");
		}
	}
}
