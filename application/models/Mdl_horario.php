<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_horario extends CI_Model {

	public function pdf($turma, $output = "")
	{
		include APPPATH."classes/TCPDF/tcpdf.php";

        // create new PDF document
        $pdf = new TCPDF("LANDSCAPE", PDF_UNIT, "A4", true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Pruni');
        $pdf->SetTitle('Horario Escolar');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->SetFont('helvetica', '', 9);



        $meuid = $this->session->userdata("pessoa_id");
        $a_minhasTurmas = lista("turma_estudante", $meuid);

        $a_classes = lista("classes");
        $a_cursos = lista("cursos");
        $a_disciplinas = lista("disciplinas");

        $dadosTurma = get_turmaById($turma);
        $dadosHorario = get_horarioByTurma($dadosTurma["turma_id"]);
        $hmodelo = get_horarioModeloById($dadosHorario["hr_modelo"]);
        $dadosCurso = get_cursoById($dadosTurma["turma_curso"]);
        $dadosClasse = get_classeById($dadosTurma["turma_classeId"]);

        // -----------------------------------------------------------------------------

        $tbl = '
        <table border="0">
        <tr>
        <td></td>
        <td style="text-align: center;">
        <img src="'.base_url().'assets/images/insignia.png" width="70px" heigth="40px" /><br/>
        REPÚBLICA DE ANGOLA<br/>
        MINISTERIO DA EDUCACAO<br/>

        '.get_nomeInstituicao().'<br/>																						
        SUB-DIRECÇÃO PEDAGÓGICA<br/>																				
        <strong style="font-size: 15px; font-weight: bold;">HORÁRIO DA TURMA '.$dadosTurma["turma_nome"].' - '.mb_strtoupper($dadosClasse["classe_nome"], 'UTF-8').'</strong><br/>																						
        <span style="color: red; font-size: 15px; font-weight: bold;">CURSO: '.mb_strtoupper($dadosCurso["curso_nome"], 'UTF-8').'</span>

        
        </td>
        <td></td>
        </tr>
        </table>

        <table border="0" width="400px">
        <tr>
        <td></td>
        </tr>
        </table>';


        $tbl .= '
        <table border="0" width="400px">
        <tr>
        <td></td>
        </tr>
        </table>

        <table border="1" style="text-align: center; line-height: 25px;">
            <thead>
            <tr>
                <th style="color:#FFF; background-color:#339966; width: 95px;">Tempo</th>
                <th style="color:#FFF; background-color:#339966; width: 120px;">Segunda-Feira</th>
                <th style="color:#FFF; background-color:#339966; width: 50px;">Sala</th>
                <th style="color:#FFF; background-color:#339966; width: 120px;">Terça-Feira</th>
                <th style="color:#FFF; background-color:#339966; width: 50px;">Sala</th>
                <th style="color:#FFF; background-color:#339966; width: 120px;">Quarta-Feira</th>
                <th style="color:#FFF; background-color:#339966; width: 50px;">Sala</th>
                <th style="color:#FFF; background-color:#339966; width: 120px;">Quinta-Feira</th>
                <th style="color:#FFF; background-color:#339966; width: 50px;">Sala</th>
                <th style="color:#FFF; background-color:#339966; width: 120px;">Sexta-Feira</th>
                <th style="color:#FFF; background-color:#339966; width: 50px;">Sala</th>
            </tr>
            </thead>
            <tbody>';
            

            $a_modelo = json_decode($hmodelo["hmod_body"], TRUE);
            $a_dados = json_decode($dadosHorario["hr_body"], TRUE);

            foreach ($a_modelo as $key => $value)
            {
                $tbl .= '<tr>
                        <td style="color:#FFF; background-color:#339966; width: 95px;">'.$value["inicio"].' - '.$value["fim"].'</td>
                        <td style="width: 120px;">'.($a_disciplinas[$a_dados["seg_disciplina"][$key]]??"").'</td>
                        <td style="width: 50px;">'.$a_dados["seg_sala"][$key].'</td>
                        <td style="width: 120px;">'.($a_disciplinas[$a_dados["ter_disciplina"][$key]]??"").'</td>
                        <td style="width: 50px;">'.$a_dados["ter_sala"][$key].'</td>
                        <td style="width: 120px;">'.($a_disciplinas[$a_dados["qua_disciplina"][$key]]??"").'</td>
                        <td style="width: 50px;">'.$a_dados["qua_sala"][$key].'</td>
                        <td style="width: 120px;">'.($a_disciplinas[$a_dados["qui_disciplina"][$key]]??"").'</td>
                        <td style="width: 50px;">'.$a_dados["qui_sala"][$key].'</td>
                        <td style="width: 120px;">'.($a_disciplinas[$a_dados["sex_disciplina"][$key]]??"").'</td>
                        <td style="width: 50px;">'.$a_dados["sex_sala"][$key].'</td>
                    </tr>';
            }

            $tbl .='
            </tbody>
        </table>';

        

        $pdf->writeHTML($tbl, true, false, false, false, '');

        // -----------------------------------------------------------------------------

        $_sClass = (explode(" ", str_replace(array("º", "ª"), array("",""), $dadosClasse["classe_nome"]))[0]);
        //Close and output PDF document
        if(in_array($output, array("download")))
        {
            $pdf->Output('Mini-Pauta_'.$dadosTurma["turma_nome"].'_'.$_sClass.'_'.date("dmY").'_'.date("His").'.pdf', 'D');
        }
        else
        {
            $pdf->Output('Mini-Pauta_'.$dadosTurma["turma_nome"].'_'.$_sClass.'_'.date("dmY").'_'.date("His").'.pdf', 'I');
        }
		
	}
	
}
