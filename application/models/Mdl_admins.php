<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_admins extends CI_Model {

	public function get_allAdmins($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM pessoas where pessoa_usertipo = 1 AND pessoa_deleted = 0 AND (pessoa_nome LIKE('%".$filtro."%') OR pessoa_username LIKE('%".$filtro."%'))");
		return $query->result_array();
	}

}