<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_estudantes extends CI_Model {

	public function get_allEstudantes($filtro = '')
	{
		$flt_curso = '';
		if(isset($_POST["_curso"]))
		{
			if($_POST["_curso"] != "")
			{
				$flt_curso = "AND curso_id = '".$_POST["_curso"]."'";
			}
		}
		
		$flt_genero = '';
		if(isset($_POST["_genero"]))
		{
			if($_POST["_genero"] != "")
			{
				$flt_genero = "AND pessoa_genero = '".$_POST["_genero"]."'";
			}
		}
		
		$query = $this->db->query("SELECT * FROM pessoas 
		LEFT JOIN turmapessoas ON turmaPessoa_pessoa = pessoa_id
		LEFT JOIN turmas ON turmaPessoa_turma = turma_id
		LEFT JOIN cursos ON turma_curso = curso_id
		WHERE pessoa_usertipo = 4 AND pessoa_deleted = 0 AND turma_deleted = '0' AND curso_deleted = '0'
		AND (pessoa_nome LIKE('%".$filtro."%') OR pessoa_nProc LIKE('%".$filtro."%')) ".$flt_curso." ".$flt_genero."
		
		ORDER BY pessoa_nome ASC");

		$retorno = array();

		foreach ($query->result_array() as $key => $value)
		{
			$retorno[$value["pessoa_id"]] = $value;
		}
		return $retorno;
	}

}