<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_pauta extends CI_Model {

	public function pdf($turma, $disciplina, $trimestre = "1", $output = "")
	{
		include APPPATH."classes/TCPDF/tcpdf.php";

        $trimestreArray = ($trimestre == "all") ? array(1,2,3) : array($trimestre);

        $this->load->model("Mdl_geral", "mdlGeral");
        $dadosXLS = $this->mdlGeral->get_estudantesByTurma($turma, $trimestre, $disciplina);

        $dadosTurma = get_turmaById($turma);
        $dadosCurso = get_cursoById($dadosTurma["turma_curso"]);
        $dadosDisciplina = get_disciplinaById($disciplina);
        $dadosClasse = get_classeById($dadosTurma["turma_classeId"]);

        $a_turnos = lista("turnos");

        $dadosProfessor = get_profTurmaDisciplina($turma, $disciplina);

        $dadosLista = array();

        foreach ($dadosXLS as $key => $value)
        {
            $arr = array();
            $arr["dados_pessoa"] = $value;
            $arr["dados_nota"] = array();

            $arr["dados_nota"][1] = array();
            $arr["dados_nota"][2] = array();
            $arr["dados_nota"][3] = array();

            for ($i=1; $i <= 3; $i++)
            { 
                for ($k=1; $k <= 3; $k++)
                { 
                    $arr["dados_nota"][$i][$k] = "";
                }
            }
            
            if(count($trimestreArray) > 1)
            {
                $dadosAvaliacoes = get_alunoAvaliacoes_all($turma, $value["pessoa_id"], $disciplina);
            }
            else
            {
                $dadosAvaliacoes = get_alunoAvaliacoes($turma, $value["pessoa_id"], $disciplina, $trimestre);
            }

            foreach ($dadosAvaliacoes as $k => $v)
            {
                $arr["dados_nota"][$v["avaliacao_trimestre"]][$v["avaliacao_tipo"]] = $v["avaliacao_nota"];
            }
            array_push($dadosLista, $arr);
        }

        // create new PDF document
        $pdf = new TCPDF("LANDSCAPE", PDF_UNIT, "A4", true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('Mapa de Avaliação Trimestral');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->SetFont('helvetica', '', 8);

        // -----------------------------------------------------------------------------

        $tbl = '
        <table border="0">
        <tr>
        <td></td>
        <td style="text-align: center;">
        '.get_imgLogoInstituicao().'<br/>
        REPÚBLICA DE ANGOLA<br/>
        MINISTERIO DA EDUCACAO<br/>

        '.get_nomeInstituicao().'<br/>																						
        SUB-DIRECÇÃO PEDAGÓGICA<br/>																				
        <strong>MAPA DE AVALIAÇÃO TRIMESTRAL '.$dadosTurma["turma_ano"].'</strong><br/>																						
        <span style="color: red; font-size: 15px; font-weight: bold;">CURSO: '.mb_strtoupper($dadosCurso["curso_nome"], 'UTF-8').'</span>
        
        </td>
        <td></td>
        </tr>
        </table>

        <table border="0" width="400px">
        <tr>
        <td></td>
        </tr>
        </table>

        <table border="1" style="font-size: 10px; line-height: 17px;">
        <tr style="text-align: center;">
        <td colspan="4" style="width: 270px;">DISCIPLINA</td>
        <td colspan="2" style="width: 68px;">MASCULINO</td>
        <td colspan="2" style="width: 68px;">FEMENINO</td>
        <td colspan="2" rowspan="3" style="width:49px; line-height: 50px; color: red;">FALTAS</td>
        <td colspan="2" style="width: 68px;">MASCULINO</td>
        <td colspan="2" style="width: 68px;">FEMENINO</td>
        <td colspan="2" rowspan="3" style="width:49px; line-height: 50px; color: red;">FALTAS</td>
        <td colspan="2" style="width: 68px;">MASCULINO</td>
        <td colspan="2" style="width: 68px;">FEMENINO</td>
        <td colspan="2" rowspan="3" style="width:49px; line-height: 50px; color: red;">FALTAS</td>
        <td colspan="3" rowspan="3">
        <strong style="line-height: 20px; font-size: 18px;">'.$dadosTurma["turma_nome"].'</strong>
        <br>
        <strong style="line-height: 3px; font-size: 10px;">'.$dadosClasse["classe_nome"].'</strong>
        -
        <strong style="line-height: 3px; font-size: 10px;">'.($a_turnos[$dadosTurma["turma_turno"]]??"").'</strong>
        </td>
        </tr>

        <tr style="text-align: center;">
        <td colspan="4" rowspan="2" style="line-height:30px; font-size: 12px;"><b>'.$dadosDisciplina["disciplina_nome"].'</b></td>
        <td>POS.</td>
        <td>NEG.</td>
        <td>POS.</td>
        <td>NEG.</td>
        
        <td>POS.</td>
        <td>NEG.</td>
        <td>POS.</td>
        <td>NEG.</td>
        
        <td>POS.</td>
        <td>NEG.</td>
        <td>POS.</td>
        <td>NEG.</td>

        </tr>

        <tr style="text-align: center;">
        
        <td>12</td>
        <td>10</td>
        <td>10</td>
        <td>05</td>
       
        <td>12</td>
        <td>10</td>
        <td>10</td>
        <td>05</td>

        <td>12</td>
        <td>10</td>
        <td>10</td>
        <td>05</td>
        </tr>

        <tr style="text-align: center;">
        <td rowspan="3" style="width: 25px; line-height: 50px;">Nº</td>
        <td rowspan="3" style="width: 170px; line-height: 50px;">Nome</td>
        <td rowspan="3" style="width: 50px; line-height: 50px;">PROC.</td>
        <td rowspan="3" style="width: 25px;">&nbsp;</td>
        <td colspan="18">CLASSIFICAÇÃO DE FREQUÊNCIA (C.F.)</td>
        <td colspan="3">&nbsp;</td>
        
        </tr>

        <tr style="text-align: center;">
        <td colspan="4">I TRIMESTRE</td>
        <td rowspan="2"></td>
        <td rowspan="2"></td>
        <td colspan="4">I TRIMESTRE</td>
        <td rowspan="2"></td>
        <td rowspan="2"></td>
        <td colspan="4">I TRIMESTRE</td>
        <td rowspan="2"></td>
        <td rowspan="2"></td>
        <td style="width: 30px;">60%</td>
        <td rowspan="2" style="width: 53px; line-height: 35px;">OBS</td>
        <td rowspan="2" style="width: 30px; line-height: 25px;">Nº</td>
        </tr>

        <tr style="text-align: center;">
        <td>1P</td>
        <td>2P</td>
        <td>MP</td>
        <td>MTI</td>
        <td>1P</td>
        <td>2P</td>
        <td>MP</td>
        <td>MTII</td>
        <td>1P</td>
        <td>2P</td>
        <td>MP</td>
        <td>MTIII</td>
        <td>CF</td>
        </tr>';

        $_counter = 0;

        foreach ($dadosLista as $key => $value)
        {
            $_counter++;

            $t1[1] = (float)$value["dados_nota"][1][1];
            $t1[2] = (float)$value["dados_nota"][1][2];
            $t1[3] = (float)$value["dados_nota"][1][3];

            $t2[1] = (float)$value["dados_nota"][2][1];
            $t2[2] = (float)$value["dados_nota"][2][2];
            $t2[3] = (float)$value["dados_nota"][2][3];

            $t3[1] = (float)$value["dados_nota"][3][1];
            $t3[2] = (float)$value["dados_nota"][3][2];
            $t3[3] = (float)$value["dados_nota"][3][3];

            $media1 = ((float)$value["dados_nota"][1][1] + (float)$value["dados_nota"][1][2] + (float)$value["dados_nota"][1][3])/3;
            $media2 = ((float)$value["dados_nota"][2][1] + (float)$value["dados_nota"][2][2] + (float)$value["dados_nota"][2][3])/3;
            $media3 = ((float)$value["dados_nota"][3][1] + (float)$value["dados_nota"][3][2] + (float)$value["dados_nota"][3][3])/3;


            $tbl .= '<tr style="text-align: center;">
            <td>'.$_counter.'</td>
            <td style="text-align: left;">'.$value["dados_pessoa"]["pessoa_nome"].'</td>
            <td>'.$value["dados_pessoa"]["pessoa_nProc"].'</td>
            <td>'.$value["dados_pessoa"]["pessoa_genero"].'</td>
            <td style="color: '.(($t1[1] < 10) ? "red" : "blue").';">'.$t1[1].'</td>
            <td style="color: '.(($t1[2] < 10) ? "red" : "blue").';">'.$t1[2].'</td>
            <td style="color: '.(($t1[3] < 10) ? "red" : "blue").';">'.$t1[3].'</td>
            <td style="color: '.(($media1 < 10) ? "red" : "blue").';">'.number_format($media1, 1, ",", ".").'</td>
            <td></td>
            <td></td>
            <td style="color: '.(($t2[1] < 10) ? "red" : "blue").';">'.$t2[1].'</td>
            <td style="color: '.(($t2[2] < 10) ? "red" : "blue").';">'.$t2[2].'</td>
            <td style="color: '.(($t2[3] < 10) ? "red" : "blue").';">'.$t2[3].'</td>
            <td style="color: '.(($media2 < 10) ? "red" : "blue").';">'.number_format($media2, 1, ",", ".").'</td>
            <td></td>
            <td></td>
            <td style="color: '.(($t3[1] < 10) ? "red" : "blue").';">'.$t3[1].'</td>
            <td style="color: '.(($t3[2] < 10) ? "red" : "blue").';">'.$t3[2].'</td>
            <td style="color: '.(($t3[3] < 10) ? "red" : "blue").';">'.$t3[3].'</td>
            <td style="color: '.(($media3 < 10) ? "red" : "blue").';">'.number_format($media3, 1, ",", ".").'</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>'.$_counter.'</td>
            </tr>';
        }

        $_nomeProfessor = isset($dadosProfessor[0]) ? $dadosProfessor[0]["pessoa_nome"] : "undefined";
        
        $tbl .='        
        </table>

        <table border="0"><tr><td></td></tr></table>
        <table border="0"><tr><td></td></tr></table>
        <table border="0"><tr><td></td></tr></table>
        <table border="0"><tr><td></td></tr></table>
        <table border="0"><tr><td></td></tr></table>
        <table border="0"><tr><td></td></tr></table>
        <table border="0"><tr><td></td></tr></table>

        <table border="0" style="line-height: 25px;">
        <tr>
        <td>
        O PROFESSOR<br>
        <span style="font-size: 18px; color: blue;"><strong>'.$_nomeProfessor.'</strong></span><br>
        <strong>(NOME COMPLETO)</strong>
        </td>
        <td></td>
        <td style="text-align: center;">O COORDENADOR DA ÁREA DE FORMAÇÃO</td>
        </tr>
        </table>';

        $_xInicial = $pdf->GetX();
        $_yInicial = $pdf->GetY();

        $pdf->StartTransform();
        $pdf->SetXY(85, 87);
        $pdf->Rotate(90);
        $pdf->Cell(10,4,'SEXO',0,0,'L',0,'');
        $pdf->StopTransform();

        $pdf->SetFont('helvetica', '', 6);

        $pdf->StartTransform();
        $pdf->SetXY(131, 89);
        $pdf->Rotate(90);
        $pdf->SetTextColor(0, 0, 255);
        $pdf->Cell(10,4,'JUSTIF',0,0,'L',0,'');
        $pdf->StopTransform();

        $pdf->StartTransform();
        $pdf->SetXY(138, 89);
        $pdf->Rotate(90);
        $pdf->SetTextColor(255, 0, 0);
        $pdf->Cell(10,4,'INJUST',0,0,'L',0,'');
        $pdf->StopTransform();

        $pdf->StartTransform();
        $pdf->SetXY(183, 89);
        $pdf->Rotate(90);
        $pdf->SetTextColor(0, 0, 255);
        $pdf->Cell(10,4,'JUSTIF',0,0,'L',0,'');
        $pdf->StopTransform();

        $pdf->StartTransform();
        $pdf->SetXY(190, 89);
        $pdf->Rotate(90);
        $pdf->SetTextColor(255, 0, 0);
        $pdf->Cell(10,4,'INJUST',0,0,'L',0,'');
        $pdf->StopTransform();

        $pdf->StartTransform();
        $pdf->SetXY(235, 89);
        $pdf->Rotate(90);
        $pdf->SetTextColor(0, 0, 255);
        $pdf->Cell(10,4,'JUSTIF',0,0,'L',0,'');
        $pdf->StopTransform();

        $pdf->StartTransform();
        $pdf->SetXY(242, 89);
        $pdf->Rotate(90);
        $pdf->SetTextColor(255, 0, 0);
        $pdf->Cell(10,4,'INJUST',0,0,'L',0,'');
        $pdf->StopTransform();

        $varVisto = '<strong>&nbsp;&nbsp;&nbsp;Visto<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        O Sub-Director Pedagógico<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.get_nomeDireitorPedagogico().'</strong>';

        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(40, 25);
        $pdf->writeHTML($varVisto, true, false, false, false, '');

        $pdf->SetXY($_xInicial, $_yInicial);

        $pdf->writeHTML($tbl, true, false, false, false, '');

        // -----------------------------------------------------------------------------

        $_sClass = (explode(" ", str_replace(array("º", "ª"), array("",""), $dadosClasse["classe_nome"]))[0]);
        //Close and output PDF document
        if(in_array($output, array("download")))
        {
            $pdf->Output('Mini-Pauta_'.$dadosTurma["turma_nome"].'_'.$_sClass.'_'.date("dmY").'_'.date("His").'.pdf', 'D');
        }
        else
        {
            $pdf->Output('Mini-Pauta_'.$dadosTurma["turma_nome"].'_'.$_sClass.'_'.date("dmY").'_'.date("His").'.pdf', 'I');
        }
		
	}
	
}
