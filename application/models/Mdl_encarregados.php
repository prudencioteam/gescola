<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_encarregados extends CI_Model {

	public function get_allEncarregados($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM pessoas where pessoa_usertipo = 5 AND pessoa_deleted = 0 AND (pessoa_nome LIKE('%".$filtro."%') OR pessoa_username LIKE('%".$filtro."%'))");
		return $query->result_array();
	}

}