<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_geral extends CI_Model {

	public function get_userdata_byLogin($login)
	{
		$query = $this->db->query("SELECT * FROM pessoas WHERE pessoa_username = '$login' OR pessoa_nProc = '$login'");
		return $query->row_array();
	}

	public function get_dadosEstudanteDeclaracao($nproc)
	{
		$query = $this->db->query("SELECT * FROM pessoas
		LEFT JOIN turmapessoas ON turmaPessoa_pessoa = pessoa_id
		WHERE pessoa_nProc = '$nproc' ORDER BY turmaPessoa_turma DESC");
		return $query->row_array();
	}

	public function get_userdata_byId($id)
	{
		$query = $this->db->query("SELECT * FROM pessoas WHERE pessoa_id = '$id'");
		return $query->row_array();
	}
	
	public function get_Turma_byId($id)
	{
		$query = $this->db->query("SELECT * FROM turmas WHERE turma_id = '$id' and turma_deleted = 0");
		return $query->row_array();
	}

	public function update_password($idPessoa, $senha)
	{
		$this->db->query("UPDATE pessoas SET pessoa_password = '".$senha."' WHERE pessoa_id = '".$idPessoa."'");
	}

	public function get_estudantesByTurma($turma, $trimestre = "", $disciplina = '')
	{
		$query = $this->db->query("SELECT * FROM turmapessoas 
		LEFT JOIN pessoas ON pessoa_id = turmaPessoa_pessoa
		LEFT JOIN turmas ON turma_id = turmaPessoa_turma
		WHERE turmaPessoa_deleted = '0' AND pessoa_deleted = '0' AND turma_deleted = '0' AND turmaPessoa_turma = '$turma' AND pessoa_usertipo = '4'");
		$result = $query->result_array();
		$retorno = array();

		if($disciplina != '')
		{
			foreach ($result as $key => $value)
			{
				$discList = json_decode($value["turmaPessoa_disciplinas"], TRUE);
				
				if(in_array("all", $discList) || in_array($disciplina, $discList))
				{
					$retorno[$key] = $value;
				}
			}
		}
		else
		{
			$retorno = $result;
		}
		
		return $retorno;
	}

	// -- INICIO ANO LECTIVO -- //
	public function get_anoLectivo($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM ano_lectivo WHERE ano_lectivo_deleted = '0' AND (ano_lectivo_nome LIKE('%".$filtro."%') OR ano_lectivo_descricao LIKE('%".$filtro."%'))");
		return $query->result_array();
	}

	public function get_anoLectivoById($id)
	{
		$query = $this->db->query("SELECT * FROM ano_lectivo WHERE ano_lectivo_id = '".$id."'");
		return $query->row_array();
	}

	public function set_turmaPessoa($idPessoa, $turma)
	{
		$query = $this->db->query("INSERT INTO turmapessoas(turmaPessoa_pessoa,turmaPessoa_turma,turmaPessoa_disciplinas)
		VALUES('".$idPessoa."', '".$turma."', '[\"all\"]')");
	}

	public function get_turmaPessoa($id)
	{
		$query = $this->db->query("SELECT * FROM turmapessoas WHERE turmaPessoa_id = '$id'");
		return $query->row_array();
	}

	public function turmaAddProfessor($idPessoa, $turma, $disciplina)
	{
		$query = $this->db->query("INSERT INTO turmapessoas(turmaPessoa_pessoa,turmaPessoa_turma,turmaPessoa_disciplinas)
		VALUES('".$idPessoa."', '".$turma."', '".$disciplina."')");
	}

	public function turmaUpdateProfessor($id, $disciplina)
	{
		$query = $this->db->query("UPDATE turmapessoas SET turmaPessoa_disciplinas = '".$disciplina."' WHERE turmaPessoa_id = '".$id."'");
	}

	public function turmaAddAluno($idPessoa, $turma, $disciplina)
	{
		$query = $this->db->query("INSERT INTO turmapessoas(turmaPessoa_pessoa,turmaPessoa_turma,turmaPessoa_disciplinas)
		VALUES('".$idPessoa."', '".$turma."', '".$disciplina."')");
	}

	public function turmaUpdateAluno($id, $disciplina)
	{
		$query = $this->db->query("UPDATE turmapessoas SET turmaPessoa_disciplinas = '".$disciplina."' WHERE turmaPessoa_id = '".$id."'");
	}

	public function set_anoLectivo($nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO ano_lectivo (ano_lectivo_nome, ano_lectivo_descricao, ano_lectivo_dataCriacao, ano_lectivo_userCriador)
		VALUES('".$nome."', '".$descricao."','".date("Y-m-d H:i:s")."', '".$meuid."');");
	}

	public function update_anoLectivo($id, $nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE ano_lectivo SET ano_lectivo_nome = '".$nome."', ano_lectivo_descricao = '".$descricao."'
		WHERE ano_lectivo_id = '".$id."'");
	}

	public function delete_ano_lectivo($id)
	{
		$query = $this->db->query("UPDATE ano_lectivo SET ano_lectivo_deleted = '1' WHERE ano_lectivo_id = '$id'");
	}
		// -- FIM ANO LECTIVO -- //
	
	public function delete_turmapessoa($id)
	{
		$query = $this->db->query("UPDATE turmapessoas SET turmaPessoa_deleted = '1' WHERE turmaPessoa_id = '$id'");
	}

	// -- INICIO CURSOS -- //
	public function get_cursos($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM cursos WHERE curso_deleted = '0' AND (curso_nome LIKE('%".$filtro."%') OR curso_descricao LIKE('%".$filtro."%'))");
		return $query->result_array();
	}

	public function set_cursos($nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO cursos (curso_nome, curso_descricao, curso_dataCriacao, curso_userCriador)
		VALUES('".$nome."', '".$descricao."','".date("Y-m-d H:i:s")."', '".$meuid."');");
	}

	public function get_cursosById($id)
	{
		$query = $this->db->query("SELECT * FROM cursos WHERE curso_id = '".$id."'");
		return $query->row_array();
	}

	public function update_cursos($id, $nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE cursos SET curso_nome = '".$nome."', curso_descricao = '".$descricao."'
		WHERE curso_id = '".$id."'");
	}

	public function delete_cursos($id)
	{
		$query = $this->db->query("UPDATE cursos SET curso_deleted = '1' WHERE curso_id = '$id'");
	}
	// -- FIM CURSOS --- //

	// -- INICIO TURMAS  --- //
	public function get_turmas($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM turmas WHERE turma_deleted = '0' AND (turma_nome LIKE('%".$filtro."%') OR turma_descricao LIKE('%".$filtro."%')) ORDER BY turma_id DESC");
		return $query->result_array();
	}

	public function set_turmas($nome, $descricao, $curso, $classe, $turno, $ano, $disc)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO turmas (turma_nome, turma_descricao, turma_dataCriacao, turma_userCriador, turma_turno, turma_curso, turma_classeId, turma_ano, turma_disciplinas)
		VALUES('".$nome."', '".$descricao."','".date("Y-m-d H:i:s")."', '".$meuid."', '".$turno."', '".$curso."', '".$classe."', '".$ano."', '".$disc."');");
	}

	public function get_turmasById($id)
	{
		$query = $this->db->query("SELECT * FROM turmas WHERE turma_id = '".$id."'");
		return $query->row_array();
	}

	public function update_turmas($id, $nome, $descricao, $curso, $classe, $turno, $ano, $disc)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE turmas SET turma_nome = '".$nome."', turma_descricao = '".$descricao."', turma_curso = '".$curso."', turma_classeId = '".$classe."', turma_turno = '".$turno."', turma_ano = '".$ano."', turma_disciplinas = '".$disc."'
		WHERE turma_id = '".$id."'");
	}

	public function delete_turmas($id)
	{
		$query = $this->db->query("UPDATE turmas SET turma_deleted = '1' WHERE turma_id = '$id'");
		$query2 = $this->db->query("UPDATE turmapessoas SET turmaPessoa_deleted = '1' WHERE turmaPessoa_turma = '$id'");
	}
	// -- CADASTRO DE CURSOS ---

	// -- INICIO DISCIPLINAS  --- //
	public function get_disciplinas($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM disciplinas WHERE disciplina_deleted = '0' AND (disciplina_nome LIKE('%".$filtro."%') OR disciplina_descricao LIKE('%".$filtro."%'))");
		return $query->result_array();
	}

	public function set_disciplinas($nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO disciplinas (disciplina_nome, disciplina_descricao, disciplina_dataCriacao, disciplina_userCriador)
		VALUES('".$nome."', '".$descricao."','".date("Y-m-d H:i:s")."', '".$meuid."');");
	}

	public function get_disciplinasById($id)
	{
		$query = $this->db->query("SELECT * FROM disciplinas WHERE disciplina_id = '".$id."'");
		return $query->row_array();
	}

	public function update_disciplinas($id, $nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE disciplinas SET disciplina_nome = '".$nome."', disciplina_descricao = '".$descricao."'
		WHERE disciplina_id = '".$id."'");
	}

	public function delete_disciplinas($id)
	{
		$query = $this->db->query("UPDATE disciplinas SET disciplina_deleted = '1' WHERE disciplina_id = '$id'");
	}
	// -- FIM DISCIPLINA ---

	// -- INICIO CLASSES  --- //
	public function get_classes($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM classes WHERE classe_deleted = '0' AND (classe_nome LIKE('%".$filtro."%') OR classe_descricao LIKE('%".$filtro."%'))");
		return $query->result_array();
	}

	public function set_classes($nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO classes (classe_nome, classe_descricao, classe_dataCriacao, classe_userCriador)
		VALUES('".$nome."', '".$descricao."','".date("Y-m-d H:i:s")."', '".$meuid."');");
	}

	public function get_classeById($id)
	{
		$query = $this->db->query("SELECT * FROM classes WHERE classe_id = '".$id."'");
		return $query->row_array();
	}

	public function update_classes($id, $nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE classes SET classe_nome = '".$nome."', classe_descricao = '".$descricao."'
		WHERE classe_id = '".$id."'");
	}

	public function delete_classes($id)
	{
		$query = $this->db->query("UPDATE classes SET classe_deleted = '1' WHERE classe_id = '$id'");
	}
	// -- FIM CLASSES ---

	// -- INICIO TURNOS  --- //
	public function get_turnos($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM turnos WHERE turno_deleted = '0' AND (turno_nome LIKE('%".$filtro."%') OR turno_descricao LIKE('%".$filtro."%'))");
		return $query->result_array();
	}

	public function set_turnos($nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO turnos (turno_nome, turno_descricao, turno_dataCriacao, turno_userCriador)
		VALUES('".$nome."', '".$descricao."','".date("Y-m-d H:i:s")."', '".$meuid."');");
	}

	public function get_turnoById($id)
	{
		$query = $this->db->query("SELECT * FROM turnos WHERE turno_id = '".$id."'");
		return $query->row_array();
	}

	public function update_turnos($id, $nome, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE turnos SET turno_nome = '".$nome."', turno_descricao = '".$descricao."'
		WHERE turno_id = '".$id."'");
	}

	public function delete_turnos($id)
	{
		$query = $this->db->query("UPDATE turnos SET turno_deleted = '1' WHERE turno_id = '$id'");
	}
	// -- FIM TURNOS ---

	public function set_estudantes($nome, $morada, $genero, $dtnascimento, $bi, $bi_emissao, $bi_local, $bi_expiracao ,$email, $telemovel, $nproc, $pai, $mae, $tipoDoc)
	{
		$password = get_defaultPassword();
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO pessoas (pessoa_nome,pessoa_morada,pessoa_genero,pessoa_dtNascimento,pessoa_numeroDoc,pessoa_docEmitido,pessoa_docLocal, pessoa_docExpiracao,pessoa_email,pessoa_nTel,pessoa_usertipo, pessoa_nProc, pessoa_password, pessoa_pai, pessoa_mae, pessoa_docTipo)
		VALUES('".$nome."','".$morada."','".$genero."','".$dtnascimento."','".$bi."','".$bi_emissao."','".$bi_local."','".$bi_expiracao."','".$email."','".$telemovel."','4', '".$nproc."', '".$password."', '".$pai."', '".$mae."', '".$tipoDoc."');");
		return $this->db->query("SELECT LAST_INSERT_ID() AS id")->row_array()["id"];
	}

	public function update_pessoa($id, $nome, $morada, $genero, $dtnascimento, $bi, $bi_emissao, $bi_local, $bi_expiracao ,$email, $telemovel, $nproc, $pai, $mae, $tipoDoc)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE pessoas SET pessoa_nome = '".$nome."', pessoa_morada = '".$morada."', pessoa_genero = '".$genero."',
		pessoa_dtNascimento = '".$dtnascimento."', pessoa_numeroDoc = '".$bi."', pessoa_docEmitido = '".$bi_emissao."', pessoa_docLocal = '".$bi_local."', 
		pessoa_docExpiracao = '".$bi_expiracao."', pessoa_email = '".$email."',pessoa_nTel = '".$telemovel."', pessoa_pai = '".$pai."', pessoa_mae = '".$mae."', pessoa_docTipo = '".$tipoDoc."'
		WHERE pessoa_id = '".$id."'");
	}

	public function set_admins($nome, $morada, $genero, $dtnascimento, $bi, $bi_emissao, $bi_local, $bi_expiracao ,$email, $telemovel, $tipoDoc)
	{
		$password = get_defaultPassword();
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO pessoas (pessoa_nome,pessoa_morada,pessoa_genero,pessoa_dtNascimento,pessoa_numeroDoc,pessoa_docEmitido,pessoa_docLocal, pessoa_docExpiracao,pessoa_email,pessoa_nTel,pessoa_usertipo, pessoa_username, pessoa_password, pessoa_docTipo)
		VALUES('".$nome."','".$morada."','".$genero."','".$dtnascimento."','".$bi."','".$bi_emissao."','".$bi_local."','".$bi_expiracao."','".$email."','".$telemovel."','1', '".$email."', '".$password."', '".$tipoDoc."');");
    }

	public function set_professores($nome, $morada, $genero, $dtnascimento, $bi, $bi_emissao, $bi_local, $bi_expiracao ,$email, $telemovel, $tipoDoc)
	{
		$password = get_defaultPassword();
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO pessoas (pessoa_nome,pessoa_morada,pessoa_genero,pessoa_dtNascimento,pessoa_numeroDoc,pessoa_docEmitido,pessoa_docLocal, pessoa_docExpiracao,pessoa_email,pessoa_nTel,pessoa_usertipo, pessoa_username, pessoa_password, pessoa_docTipo)
		VALUES('".$nome."','".$morada."','".$genero."','".$dtnascimento."','".$bi."','".$bi_emissao."','".$bi_local."','".$bi_expiracao."','".$email."','".$telemovel."','3', '".$email."', '".$password."', '".$tipoDoc."');");
    }

	public function set_encarregados($nome, $morada, $genero, $dtnascimento, $bi, $bi_emissao, $bi_local, $bi_expiracao ,$email, $telemovel, $tipoDoc)
	{
		$password = get_defaultPassword();
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO pessoas (pessoa_nome,pessoa_morada,pessoa_genero,pessoa_dtNascimento,pessoa_numeroDoc, pessoa_docEmitido,pessoa_docLocal, pessoa_docExpiracao,pessoa_nTel,pessoa_usertipo, pessoa_username, pessoa_password, pessoa_docTipo)
		VALUES('".$nome."','".$morada."','".$genero."','".$dtnascimento."','".$bi."','".$bi_emissao."','".$bi_local."','".$bi_expiracao."','".$telemovel."','5', '".$email."', '".$password."', '".$tipoDoc."');");
	}

	public function get_modeloHorarios($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM horario_modelos WHERE hmod_deleted = '0' AND hmod_titulo LIKE '%".$filtro."%'");
		return $query->result_array();
	}

	public function get_horarios()
	{
		$query = $this->db->query("SELECT * FROM horarios WHERE hr_deleted = '0'");
		return $query->result_array();
	}

	public function delete_pessoa($id)
	{
		$query = $this->db->query("UPDATE pessoas SET pessoa_deleted = '1' WHERE pessoa_id = '$id'");
	}

	public function delete_horario($id)
	{
		$query = $this->db->query("UPDATE horarios SET hr_deleted = '1' WHERE hr_id = '$id'");
	}

	public function delete_hmodelo($id)
	{
		$query = $this->db->query("UPDATE horario_modelos SET hmod_deleted = '1' WHERE hmod_id = '$id'");
	}

	public function get_horariosByYear($ano, $filtro = '')
	{
		$query = $this->db->query("SELECT * FROM horarios
		LEFT JOIN turmas ON hr_turma = turma_id
		LEFT JOIN horario_modelos ON hr_modelo = hmod_id
		WHERE turma_ano = '$ano' AND turma_deleted = 0 AND hr_deleted = 0 
		AND hmod_deleted = 0 AND turma_nome LIKE('%".$filtro."%') ");
		return $query->result_array();
	}

	public function get_horarioById($id)
	{
		$query = $this->db->query("SELECT * FROM horarios
		WHERE hr_id = '$id'");
		return $query->row_array();
	}

	public function save_horarios($turma, $modelo, $body)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO horarios (hr_turma,hr_modelo,hr_body,hr_dtCria,hr_userCria)
		VALUES('".$turma."', '".$modelo."', '".$body."', '".date("Y-m-d H:i:s")."', '".$meuid."');");
	}

	public function update_horario($id, $body)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE horarios SET hr_body = '".$body."' WHERE hr_id = '".$id."'");
	}

	public function get_modeloHorarioById($id)
	{
		$query = $this->db->query("SELECT * FROM horario_modelos WHERE hmod_id = '$id'");
		return $query->row_array();
	}
	
	public function get_todosDocumentos()
	{
		$id = $this->session->userdata("pessoa_id");
		$query = $this->db->query("SELECT * FROM documentos WHERE documento_deleted = '0' AND pedido_por =  '$id' ORDER BY id_documento DESC ");
		return $query->result_array();
	}

	public function get_docs_pedidos()
	{
		$id = $this->session->userdata("pessoa_id");
		$query = $this->db->query("SELECT * FROM documentos WHERE documento_deleted = '0' AND pedido_por <> 0 ORDER BY id_documento DESC ");
		return $query->result_array();
	}
	
	
	public function save_modeloHorarios($titulo, $body)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO horario_modelos (hmod_titulo,hmod_userCria,hmod_body,hmod_dtCria)
		VALUES('".$titulo."', '".$meuid."', '".$body."', '".date("Y-m-d H:i:s")."');");
	}

	public function documento_pedido_add($tipo, $efeito, $descricao)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO documentos (tipo_documento,efeito_documento,descricao_documento,data_pedido_documento,pedido_por, estado_documento)
		VALUES('".$tipo."', '".$efeito."', '".$descricao."', '".date("Y-m-d H:i:s")."', '".$meuid."','2');");
	}


	public function get_notificacaoById($id)
	{
		$query = $this->db->query("SELECT * FROM notificacoes WHERE notif_id = '$id'");
		return $query->row_array();
	}
	
	public function get_disciplinaTurma($turma)
	{
		$query = $this->db->query("SELECT * FROM turmapessoas 
		LEFT JOIN pessoas ON pessoa_id = turmaPessoa_pessoa
		WHERE turmaPessoa_deleted = '0' AND pessoa_deleted = '0' AND turmaPessoa_turma = '$turma' AND pessoa_usertipo = '4'");
		return $query->result_array();
	}
	
	public function delete_notificacao($id)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE notificacoes SET notif_deleted = '1' WHERE notif_criador = '$meuid' AND notif_id = '$id'");
	}
	
	public function hide_notificacao($id)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("UPDATE notificacoesvistas SET nvista_deleted = '1' WHERE nvista_pessoa = '$meuid' AND nvista_id = '$id'");
	}

	public function add_notificacao($titulo, $mensagem, $para, $paraespecifico)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO notificacoes(notif_titulo,notif_message,notif_dtCria,notif_for,notif_forEspecific,notif_criador)
		VALUES('".$titulo."','".$mensagem."','".date("Y-m-d H:i:s")."','".$para."','".$paraespecifico."','".$meuid."');");
	}

	public function add_notificacao_turma($titulo, $mensagem, $para, $paraespecifico, $turma)
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("INSERT INTO notificacoes(notif_titulo,notif_message,notif_dtCria,notif_for,notif_forEspecific,notif_criador, notif_turma)
		VALUES('".$titulo."','".$mensagem."','".date("Y-m-d H:i:s")."','".$para."','".$paraespecifico."','".$meuid."', '".$turma."');");
	}

	public function notif_markAsViewed($notif, $pessoa)
	{
		$retorno = 0;

		$_dd = $this->db->query("SELECT * FROM notificacoesvistas WHERE nvista_notif = '".$notif."' AND nvista_pessoa = '".$pessoa."'")->row_array();
		if(empty($_dd))
		{
			$query = $this->db->query("INSERT INTO notificacoesvistas(nvista_notif, nvista_pessoa)
			VALUES('".$notif."','".$pessoa."')");
			$retorno = $this->db->query("SELECT LAST_INSERT_ID() AS id")->row_array()["id"];
		}
		return $retorno;
	}

	public function get_allNotificacoes($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM notificacoes WHERE notif_deleted = '0' AND ((notif_titulo LIKE('%".$filtro."%')) OR (notif_message LIKE('%".$filtro."%')))");
		return $query->result_array();
	}

	public function get_outNotificacoes($filtro = '')
	{
		$meuid = $this->session->userdata("pessoa_id");
		$query = $this->db->query("SELECT * FROM notificacoes WHERE notif_deleted = '0' AND notif_criador = '$meuid' AND ((notif_titulo LIKE('%".$filtro."%')) OR (notif_message LIKE('%".$filtro."%')))");
		return $query->result_array();
	}

	public function get_inNotificacoes($filtro = '')
	{
		$userTipo = $this->session->userdata("pessoa_usertipo");
		$meuid = $this->session->userdata("pessoa_id");
		$mygroup = "all";

		switch ($userTipo)
		{
			case 3:
				$mygroup = "teachers";
				break;

			case 4:
				$mygroup = "students";
				break;
			
			default:
				$mygroup = "all";
				break;
		}

		$strQuery = "SELECT * FROM notificacoes 
		LEFT JOIN notificacoesvistas ON ((nvista_notif = notif_id) AND (nvista_pessoa = '".$meuid."'))
		WHERE notif_deleted = '0' AND notif_criador != '".$meuid."' AND ((notif_for = 'all') OR (notif_forEspecific LIKE('%".$meuid."%')) OR notif_for = '".$mygroup."') AND ((notif_titulo LIKE('%".$filtro."%')) OR (notif_message LIKE('%".$filtro."%'))) AND (nvista_deleted = 0 OR nvista_deleted IS NULL)";
		$query = $this->db->query($strQuery);
		
		return $query->result_array();
	}

	public function emitir_boletim($userDoc, $emitidoPor, $turma, $trimestre)
	{
		$query = $this->db->query("INSERT INTO documentos(nome_documento,tipo_documento,usuario_documento,emitido_por, pedido_por, data_documento, turma_documento, trimestre_documento)
		VALUES('BOLETIM DE NOTAS', 1 , '".$userDoc."', '".$emitidoPor."', 0 , '".date("Y-m-d")."', '".$turma."', '".$trimestre."')");
	}

	public function emitir_declaracao_sem_notas($userDoc, $emitidoPor, $trimestre)
	{
		$trimestre = (int)$trimestre;
		$query = $this->db->query("INSERT INTO documentos(nome_documento,tipo_documento,usuario_documento,emitido_por,pedido_por,data_documento,trimestre_documento)
		VALUES('DECLARACAO SEM NOTAS', 2 , '".$userDoc."', '".$emitidoPor."', 0 ,'".date("Y-m-d")."','".$trimestre."')");
	}

	public function emitir_declaracao_com_notas($userDoc, $emitidoPor, $trimestre)
	{
		$query = $this->db->query("INSERT INTO documentos(nome_documento,tipo_documento,usuario_documento,emitido_por,pedido_por,data_documento, trimestre_documento)
		VALUES('DECLARACAO COM NOTAS', 3 , '".$userDoc."', '".$emitidoPor."', 0 , '".date("Y-m-d")."', '".$trimestre."')");
	}

	public function emitir_certificado($userDoc, $emitidoPor, $trimestre)
	{
		$query = $this->db->query("INSERT INTO documentos(nome_documento,tipo_documento,usuario_documento,emitido_por,pedido_por,data_documento, trimestre_documento)
		VALUES('CERTIFICADO', 4 , '".$userDoc."', '".$emitidoPor."', 0 , '".date("Y-m-d")."', '0')");
	}

	public function delete_documento($id)
	{
		$query = $this->db->query("UPDATE documentos SET documento_deleted = '1' WHERE id_documento = '$id'");
	}

	public function get_allBoletim($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM documentos LEFT JOIN pessoas ON usuario_documento = pessoa_id WHERE documento_deleted = '0' AND tipo_documento =  1 AND pedido_por = 0 ORDER BY id_documento DESC");
		return $query->result_array();
	}
	
	public function get_AllDecSemNotas($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM documentos LEFT JOIN pessoas ON usuario_documento = pessoa_id WHERE documento_deleted = '0' AND tipo_documento =  2 AND pedido_por = 0  AND pessoa_nome LIKE('%".$filtro."%') ORDER BY id_documento DESC");
		return $query->result_array();
	}
	
	public function get_AllDecComNotas($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM documentos LEFT JOIN pessoas ON usuario_documento = pessoa_id WHERE documento_deleted = '0' AND tipo_documento =  3 AND pedido_por = 0 ORDER BY id_documento DESC");
		return $query->result_array();
	}
	
	public function get_decComNotas($id)
	{
		$query = $this->db->query("SELECT * FROM documentos WHERE id_documento = '$id'");
		return $query->row_array();
	}
	
	public function get_AllCertificados($filtro = '')
	{
		$query = $this->db->query("SELECT * FROM documentos LEFT JOIN pessoas ON usuario_documento = pessoa_id WHERE documento_deleted = '0' AND tipo_documento =  4 AND pedido_por = 0 ORDER BY id_documento DESC");
		return $query->result_array();
	}

	function get_professorTurmas($idProfessor, $ano = '')
	{
		$wh = ($ano != '') ? "AND turma_ano ='$ano'" : "";
		$query = $this->db->query("SELECT * FROM turmapessoas
		LEFT JOIN turmas ON turma_id = turmapessoa_turma
		WHERE turmaPessoa_pessoa = '$idProfessor' AND turmaPessoa_deleted = 0 AND turma_deleted = 0 ".$wh);
		return $query->result_array();
	}

	function get_turmasPorAno($AnoActual)
	{
		$query = $this->db->query("SELECT * FROM turmas WHERE turma_ano = '$AnoActual' AND turma_deleted = 0 ");
		return $query->result_array();
	}

	public function update_avaliacao($idAvaliacao, $valorAvaliacao)
	{
		$this->db->query("UPDATE avaliacoes SET avaliacao_nota = '".$valorAvaliacao."' WHERE avaliacao_id = '".$idAvaliacao."'");
	}

	function del_avaliacao($trimestre, $disciplina, $turma, $estudanteId, $tipo)
	{
		$this->db->query("DELETE FROM avaliacoes 
		WHERE avaliacao_trimestre = '$trimestre' AND avaliacao_disciplina = '$disciplina' AND avaliacao_turma = '$turma' AND avaliacao_estudante = '$estudanteId' AND avaliacao_tipo = '$tipo'");
	}

	function del_boletim_de_notas($idUsuario)
	{
		$this->db->query("DELETE FROM avaliacoes 
		WHERE avaliacao_trimestre = '$trimestre' AND avaliacao_disciplina = '$disciplina' AND avaliacao_turma = '$turma' AND avaliacao_estudante = '$estudanteId' AND avaliacao_tipo = '$tipo'");
	}

	public function save_base($nomeEscola, $siglaEscola, $emailEscola, $emailPassword, $defaultPassword, $docHeader, $enderecoEscola, $telEscola, $dirAdmin, $dirPedag, $nProcInicio)
	{
		$strQuery = "INSERT INTO config(config_nomeEscola,config_emailEscola,config_emailPassword,config_defaultPassword,config_siglaEscola,config_docHeader,config_enderecoEscola,config_telEscola,config_diretorAdministrativo,config_diretorPedagogico,config_nProcessInicio)
		VALUES('".$nomeEscola."', '".$emailEscola."', '".$emailPassword."', '".$defaultPassword."', '".$siglaEscola."', '".$docHeader."', '".$enderecoEscola."', '".$telEscola."', '".$dirAdmin."', '".$dirPedag."', '".$nProcInicio."');";
		$this->db->query($strQuery);
	}

	public function save_avaliacao($trimestre, $disciplina, $turma, $tipo, $nota, $estudante, $professor)
	{
		$strQuery = "INSERT INTO avaliacoes(avaliacao_disciplina,avaliacao_tipo,avaliacao_turma,
		avaliacao_nota,avaliacao_estudante,avaliacao_professor,avaliacao_trimestre,avaliacao_dtAvaliacao)
		VALUES('".$disciplina."', '".$tipo."', '".$turma."', '".$nota."', '".$estudante."', '".$professor."', '".$trimestre."', '".date("Y-m-d H:i:s")."')";
		$this->db->query($strQuery);
	}
	
	public function limpar_pauta($turma, $disciplina, $trimestre)
	{
		$strQuery = "UPDATE avaliacoes SET avaliacao_nota = '0' WHERE avaliacao_trimestre = '$trimestre' 
		AND avaliacao_disciplina = '$disciplina' AND avaliacao_turma = '$turma' ";
		$this->db->query($strQuery);
	}
	
	public function documento_aceitar_pedido($id)
	{
		$this->db->query("UPDATE documentos SET estado_documento = '3' WHERE id_documento = '$id'");
	}

	public function documento_recusar_pedido($id)
	{
		$this->db->query("UPDATE documentos SET estado_documento = '4' WHERE id_documento = '$id'");
	}

	public function documento_apagar($id)
	{
		$this->db->query("UPDATE documentos SET documento_deleted = '1' WHERE id_documento = '$id'");
	}
}
