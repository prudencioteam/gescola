<?php

function verificar_sessao()
{
	$CI = &get_instance();
	$CI->session->set_flashdata('init_url', $CI->uri->uri_string);
	if(!isset($_SESSION))
	{
		session_start();
    }
    
	if(!isset($_SESSION["pessoa_id"]))
	{
		redirect(base_url()."auth/login","refresh");
	}
	else
	{
		if($_SESSION["pessoa_password"] == get_defaultPassword())
		{
			redirect(base_url()."Perfil/change_password_view/info","refresh");
		}
	}

	return $_SESSION;
}

function get_nomeInstituicao()
{
	$CI = &get_instance();
	return $CI->db->query("SELECT * FROM config ORDER BY config_id DESC")->row_array()["config_nomeEscola"];
}

function get_nomeDireitorGeral()
{
	$CI = &get_instance();
	return $CI->db->query("SELECT * FROM config ORDER BY config_id DESC")->row_array()["config_diretorAdministrativo"];
}

function get_nomeDireitorPedagogico()
{
	$CI = &get_instance();
	return $CI->db->query("SELECT * FROM config ORDER BY config_id DESC")->row_array()["config_diretorPedagogico"];
}

function get_imgLogoInstituicao($width = '75')
{
	$CI = &get_instance();
	$_imgLogo = APPPATH."data/img/system/logo.jpg";

	$retorno = '';
	if(file_exists($_imgLogo))
	{
		$retorno = '<img src="'.base_url().'Files/get_imgLogoInstituicao" width="'.$width.'px" />';
	}

	return $retorno;
}
	
function get_pathLogoInstituicao()
{
	return APPPATH."data/img/system/logo.jpg";
}

function get_formattedDate($dt)
{
	$aMeses = lista("meses");
	return date("d", strtotime($dt))." de ".($aMeses[(int)date("m", strtotime($dt))])." de ".date("Y", strtotime($dt));
}

function get_estudantesByProf($idProf = '', $output = 'array')
{
	$turmas = lista("turmas_pessoa", $idProf, date("Y"));
	
	$CI = &get_instance();
	$query = $CI->db->query("SELECT * FROM turmapessoas
	LEFT JOIN pessoas ON pessoa_id = turmaPessoa_pessoa
	WHERE turmaPessoa_turma IN('".implode("','", array_keys($turmas))."') AND turmaPessoa_deleted = 0 AND pessoa_deleted = 0 AND pessoa_usertipo = 4");
	
	if($output == "array")
	{
		$arr = $query->result_array();
		$r = array();

		foreach ($arr as $key => $value)
		{
			$r[$value["pessoa_id"]] = $value["pessoa_nome"];
		}
		return $r;
	}
	else
	{
		return $query->result_array();
	}
	
}

function get_encarregadosByProf($idProf = '', $output = 'array')
{
	$turmas = lista("turmas_pessoa", $idProf, date("Y"));
	
	$CI = &get_instance();
	$query = $CI->db->query("SELECT * FROM turmapessoas
	LEFT JOIN pessoas ON pessoa_id = turmaPessoa_pessoa
	WHERE turmaPessoa_turma IN('".implode("','", array_keys($turmas))."') AND turmaPessoa_deleted = 0 AND pessoa_deleted = 0 AND pessoa_usertipo = 4");
	
	$arr = $query->result_array();
	$r = array();

	if($output == "array")
	{
		foreach ($arr as $key => $value)
		{
			$r[$value["pessoa_pai"]] = (get_pessoaById($value["pessoa_pai"])["pessoa_nome"]??"");
			$r[$value["pessoa_mae"]] = (get_pessoaById($value["pessoa_mae"])["pessoa_nome"]??"");
		}
	}
	else
	{
		foreach ($arr as $key => $value)
		{
			$r[$value["pessoa_pai"]] = get_pessoaById($value["pessoa_pai"]);
			$r[$value["pessoa_mae"]] = get_pessoaById($value["pessoa_mae"]);
		}
	}

	return $r;
}

function get_estudantesByTurma($turma)
{	
	$CI = &get_instance();
	$query = $CI->db->query("SELECT * FROM turmapessoas
	LEFT JOIN pessoas ON pessoa_id = turmaPessoa_pessoa
	WHERE turmaPessoa_turma = '$turma' AND turmaPessoa_deleted = 0 AND pessoa_deleted = 0 AND pessoa_usertipo = 4");
	
	$arr = $query->result_array();
	$r = array();

	foreach ($arr as $key => $value)
	{
		$r[$value["pessoa_id"]] = $value["pessoa_nome"];
	}
	
	return $r;
}

function is_estudante($id = 'eu')
{
	$CI = &get_instance();
	$userTipo = 0;
	if($id == "eu")
	{
		$userTipo = $CI->session->userdata("pessoa_usertipo");
	}
	else
	{
    	$sQ = "SELECT * FROM pessoas WHERE pessoa_id = '$id'";
		$q = $CI->db->query($sQ);
		$userTipo = ($q->row_array()["pessoa_usertipo"]??"");
	}

	if($userTipo == 4) return TRUE;
	else return FALSE;
}

function get_notifyForMe()
{
	$CI = &get_instance();
	$userTipo = $CI->session->userdata("pessoa_usertipo");
	$meuid = $CI->session->userdata("pessoa_id");
	$mygroup = "all";

	switch ($userTipo)
	{
		case 3:
			$mygroup = "teachers";
			break;

		case 4:
			$mygroup = "students";
			break;
		
		default:
			$mygroup = "all";
			break;
	}

    $sQ = "SELECT * FROM notificacoes 
	LEFT JOIN notificacoesvistas ON ((nvista_notif = notif_id) AND (nvista_pessoa = '".$meuid."'))
	WHERE notif_deleted = '0' AND notif_criador != '".$meuid."' AND ((notif_for = 'all') OR (notif_forEspecific LIKE('%".$meuid."%')) OR notif_for = '".$mygroup."') 
	AND (nvista_deleted = 0 OR nvista_deleted IS NULL)";
    $q = $CI->db->query($sQ);
    return $q->result_array();
}


function count_unviewedNotifyForMe()
{
	$CI = &get_instance();
	$userTipo = $CI->session->userdata("pessoa_usertipo");
	$meuid = $CI->session->userdata("pessoa_id");
	$mygroup = "all";

	switch ($userTipo)
	{
		case 3:
			$mygroup = "teachers";
			break;

		case 4:
			$mygroup = "students";
			break;
		
		default:
			$mygroup = "all";
			break;
	}

    $sQ = "SELECT COUNT(*) AS total FROM notificacoes 
	LEFT JOIN notificacoesvistas ON ((nvista_notif = notif_id) AND (nvista_pessoa = '".$meuid."'))
	WHERE notif_deleted = '0' AND notif_criador != '".$meuid."' AND ((notif_for = 'all') OR (notif_forEspecific LIKE('%".$meuid."%')) OR notif_for = '".$mygroup."') 
	AND (nvista_deleted IS NULL)";
	$q = $CI->db->query($sQ);
    return $q->row_array()["total"];
}

function get_pessoaById($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM pessoas WHERE pessoa_id = '$id'";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_newNProc()
{
    $CI = &get_instance();
	$sQ = "SELECT MAX(pessoa_nproc) as last_proc FROM pessoas";
	$q = $CI->db->query($sQ);
	$lastNProc = (int)$q->row_array()["last_proc"];

	if($lastNProc == 0)
	{
		$lastNProc = get_defaultNProc();
	}

	return ($lastNProc + 1);
}

function get_baseConfig()
{
	$CI = &get_instance();
	return $CI->db->query("SELECT * FROM config ORDER BY config_id DESC")->row_array();
}

function get_defaultNProc()
{
	$CI = &get_instance();
	return $CI->db->query("SELECT * FROM config ORDER BY config_id DESC")->row_array()["config_nProcessInicio"];
}

function get_defaultPassword()
{
	$CI = &get_instance();
	return $CI->db->query("SELECT * FROM config ORDER BY config_id DESC")->row_array()["config_defaultPassword"];
}

function get_alunosTurma($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM turmapessoas
	LEFT JOIN turmas ON turmaPessoa_turma = turma_id
	LEFT JOIN pessoas ON turmaPessoa_pessoa = pessoa_id
	WHERE turmaPessoa_turma = '$id' AND pessoa_usertipo = 4 AND turmaPessoa_deleted = 0 ORDER BY pessoa_nome ASC";
    $q = $CI->db->query($sQ);
    return $q->result_array();
}

function get_turmaAnoExtremos()
{
	$CI = &get_instance();
    $sQ = "SELECT MIN(turma_ano) AS inicio, MAX(turma_ano) AS fim FROM turmas
	WHERE turma_deleted = 0";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_professoresTurma($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM turmapessoas
	LEFT JOIN turmas ON turmaPessoa_turma = turma_id
	LEFT JOIN pessoas ON turmaPessoa_pessoa = pessoa_id
	WHERE turmaPessoa_turma = '$id' AND pessoa_usertipo = 3 AND turmaPessoa_deleted = 0";
    $q = $CI->db->query($sQ);
    return $q->result_array();
}

function dados_academicos($id)
{
    $CI = &get_instance();
	$sQ = "SELECT * FROM turmapessoas
	LEFT JOIN pessoas ON turmaPessoa_pessoa = pessoa_id
	LEFT JOIN turmas ON turmaPessoa_turma = turma_id
	LEFT JOIN cursos ON turma_curso = curso_id
	LEFT JOIN classes ON turma_classeId = classe_id
	LEFT JOIN turnos ON turma_turno = turno_id
	WHERE turmaPessoa_pessoa = '".$id."' AND turmaPessoa_deleted = 0";
	$q = $CI->db->query($sQ);
	return $q->result_array();
}

function get_alunosTurmaPorPagina($id, $limitStart, $limitCount)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM turmapessoas
	LEFT JOIN turmas ON turmaPessoa_turma = turma_id
	LEFT JOIN pessoas ON turmaPessoa_pessoa = pessoa_id
	WHERE turmaPessoa_turma = '$id' AND pessoa_usertipo = '4' 
	LIMIT ".($limitStart*$limitCount).", $limitCount";
	
	$q = $CI->db->query($sQ);
    return $q->result_array();
}

function get_alunosTurmaPorPagina_total($id)
{
    $CI = &get_instance();
    $sQ = "SELECT count(*) as total FROM turmapessoas
	LEFT JOIN turmas ON turmaPessoa_turma = turma_id
	LEFT JOIN pessoas ON turmaPessoa_pessoa = pessoa_id
	WHERE turmaPessoa_turma = '$id' AND pessoa_usertipo = '4'";
	
	$q = $CI->db->query($sQ);
    return $q->row_array()["total"];
}

function get_pessoaByNproc($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM pessoas WHERE pessoa_nProc = '$id'";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_turmaById($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM turmas WHERE turma_id = '$id'";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_horarioByTurma($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM horarios WHERE hr_turma = '$id' AND hr_deleted = 0";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_horarioModeloById($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM horario_modelos WHERE hmod_id = '$id'";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_profTurmaDisciplina($turma, $disciplina)
{
    $CI = &get_instance();
	$sQ = "SELECT * FROM turmapessoas
	INNER JOIN pessoas ON turmaPessoa_pessoa = pessoa_id
	WHERE turmaPessoa_turma = '$turma' AND pessoa_usertipo = '3'";
    $q = $CI->db->query($sQ);
	$r = $q->result_array();

	$retorno = array();

	foreach ($r as $key => $value)
	{
		$arr = json_decode($value["turmaPessoa_disciplinas"], TRUE);
		$arr = is_array($arr) ? $arr : array();

		if(in_array($disciplina, $arr) || in_array("all", $arr))
		{
			array_push($retorno, $value);
		}
	}
	
	return $retorno;
}

function get_classeById($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM classes WHERE classe_id = '$id'";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_cursoById($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM cursos WHERE curso_id = '$id'";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_disciplinaById($id)
{
    $CI = &get_instance();
    $sQ = "SELECT * FROM disciplinas WHERE disciplina_id = '$id'";
    $q = $CI->db->query($sQ);
    return $q->row_array();
}

function get_alunoAvaliacoes($turma, $pessoa, $disciplina, $trimestre = "1")
{
	$CI = &get_instance();


	$and = "";

	if($trimestre != "")
	{
		$and = "AND avaliacao_trimestre = '".$trimestre."'";
	}

    $sQ = "SELECT * FROM avaliacoes 
	WHERE avaliacao_turma = '$turma' AND avaliacao_estudante = '$pessoa' AND avaliacao_disciplina = '$disciplina'
	AND avaliacao_deleted = '0' ".$and;
    $q = $CI->db->query($sQ);
    return $q->result_array();
}

function get_alunoAvaliacoesBoletim($turma, $pessoa, $trimestre = "1")
{
	$CI = &get_instance();
    $sQ = "SELECT * FROM avaliacoes 
	WHERE avaliacao_turma = '$turma' AND avaliacao_estudante = '$pessoa'
	AND avaliacao_deleted = '0' AND avaliacao_trimestre = '".$trimestre."'";
	$q = $CI->db->query($sQ);
    return $q->result_array();
}

function podeEmitirCertificado($id)
{
	$_a = get_alunoCertificado($id);

	$arr_notasFinais = array();

	foreach ($_a as $key => $value)
    {
        if(!isset($arr_notasFinais[$value["avaliacao_disciplina"]]))
        {
            $arr_notasFinais[$value["avaliacao_disciplina"]] = array();
        }

        if(!isset($arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]]))
        {
            $arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]] = array();
        }

        array_push($arr_notasFinais[$value["avaliacao_disciplina"]][$value["turma_id"]], $value["avaliacao_nota"]);
    }

    $arrNotas = array();

    foreach ($arr_notasFinais as $rkey => $rvalue)
    {
        $__disciplina = $rkey;
        $___arrMedia = array();
        foreach ($rvalue as $rk => $rv)
        {
            $__turma = $rk;
            $___soma = array_sum($rv);
            $___quantidade = sizeof($rv);
            $___media = $___soma/$___quantidade;
            array_push($___arrMedia, $___media);
        }
        
        $___somaT = array_sum($___arrMedia);
        $___quantidadeT = sizeof($___arrMedia);
        $___mediaD = $___somaT/$___quantidadeT;
        $arrNotas[$__disciplina] = $___mediaD;

	}

	$result = TRUE;
	
	foreach ($arrNotas as $key => $value)
	{
		if($value < 10)
		{
			$result = FALSE;
		}
	}

	return $result;
}

function temTurma($id)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT * FROM turmapessoas WHERE turmaPessoa_pessoa = '$id' AND turmaPessoa_deleted = 0");
	if(empty($query->result_array())) return FALSE;

	return TRUE;;
}

function get_alunoTurmas($pessoa)
{
	$CI = &get_instance();
    $sQ = "SELECT * FROM turmapessoas 
	WHERE turmaPessoa_pessoa = '$pessoa' AND turmaPessoa_deleted = '0'";
	$q = $CI->db->query($sQ);
	$result = array();

	foreach ($q->result_array() as $key => $value)
	{
		array_push($result, $value["turmaPessoa_turma"]);
	}
    return $result;
}

function get_pessoasTurma($turma,$tipo = '9999')
{
	$CI = &get_instance();
    $sQ = "SELECT * FROM turmapessoas 
	LEFT JOIN pessoas ON pessoa_id = turmaPessoa_pessoa 
	WHERE turmaPessoa_turma = '$turma' AND pessoa_usertipo = '$tipo'";
	$q = $CI->db->query($sQ);

	$result = array();

	foreach ($q->result_array() as $key => $value)
	{
		array_push($result, $value["turmaPessoa_id"]);
	}
	return $result;
}

function get_alunoDeclaracaoComNotas($pessoa, $ano = '')
{
	$ano = ($ano == '') ? date("Y") : $ano;
	$CI = &get_instance();
    $sQ = "SELECT * FROM avaliacoes 
	LEFT JOIN turmas ON turma_id = avaliacao_turma 
	WHERE avaliacao_estudante = '$pessoa'
	AND avaliacao_tipo = '4'
	AND avaliacao_deleted = '0'";
	$q = $CI->db->query($sQ);

	$result = array();

	foreach ($q->result_array() as $key => $value)
	{
		if($value["turma_ano"] < $ano)
		{
			$result[$key] = $value;
		}
	}
	
    return $result;
}

function get_alunoCertificado($pessoa)
{
	$CI = &get_instance();
    $sQ = "SELECT * FROM avaliacoes 
	LEFT JOIN turmas ON turma_id = avaliacao_turma 
	WHERE avaliacao_estudante = '$pessoa'
	AND avaliacao_tipo = '4'
	AND avaliacao_deleted = '0'";
	$q = $CI->db->query($sQ);
    return $q->result_array();
}

function get_alunoAllAvaliacoes($pessoa)
{
	$CI = &get_instance();

    $sQ = "SELECT * FROM avaliacoes 
	LEFT JOIN turmas ON turma_id = avaliacao_turma
	WHERE avaliacao_estudante = '$pessoa'
	AND turma_deleted = '0' AND avaliacao_deleted = '0'
	AND avaliacao_deleted = '0' ORDER BY turma_ano DESC, avaliacao_trimestre DESC";
    $q = $CI->db->query($sQ);
    return $q->result_array();
}

function get_alunoAllAvaliacoes_boletim($pessoa)
{
	$CI = &get_instance();

    $sQ = "SELECT * FROM avaliacoes 
	LEFT JOIN turmas ON turma_id = avaliacao_turma
	WHERE avaliacao_estudante = '$pessoa'
	AND avaliacao_deleted = '0' ORDER BY turma_ano,avaliacao_trimestre,avaliacao_id DESC";
    $q = $CI->db->query($sQ);
    return $q->result_array();
}

function get_alunoAvaliacoes_all($turma, $pessoa, $disciplina)
{
	$CI = &get_instance();

    $sQ = "SELECT * FROM avaliacoes 
	WHERE avaliacao_turma = '$turma' AND avaliacao_estudante = '$pessoa' AND avaliacao_disciplina = '$disciplina'
	AND avaliacao_deleted = '0' ";
    $q = $CI->db->query($sQ);
    return $q->result_array();
}

function getFormattedName($name)
{
	$arr = explode(" ", trim($name));
	$result = $arr[0]." ".$arr[sizeof($arr) -1];
	return $result;
}

function get_quantidade($op, $param = '')
{
    $CI = &get_instance();
	$result = -1;
	switch ($op)
	{
		case 'cursos':
			$result = $CI->db->query("SELECT count(*) as valor FROM cursos WHERE curso_deleted = '0'")->row_array()["valor"];
			break;
			
		case 'professores':
			$result = $CI->db->query("SELECT count(*) as valor FROM pessoas WHERE pessoa_usertipo = '3' AND pessoa_deleted = '0'")->row_array()["valor"];
			break;
			
		case 'estudantes':
			$result = $CI->db->query("SELECT count(*) as valor FROM turmapessoas 
			LEFT JOIN pessoas ON pessoa_id = turmaPessoa_pessoa
			LEFT JOIN turmas ON turma_id = turmaPessoa_turma
			WHERE pessoa_usertipo = '4' AND pessoa_deleted = '0' AND turmaPessoa_deleted = '0' AND turma_deleted = '0' AND turma_ano = '".date("Y")."'")->row_array()["valor"];
			break;
			
		case 'turmas':
			$result = $CI->db->query("SELECT count(*) as valor FROM turmas WHERE turma_ano = '".date("Y")."' AND turma_deleted = '0'")->row_array()["valor"];
			break;
		
		default:
			$result = -100;
			break;
	}
	
	return $result;
}

function lista($op, $param = '', $param2 = '')
{
    $CI = &get_instance();
	$result = array();
	switch ($op)
	{
		case 'meses':
			$result =array(1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
			break;
		
		case "notify_groups":
			$result = array("all" => "Todos os Usuários", "teachers" => "Todos os Professores", "students" => "Todos os Estudantes", "admins" => "Pessoal Administrativo", "group" => "Um Grupo Específico", "person" => "Uma pessoa específica");
			break;
		
		case "notify_groups_prof":
			$result = array("" => "Selecione uma opção", "group" => "Um Grupo Específico", "person" => "Uma pessoa específica");
			break;

		case 'genero':
			$result = array("M" => "Masculino", "F" => "Femenino");
			break;

		case 'tipoDoc':
			$result = array("bi" => "Bilhete de Identidade", "pp" => "Passaporte", "cr" => "Cartão de Residente");
			break;

		case 'estado_civil':
			$result =array("1" => "Solteiro(a)", "2" => "Casado(a)", "3" => "Divorciado(a)", "4" => "Viúvo(a)");
			break;

		case 'tipo_usuario':
			$result =array("1" => "Diretor", "2" => "Secretário", "3" => "Professor", "4" => "Aluno");
			break;

		case 'turnos':
			$result =array("1" => "Diurno", "2" => "Noturno", "3" => "Manhã", "4" => "Tarde", "5" => "Noite");
			break;

		case 'trimestres':
			$result =array("1" => "1º Trimestre", "2" => "2º Trimestre", "3" => "3º Trimestre");
			break;
			
		case 'allAlunos':
			$arr = $CI->db->query("SELECT * FROM pessoas 
			WHERE pessoa_usertipo = 4 AND pessoa_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_id"]] = $value["pessoa_nome"];
			}
			break;
			
		case 'allProfessores':
			$arr = $CI->db->query("SELECT * FROM pessoas 
			WHERE pessoa_usertipo = 3 AND pessoa_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_id"]] = $value["pessoa_nome"];
			}
			break;
			
		case 'allEncarregados':
			$arr = $CI->db->query("SELECT * FROM pessoas 
			WHERE pessoa_usertipo = 5 AND pessoa_deleted = 0")->result_array();
			$result = array();
			if($param == "form") $result[""] = "Selecione uma opção";
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_id"]] = $value["pessoa_nome"];
			}
			break;
			
		case 'h_encarregados':
			$arr = $CI->db->query("SELECT * FROM pessoas 
			WHERE pessoa_usertipo = 5 AND pessoa_genero = 'M' AND pessoa_deleted = 0")->result_array();
			$result = array();
			if($param == "form") $result[""] = "Selecione uma opção";
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_id"]] = $value["pessoa_nome"];
			}
			break;
			
		case 'm_encarregados':
			$arr = $CI->db->query("SELECT * FROM pessoas 
			WHERE pessoa_usertipo = 5 AND pessoa_genero = 'F' AND pessoa_deleted = 0")->result_array();
			$result = array();
			if($param == "form") $result[""] = "Selecione uma opção";
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_id"]] = $value["pessoa_nome"];
			}
			break;
			
		case 'pessoas':
			$arr = $CI->db->query("SELECT * FROM pessoas 
			WHERE pessoa_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_id"]] = $value["pessoa_nome"];
			}
			break;

		case 'estado':
				$arr = $CI->db->query("SELECT * FROM estado 
				WHERE estado_deleted = 0")->result_array();
				$result = array();
				foreach ($arr as $key => $value)
				{
					$result[$value["estado_id"]] = $value["estado_nome"];
				}
				break;
			
		case 'dados_academicos':
			$arr = $CI->db->query("SELECT * FROM turmapessoas
			LEFT JOIN pessoas ON turmaPessoa_pessoa = pessoa_id
			LEFT JOIN turmas ON turmaPessoa_turma = turma_id
            LEFT JOIN cursos ON turma_curso = curso_id
            LEFT JOIN classes ON turma_classeId = classe_id
            LEFT JOIN turnos ON turma_turno = turno_id
			WHERE turmaPessoa_pessoa = '".$param."'")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_id"]] = $value["pessoa_nome"];
			}
			break;
			
		case 'filtered_estudantes':
			$arr = $CI->db->query("SELECT * FROM pessoas 
			LEFT JOIN turmapessoas ON turmaPessoa_pessoa = pessoa_id
			WHERE (pessoa_nome LIKE('%".$param."%') OR pessoa_nproc = '".$param."')
			AND pessoa_usertipo = '4' AND pessoa_deleted = '0' AND turmaPessoa_deleted = '0'")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_id"]] = $value["pessoa_nome"];
			}
			break;
			
		case 'turma_estudante':
			$arr = $CI->db->query("SELECT * FROM turmapessoas
			LEFT JOIN turmas ON turmaPessoa_turma = turma_id
			WHERE turmaPessoa_pessoa = '".$param."' AND turmaPessoa_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["turma_id"]] = $value["turma_nome"];
			}
			break;
	
		case 'ano_lectivo':
			$arr = $CI->db->query("SELECT * FROM ano_lectivo 
			WHERE ano_lectivo_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["ano_lectivo_id"]] = $value["ano_lectivo_nome"];
			}
			break;
			
		case 'turmas_curso':
			$arr = $CI->db->query("SELECT * FROM turmas 
			LEFT JOIN cursos ON turma_curso = curso_id
			WHERE turma_deleted = '0' AND curso_deleted = '0'")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["turma_id"]] = $value["turma_nome"]." - ".$value["turma_ano"]." | ".$value["curso_nome"];
			}
			break;
			
		case 'turmas_pessoa':
			$arr = $CI->db->query("SELECT * FROM turmapessoas
			LEFT JOIN turmas ON turma_id = turmapessoa_turma
			WHERE turmapessoa_deleted = '0' AND turma_deleted = 0 AND turmapessoa_pessoa = '$param'")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				if(($param2 != "all") && ($param2 != ""))
				{
					if($value["turma_ano"] != $param2) continue;
				}
				$result[$value["turma_id"]] = $value["turma_nome"];
			}
			break;
			
		case 'classes':
			$arr = $CI->db->query("SELECT * FROM classes 
			WHERE classe_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["classe_id"]] = $value["classe_nome"];
			}
			break;
			
		case 'turmas':
			$arr = $CI->db->query("SELECT * FROM turmas 
			WHERE turma_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["turma_id"]] = $value["turma_nome"];
			}
			break;
			
		case 'turmas_sem_horario':
			$arr = $CI->db->query("SELECT * FROM turmas 
			WHERE turma_deleted = 0 AND turma_id NOT IN(SELECT hr_turma FROM horarios WHERE hr_deleted = 0)")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["turma_id"]] = $value["turma_nome"]." - ".$value["turma_ano"];
			}
			break;
			
		case 'cursos':
			$arr = $CI->db->query("SELECT * FROM cursos 
			WHERE curso_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["curso_id"]] = $value["curso_nome"];
			}
			break;

		case 'turnos':
			$arr = $CI->db->query("SELECT * FROM turnos 
			WHERE turno_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["turno_id"]] = $value["turno_nome"];
			}
			break;
		
		case 'pessoas_nprocId':
			$arr = $CI->db->query("SELECT * FROM pessoas 
			WHERE pessoa_deleted = 0 AND pessoa_usertipo = '4'")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["pessoa_nProc"]] = $value["pessoa_id"];
			}
			break;
		
		case 'disciplinas':
			$arr = $CI->db->query("SELECT * FROM disciplinas 
			WHERE disciplina_deleted = 0")->result_array();
			$result = array();
			foreach ($arr as $key => $value)
			{
				$result[$value["disciplina_id"]] = $value["disciplina_nome"];
			}
			break;

			case 'documentos':
				$arr = $CI->db->query("SELECT * FROM tipo_documento 
				WHERE documento_deleted = 0")->result_array();
				$result = array();
				foreach ($arr as $key => $value)
				{
					$result[$value["idtipo_documento"]] = $value["nome_tipodocumento"];
				}
				break;
				
			case 'docs_pedido':
				$arr = $CI->db->query("SELECT * FROM tipo_documento 
				WHERE documento_deleted = 0 AND idtipo_documento <> 1")->result_array();
				$result = array();
				foreach ($arr as $key => $value)
				{
					$result[$value["idtipo_documento"]] = $value["nome_tipodocumento"];
				}
				break;
		
		default:
			$result = array();
			break;
	}
	
	return $result;
}

function temAcesso($modulo, $permissao = '')
{
	$CI = &get_instance();
	$arrPerms = array("", "R", "W", "X");
	$result = FALSE;

	//$pPos = array_search(, $arrPerms);

	switch (strtolower($modulo))
	{
		case ('notificacao' || 'notificacoes'):
			
			
			$result = TRUE;

			break;
		
		default:
			$result = TRUE;
			break;
	}

	return $result;
}

?>