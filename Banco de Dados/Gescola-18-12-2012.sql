-- --------------------------------------------------------
-- Anfitrião:                    127.0.0.1
-- Versão do servidor:           5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for gestor_escolar
CREATE DATABASE IF NOT EXISTS `gestor_escolar` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gestor_escolar`;

-- Dumping structure for table gestor_escolar.ano_lectivo
CREATE TABLE IF NOT EXISTS `ano_lectivo` (
  `ano_lectivo_id` int(11) NOT NULL AUTO_INCREMENT,
  `ano_lectivo_nome` int(11) DEFAULT NULL,
  `ano_lectivo_userCriador` int(11) DEFAULT '0',
  `ano_lectivo_dataCriacao` varchar(50) DEFAULT '0',
  `ano_lectivo_descricao` text,
  `ano_lectivo_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`ano_lectivo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table gestor_escolar.ano_lectivo: ~3 rows (approximately)
/*!40000 ALTER TABLE `ano_lectivo` DISABLE KEYS */;
INSERT INTO `ano_lectivo` (`ano_lectivo_id`, `ano_lectivo_nome`, `ano_lectivo_userCriador`, `ano_lectivo_dataCriacao`, `ano_lectivo_descricao`, `ano_lectivo_deleted`) VALUES
	(20, 2019, 1, '2019-12-03 11:36:26', 'Ano Lectivo de 2019', 0),
	(21, 2020, 1, '2019-12-11 15:22:06', 'adfasd', 0);
/*!40000 ALTER TABLE `ano_lectivo` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.avaliacoes
CREATE TABLE IF NOT EXISTS `avaliacoes` (
  `avaliacao_id` int(11) NOT NULL AUTO_INCREMENT,
  `avaliacao_disciplina` int(11) DEFAULT NULL,
  `avaliacao_tipo` int(11) DEFAULT NULL,
  `avaliacao_turma` int(11) DEFAULT NULL,
  `avaliacao_nota` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `avaliacao_trimestre` int(11) DEFAULT NULL,
  `avaliacao_estudante` int(11) DEFAULT NULL,
  `avaliacao_professor` int(11) DEFAULT NULL,
  `avaliacao_dtAvaliacao` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `avaliacao_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`avaliacao_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.avaliacoes: ~80 rows (approximately)
/*!40000 ALTER TABLE `avaliacoes` DISABLE KEYS */;
INSERT INTO `avaliacoes` (`avaliacao_id`, `avaliacao_disciplina`, `avaliacao_tipo`, `avaliacao_turma`, `avaliacao_nota`, `avaliacao_trimestre`, `avaliacao_estudante`, `avaliacao_professor`, `avaliacao_dtAvaliacao`, `avaliacao_deleted`) VALUES
	(1, 2, 1, 8, '20', 1, 43, 3, '2019-12-16 12:47:35', 0),
	(2, 2, 2, 8, '20', 1, 43, 3, '2019-12-16 12:47:35', 0),
	(3, 2, 3, 8, '2', 1, 43, 3, '2019-12-16 12:47:35', 0),
	(4, 2, 4, 8, '14', 1, 43, 3, '2019-12-16 12:47:35', 0),
	(5, 1, 1, 12, '14', 1, 37, 3, '2019-12-18 10:53:29', 0),
	(6, 1, 2, 12, '20', 1, 37, 3, '2019-12-18 10:53:29', 0),
	(7, 1, 3, 12, '20', 1, 37, 3, '2019-12-18 10:53:29', 0),
	(8, 1, 4, 12, '18', 1, 37, 3, '2019-12-18 10:53:29', 0),
	(9, 1, 1, 12, '6', 1, 5, 3, '2019-12-18 10:53:30', 0),
	(10, 1, 2, 12, '7', 1, 5, 3, '2019-12-18 10:53:30', 0),
	(11, 1, 3, 12, '20', 1, 5, 3, '2019-12-18 10:53:30', 0),
	(12, 1, 4, 12, '11', 1, 5, 3, '2019-12-18 10:53:30', 0),
	(13, 1, 1, 12, '10', 1, 6, 3, '2019-12-18 10:53:30', 0),
	(14, 1, 2, 12, '10', 1, 6, 3, '2019-12-18 10:53:30', 0),
	(15, 1, 3, 12, '2', 1, 6, 3, '2019-12-18 10:53:30', 0),
	(16, 1, 4, 12, '7.33', 1, 6, 3, '2019-12-18 10:53:30', 0),
	(17, 1, 1, 12, '6', 1, 7, 3, '2019-12-18 10:53:30', 0),
	(18, 1, 2, 12, '18', 1, 7, 3, '2019-12-18 10:53:30', 0),
	(19, 1, 3, 12, '19', 1, 7, 3, '2019-12-18 10:53:30', 0),
	(20, 1, 4, 12, '14.33', 1, 7, 3, '2019-12-18 10:53:30', 0),
	(21, 1, 1, 12, '19', 1, 8, 3, '2019-12-18 10:53:30', 0),
	(22, 1, 2, 12, '5', 1, 8, 3, '2019-12-18 10:53:30', 0),
	(23, 1, 3, 12, '5', 1, 8, 3, '2019-12-18 10:53:30', 0),
	(24, 1, 4, 12, '9.67', 1, 8, 3, '2019-12-18 10:53:30', 0),
	(25, 1, 1, 12, '20', 1, 9, 3, '2019-12-18 10:53:30', 0),
	(26, 1, 2, 12, '20', 1, 9, 3, '2019-12-18 10:53:30', 0),
	(27, 1, 3, 12, '20', 1, 9, 3, '2019-12-18 10:53:30', 0),
	(28, 1, 4, 12, '20', 1, 9, 3, '2019-12-18 10:53:31', 0),
	(29, 1, 1, 12, '2', 1, 10, 3, '2019-12-18 10:53:31', 0),
	(30, 1, 2, 12, '15', 1, 10, 3, '2019-12-18 10:53:31', 0),
	(31, 1, 3, 12, '19', 1, 10, 3, '2019-12-18 10:53:31', 0),
	(32, 1, 4, 12, '12', 1, 10, 3, '2019-12-18 10:53:31', 0),
	(33, 1, 1, 12, '7', 1, 11, 3, '2019-12-18 10:53:31', 0),
	(34, 1, 2, 12, '18', 1, 11, 3, '2019-12-18 10:53:31', 0),
	(35, 1, 3, 12, '1', 1, 11, 3, '2019-12-18 10:53:31', 0),
	(36, 1, 4, 12, '8.67', 1, 11, 3, '2019-12-18 10:53:31', 0),
	(37, 1, 1, 12, '10', 1, 12, 3, '2019-12-18 10:53:31', 0),
	(38, 1, 2, 12, '16', 1, 12, 3, '2019-12-18 10:53:31', 0),
	(39, 1, 3, 12, '17', 1, 12, 3, '2019-12-18 10:53:31', 0),
	(40, 1, 4, 12, '14.33', 1, 12, 3, '2019-12-18 10:53:31', 0),
	(41, 1, 1, 12, '6', 1, 13, 3, '2019-12-18 10:53:31', 0),
	(42, 1, 2, 12, '17', 1, 13, 3, '2019-12-18 10:53:31', 0),
	(43, 1, 3, 12, '19', 1, 13, 3, '2019-12-18 10:53:31', 0),
	(44, 1, 4, 12, '14', 1, 13, 3, '2019-12-18 10:53:31', 0),
	(45, 1, 1, 12, '3', 1, 14, 3, '2019-12-18 10:53:31', 0),
	(46, 1, 2, 12, '19', 1, 14, 3, '2019-12-18 10:53:32', 0),
	(47, 1, 3, 12, '20', 1, 14, 3, '2019-12-18 10:53:32', 0),
	(48, 1, 4, 12, '14', 1, 14, 3, '2019-12-18 10:53:32', 0),
	(49, 1, 1, 12, '10', 1, 15, 3, '2019-12-18 10:53:32', 0),
	(50, 1, 2, 12, '10', 1, 15, 3, '2019-12-18 10:53:32', 0),
	(51, 1, 3, 12, '2', 1, 15, 3, '2019-12-18 10:53:32', 0),
	(52, 1, 4, 12, '7.33', 1, 15, 3, '2019-12-18 10:53:32', 0),
	(53, 1, 1, 12, '16', 1, 16, 3, '2019-12-18 10:53:32', 0),
	(54, 1, 2, 12, '20', 1, 16, 3, '2019-12-18 10:53:32', 0),
	(55, 1, 3, 12, '1', 1, 16, 3, '2019-12-18 10:53:32', 0),
	(56, 1, 4, 12, '12.33', 1, 16, 3, '2019-12-18 10:53:32', 0),
	(57, 1, 1, 12, '1', 1, 17, 3, '2019-12-18 10:53:32', 0),
	(58, 1, 2, 12, '19', 1, 17, 3, '2019-12-18 10:53:32', 0),
	(59, 1, 3, 12, '1', 1, 17, 3, '2019-12-18 10:53:32', 0),
	(60, 1, 4, 12, '7', 1, 17, 3, '2019-12-18 10:53:32', 0),
	(61, 1, 1, 12, '16', 1, 18, 3, '2019-12-18 10:53:33', 0),
	(62, 1, 2, 12, '19', 1, 18, 3, '2019-12-18 10:53:33', 0),
	(63, 1, 3, 12, '20', 1, 18, 3, '2019-12-18 10:53:33', 0),
	(64, 1, 4, 12, '18.33', 1, 18, 3, '2019-12-18 10:53:33', 0),
	(65, 1, 1, 12, '14', 1, 19, 3, '2019-12-18 10:53:33', 0),
	(66, 1, 2, 12, '4', 1, 19, 3, '2019-12-18 10:53:33', 0),
	(67, 1, 3, 12, '20', 1, 19, 3, '2019-12-18 10:53:33', 0),
	(68, 1, 4, 12, '12.67', 1, 19, 3, '2019-12-18 10:53:33', 0),
	(69, 1, 1, 12, '15', 1, 20, 3, '2019-12-18 10:53:33', 0),
	(70, 1, 2, 12, '10', 1, 20, 3, '2019-12-18 10:53:33', 0),
	(71, 1, 3, 12, '20', 1, 20, 3, '2019-12-18 10:53:33', 0),
	(72, 1, 4, 12, '15', 1, 20, 3, '2019-12-18 10:53:33', 0),
	(73, 1, 1, 12, '18', 1, 21, 3, '2019-12-18 10:53:33', 0),
	(74, 1, 2, 12, '10', 1, 21, 3, '2019-12-18 10:53:33', 0),
	(75, 1, 3, 12, '20', 1, 21, 3, '2019-12-18 10:53:33', 0),
	(76, 1, 4, 12, '16', 1, 21, 3, '2019-12-18 10:53:33', 0),
	(77, 1, 1, 12, '10', 1, 22, 3, '2019-12-18 10:53:33', 0),
	(78, 1, 2, 12, '16', 1, 22, 3, '2019-12-18 10:53:33', 0),
	(79, 1, 3, 12, '16', 1, 22, 3, '2019-12-18 10:53:33', 0),
	(80, 1, 4, 12, '14', 1, 22, 3, '2019-12-18 10:53:33', 0);
/*!40000 ALTER TABLE `avaliacoes` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.classes
CREATE TABLE IF NOT EXISTS `classes` (
  `classe_id` int(11) NOT NULL AUTO_INCREMENT,
  `classe_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `classe_descricao` text COLLATE utf8_bin,
  `classe_dataCriacao` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `classe_userCriador` int(11) DEFAULT NULL,
  `classe_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`classe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.classes: ~3 rows (approximately)
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` (`classe_id`, `classe_nome`, `classe_descricao`, `classe_dataCriacao`, `classe_userCriador`, `classe_deleted`) VALUES
	(1, '10ª Classe', NULL, NULL, NULL, 0),
	(2, '11ª Classe', NULL, NULL, NULL, 0),
	(3, '12ª Classe', NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.config
CREATE TABLE IF NOT EXISTS `config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_nomeEscola` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `config_emailEscola` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `config_emailPassword` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `config_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.config: ~0 rows (approximately)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.cursos
CREATE TABLE IF NOT EXISTS `cursos` (
  `curso_id` int(11) NOT NULL AUTO_INCREMENT,
  `curso_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `curso_descricao` text COLLATE utf8_bin,
  `curso_dataCriacao` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `curso_userCriador` int(11) DEFAULT NULL,
  `curso_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`curso_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.cursos: ~2 rows (approximately)
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` (`curso_id`, `curso_nome`, `curso_descricao`, `curso_dataCriacao`, `curso_userCriador`, `curso_deleted`) VALUES
	(1, 'Engenharia Informática', NULL, NULL, NULL, 0),
	(2, 'Engenharia de Telecomunicações', '', NULL, NULL, 0);
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.disciplinas
CREATE TABLE IF NOT EXISTS `disciplinas` (
  `disciplina_id` int(11) NOT NULL AUTO_INCREMENT,
  `disciplina_nome` varchar(45) DEFAULT NULL,
  `disciplina_descricao` varchar(45) DEFAULT NULL,
  `disciplina_dataCriacao` varchar(45) DEFAULT NULL,
  `disciplina_userCriador` int(11) DEFAULT NULL,
  `disciplina_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`disciplina_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table gestor_escolar.disciplinas: ~4 rows (approximately)
/*!40000 ALTER TABLE `disciplinas` DISABLE KEYS */;
INSERT INTO `disciplinas` (`disciplina_id`, `disciplina_nome`, `disciplina_descricao`, `disciplina_dataCriacao`, `disciplina_userCriador`, `disciplina_deleted`) VALUES
	(1, 'Cálculo 1', NULL, NULL, NULL, 0),
	(2, 'Fisica 1', NULL, NULL, NULL, 0),
	(3, 'TLP', NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `disciplinas` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.documentos
CREATE TABLE IF NOT EXISTS `documentos` (
  `id_documento` int(11) NOT NULL AUTO_INCREMENT,
  `nome_documento` varchar(45) DEFAULT NULL,
  `tipo_documento` int(11) DEFAULT NULL,
  `usuario_documento` int(11) DEFAULT NULL,
  `pedido_por` varchar(10) DEFAULT NULL,
  `data_pedido_documento` varchar(30) DEFAULT NULL,
  `emitido_por` varchar(10) DEFAULT NULL,
  `data_documento` date DEFAULT NULL,
  `estado_documento` varchar(45) DEFAULT '0',
  `efeito_documento` varchar(60) DEFAULT NULL,
  `descricao_documento` text,
  `documento_deleted` int(11) DEFAULT '0',
  `turma_documento` int(11) DEFAULT '0',
  `trimestre_documento` int(11) DEFAULT '0',
  PRIMARY KEY (`id_documento`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

-- Dumping data for table gestor_escolar.documentos: ~12 rows (approximately)
/*!40000 ALTER TABLE `documentos` DISABLE KEYS */;
INSERT INTO `documentos` (`id_documento`, `nome_documento`, `tipo_documento`, `usuario_documento`, `pedido_por`, `data_pedido_documento`, `emitido_por`, `data_documento`, `estado_documento`, `efeito_documento`, `descricao_documento`, `documento_deleted`, `turma_documento`, `trimestre_documento`) VALUES
	(85, 'BOLETIM DE NOTAS', 1, 6, '0', NULL, '2', '2019-12-16', '0', NULL, NULL, 1, 10, 1),
	(86, 'DECLARACAO SEM NOTAS', 2, 37, '0', NULL, '2', '2019-12-16', '0', NULL, NULL, 0, 0, 0),
	(87, 'BOLETIM DE NOTAS', 1, 37, '0', NULL, '2', '2019-12-16', '0', NULL, NULL, 0, 12, 1),
	(88, 'DECLARACAO COM NOTAS', 3, 37, '0', NULL, '2', '2019-12-16', '0', NULL, NULL, 0, 0, 1),
	(89, 'CERTIFICADO', 4, 62, '0', NULL, '2', '2019-12-16', '0', NULL, NULL, 1, 0, 0),
	(90, 'CERTIFICADO', 4, 37, '0', NULL, '2', '2019-12-16', '0', NULL, NULL, 1, 0, 0),
	(91, 'CERTIFICADO', 4, 12, '0', NULL, '2', '2019-12-16', '0', NULL, NULL, 1, 0, 0),
	(92, 'BOLETIM DE NOTAS', 1, 6, '0', NULL, '2', '2019-12-18', '0', NULL, NULL, 0, 12, 1),
	(93, 'DECLARACAO COM NOTAS', 3, 22, '0', NULL, '2', '2019-12-18', '0', NULL, NULL, 0, 0, 1),
	(94, 'CERTIFICADO', 4, 9, '0', NULL, '2', '2019-12-18', '0', NULL, NULL, 0, 0, 0),
	(95, 'DECLARACAO COM NOTAS', 3, 6, '0', NULL, '2', '2019-12-18', '0', NULL, NULL, 0, 0, 3),
	(96, 'DECLARACAO COM NOTAS', 3, 6, '0', NULL, '2', '2019-12-18', '0', NULL, NULL, 0, 0, 2),
	(97, 'DECLARACAO COM NOTAS', 3, 6, '0', NULL, '2', '2019-12-18', '0', NULL, NULL, 0, 0, 3);
/*!40000 ALTER TABLE `documentos` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.estado
CREATE TABLE IF NOT EXISTS `estado` (
  `estado_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `estado_nome` varchar(20) DEFAULT NULL,
  `estado_descricao` tinytext,
  `estado_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`estado_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table gestor_escolar.estado: ~2 rows (approximately)
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` (`estado_id`, `estado_nome`, `estado_descricao`, `estado_deleted`) VALUES
	(1, 'Activo', NULL, 0),
	(2, 'Pendente', NULL, 0);
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.horariomodelos
CREATE TABLE IF NOT EXISTS `horariomodelos` (
  `hmod_id` int(11) NOT NULL AUTO_INCREMENT,
  `hmod_titulo` varchar(45) DEFAULT NULL,
  `hmod_userCria` varchar(45) DEFAULT NULL,
  `hmod_body` text,
  `hmod_dtCria` varchar(45) DEFAULT NULL,
  `hmod_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`hmod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table gestor_escolar.horariomodelos: ~0 rows (approximately)
/*!40000 ALTER TABLE `horariomodelos` DISABLE KEYS */;
/*!40000 ALTER TABLE `horariomodelos` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.horarios
CREATE TABLE IF NOT EXISTS `horarios` (
  `hr_id` int(11) NOT NULL AUTO_INCREMENT,
  `hr_turma` int(11) DEFAULT NULL,
  `hr_modelo` int(11) DEFAULT NULL,
  `hr_body` text COLLATE utf8_bin,
  `hr_dtCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `hr_userCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `hr_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`hr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.horarios: ~0 rows (approximately)
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;
INSERT INTO `horarios` (`hr_id`, `hr_turma`, `hr_modelo`, `hr_body`, `hr_dtCria`, `hr_userCria`, `hr_deleted`) VALUES
	(12, 8, 1, '{"seg_disciplina":["","","1",""],"seg_sala":["","","",""],"ter_disciplina":["1","","2",""],"ter_sala":["","","",""],"qua_disciplina":["","3","",""],"qua_sala":["","","",""],"qui_disciplina":["2","1","",""],"qui_sala":["","","",""],"sex_disciplina":["1","","",""],"sex_sala":["","","",""]}', '2019-12-16 13:13:55', '1', 0);
/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.horario_modelos
CREATE TABLE IF NOT EXISTS `horario_modelos` (
  `hmod_id` int(11) NOT NULL AUTO_INCREMENT,
  `hmod_body` text CHARACTER SET utf8,
  `hmod_titulo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `hmod_dtCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `hmod_userCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `hmod_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`hmod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.horario_modelos: ~0 rows (approximately)
/*!40000 ALTER TABLE `horario_modelos` DISABLE KEYS */;
INSERT INTO `horario_modelos` (`hmod_id`, `hmod_body`, `hmod_titulo`, `hmod_dtCria`, `hmod_userCria`, `hmod_deleted`) VALUES
	(1, '[{"inicio":"07:00","fim":"08:30"},{"inicio":"08:40","fim":"10:05"},{"inicio":"10:10","fim":"11:30"},{"inicio":"11:40","fim":"12:50"}]', 'Manhã', '2019-12-16 13:13:23', '1', 1),
	(2, '[{"inicio":"07:00","fim":"08:30"},{"inicio":"08:40","fim":"10:20"},{"inicio":"10:30","fim":"11:05"},{"inicio":"11:10","fim":"12:50"}]', 'Manhã', '2019-12-17 22:55:52', '1', 0);
/*!40000 ALTER TABLE `horario_modelos` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.notificacoes
CREATE TABLE IF NOT EXISTS `notificacoes` (
  `notif_id` int(11) NOT NULL AUTO_INCREMENT,
  `notif_titulo` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `notif_message` text COLLATE utf8_bin,
  `notif_dtCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `notif_for` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `notif_forEspecific` text COLLATE utf8_bin,
  `notif_criador` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `notif_deleted` int(11) DEFAULT '0',
  `notif_turma` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`notif_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.notificacoes: ~0 rows (approximately)
/*!40000 ALTER TABLE `notificacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `notificacoes` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.notificacoesvistas
CREATE TABLE IF NOT EXISTS `notificacoesvistas` (
  `nvista_id` int(11) NOT NULL AUTO_INCREMENT,
  `nvista_notif` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `nvista_pessoa` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `nvista_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`nvista_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.notificacoesvistas: ~0 rows (approximately)
/*!40000 ALTER TABLE `notificacoesvistas` DISABLE KEYS */;
/*!40000 ALTER TABLE `notificacoesvistas` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.pessoas
CREATE TABLE IF NOT EXISTS `pessoas` (
  `pessoa_id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_nome` varchar(80) DEFAULT NULL,
  `pessoa_dtNascimento` varchar(45) DEFAULT NULL,
  `pessoa_numeroDoc` varchar(45) DEFAULT NULL,
  `pessoa_docVal` varchar(45) DEFAULT NULL,
  `pessoa_docEmitido` varchar(45) DEFAULT NULL,
  `pessoa_docLocal` varchar(45) DEFAULT NULL,
  `pessoa_docExpiracao` varchar(45) DEFAULT NULL,
  `pessoa_estadoCivil` int(11) DEFAULT NULL,
  `pessoa_username` varchar(45) DEFAULT NULL,
  `pessoa_password` varchar(45) DEFAULT NULL,
  `pessoa_googleAuth` varchar(45) DEFAULT NULL,
  `pessoa_email` varchar(45) DEFAULT NULL,
  `pessoa_nTel` varchar(45) DEFAULT NULL,
  `pessoa_pai` varchar(80) DEFAULT NULL,
  `pessoa_mae` varchar(80) DEFAULT NULL,
  `pessoa_nProc` varchar(20) DEFAULT NULL,
  `pessoa_usertipo` int(11) DEFAULT NULL,
  `pessoa_morada` varchar(45) DEFAULT NULL,
  `pessoa_genero` varchar(10) DEFAULT NULL,
  `pessoa_nacionalidade` varchar(45) DEFAULT NULL,
  `pessoa_naturalidade` varchar(45) DEFAULT NULL,
  `pessoa_provincia` varchar(45) DEFAULT NULL,
  `pessoa_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`pessoa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Dumping data for table gestor_escolar.pessoas: ~65 rows (approximately)
/*!40000 ALTER TABLE `pessoas` DISABLE KEYS */;
INSERT INTO `pessoas` (`pessoa_id`, `pessoa_nome`, `pessoa_dtNascimento`, `pessoa_numeroDoc`, `pessoa_docVal`, `pessoa_docEmitido`, `pessoa_docLocal`, `pessoa_docExpiracao`, `pessoa_estadoCivil`, `pessoa_username`, `pessoa_password`, `pessoa_googleAuth`, `pessoa_email`, `pessoa_nTel`, `pessoa_pai`, `pessoa_mae`, `pessoa_nProc`, `pessoa_usertipo`, `pessoa_morada`, `pessoa_genero`, `pessoa_nacionalidade`, `pessoa_naturalidade`, `pessoa_provincia`, `pessoa_deleted`) VALUES
	(1, 'Director da Escola', '1996-03-03', '63HSUUU3ESJK0LA039', NULL, 'Luanda', NULL, NULL, 1, 'admin@gescola.com', '1234', '', NULL, '935273648', NULL, NULL, '4408', 1, 'Cazenga, Zona 18', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(2, 'Secretário da Escola', '1996-03-03', 'JS783KE93IJ390LA039', NULL, 'Luanda', NULL, NULL, 2, 'secretario@gescola.com', '1234', '', NULL, '945273849', NULL, NULL, '4407', 2, 'Petrangol', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(3, 'Leonel Daniel', '1996-03-03', 'JS783KEDF3443DLA039', NULL, 'Luanda', 'Luanda', '2020-05-20', 1, 'leonel@gescola.com', '1234', '', '', '936255835', '', '', '0', 3, 'Marginal', 'M', 'Angolana', 'Cazenga', NULL, 0),
	(4, 'ABEDNEGO JOSÉ FIGUEIREDO MESSELE', '1996-03-04', '63HSKDL3ESJK0LA039', NULL, 'Luanda', 'Luanda', '2020-03-26', 1, '', '1234', '', 'abegnego.jose@gmail.com', '928255835', '', '', '23444', 4, 'Rangel', 'M', 'Angolana', 'Rangel', NULL, 0),
	(5, 'ABEL CAUMBE UCUAHAMBA', '1996-03-05', '63HSUUU3ESJK0LA040', NULL, 'Luanda', NULL, NULL, 2, '23559', '1234', '', NULL, '928255836', NULL, NULL, '23559', 4, 'Sambizanga', 'M', 'Angolana', 'Boa Vista', NULL, 0),
	(6, 'ABEL FINANI MUANZA GABRIEL', '1996-03-06', '63HSUUU3ESJK0LA041', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255837', NULL, NULL, '23637', 4, 'Cuca', 'M', 'Angolana', 'São Paulo', NULL, 0),
	(7, 'ADEMIRO QUINTAS SEGUNDA', '1996-03-07', '63HSUUU3ESJK0LA042', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255838', NULL, NULL, '24014', 4, 'Hoji ya henda', 'M', 'Angolana', 'Luanda', NULL, 0),
	(8, 'ADLEU MANUEL AGOSTINHO ', '1996-03-08', '63HSUUU3ESJK0LA043', NULL, '2019-12-17', 'Luanda', '2019-12-12', 2, 'nada@tudo.com', '1234', '', NULL, '928255839', '68', '68', '23666', 4, 'Cazenga, Zona 18', 'M', 'Angolana', 'Viana', NULL, 0),
	(9, 'AFONSO GARCIA MARTINS', '1996-03-09', '63HSUUU3ESJK0LA044', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255840', NULL, NULL, '22085', 4, 'Petrangol', 'M', 'Angolana', 'Cacuaco', NULL, 0),
	(10, 'ALFREDO ARICLENES DA SILVA QUINDAI', '1996-03-10', '63HSUUU3ESJK0LA045', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255841', NULL, NULL, '22804', 4, 'Cuca', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(11, 'ANA DE FAUSTO MANUEL', '1996-03-11', '63HSUUU3ESJK0LA046', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255842', NULL, NULL, '22704', 4, 'Rangel', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(12, 'ANDRÉ MANECO MÁRIO', '1996-03-12', '63HSUUU3ESJK0LA047', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255843', NULL, NULL, '23343', 4, 'Sambizanga', 'M', 'Angolana', 'Cazenga', NULL, 0),
	(13, 'ANITA KENDE LAU OFUNDA', '1996-03-13', '63HSUUU3ESJK0LA048', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255844', NULL, NULL, '23376', 4, 'Cuca', 'M', 'Angolana', 'Rangel', NULL, 0),
	(14, 'ANTÓNIO DUMBO ZAMBO', '1996-03-14', '63HSUUU3ESJK0LA049', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255845', NULL, NULL, '22853', 4, 'Hoji ya henda', 'M', 'Angolana', 'Boa Vista', NULL, 0),
	(15, 'ANTÓNIO ESTÊVÃO CALIPA ', '1996-03-15', '63HSUUU3ESJK0LA050', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255846', NULL, NULL, '23403', 4, 'Cazenga, Zona 18', 'M', 'Angolana', 'São Paulo', NULL, 0),
	(16, 'CONCEIÇÃO ALVES MATEUS', '1996-03-16', '63HSUUU3ESJK0LA051', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255847', NULL, NULL, '20932', 4, 'Petrangol', 'M', 'Angolana', 'Luanda', NULL, 0),
	(17, 'DADILSON ALBERTO CHAVITO FERNANDES', '1996-03-17', '63HSUUU3ESJK0LA052', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255848', NULL, NULL, '22877', 4, 'Cuca', 'M', 'Angolana', 'Viana', NULL, 0),
	(18, 'DALVA ERNESTINACALENGUE SUMBO', '1996-03-18', '63HSUUU3ESJK0LA053', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255849', NULL, NULL, '21287', 4, 'Rangel', 'M', 'Angolana', 'Cacuaco', NULL, 0),
	(19, 'DANIELA CARLA CARDOSO DA SILVA FUTI', '1996-03-19', '63HSUUU3ESJK0LA054', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255850', NULL, NULL, '22288', 4, 'Sambizanga', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(20, 'DÉRIO DE ALMEIDA QUEQUE', '1996-03-20', '63HSUUU3ESJK0LA055', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255851', NULL, NULL, '23429', 4, 'Cuca', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(21, 'DOMINGAS ANTÓNIO BAIÃO', '1996-03-21', '63HSUUU3ESJK0LA056', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255852', NULL, NULL, '21115', 4, 'Hoji ya henda', 'M', 'Angolana', 'Cazenga', NULL, 0),
	(22, 'DOMINGOS JOSÉ JOÃO CANOEIRA', '1996-03-22', '63HSUUU3ESJK0LA057', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255853', NULL, NULL, '23648', 4, 'Cazenga, Zona 18', 'M', 'Angolana', 'Rangel', NULL, 0),
	(23, 'DONALDO FERREIRA MANUEL ', '1996-03-23', '63HSUUU3ESJK0LA058', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255854', NULL, NULL, '25064', 4, 'Petrangol', 'M', 'Angolana', 'Boa Vista', NULL, 0),
	(24, 'EDVÂNIO JOAQUIM QUIJILA', '1996-03-24', '63HSUUU3ESJK0LA059', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255855', NULL, NULL, '23041', 4, 'Cuca', 'M', 'Angolana', 'São Paulo', NULL, 0),
	(25, 'ESPERANÇA ZAMBO SANTOS ', '1996-03-25', '63HSUUU3ESJK0LA060', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255856', NULL, NULL, '22903', 4, 'Rangel', 'M', 'Angolana', 'Luanda', NULL, 0),
	(26, 'FRANCISCO DANIEL FAZTUDO', '1996-03-26', '63HSUUU3ESJK0LA061', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255857', NULL, NULL, '22917', 4, 'Sambizanga', 'M', 'Angolana', 'Viana', NULL, 0),
	(27, 'HELDER ALEXANDRE DA SILVA', '1996-03-27', '63HSUUU3ESJK0LA062', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255858', NULL, NULL, '23690', 4, 'Cuca', 'M', 'Angolana', 'Cacuaco', NULL, 0),
	(28, 'HIGINO MÁRIO DOS SANTOS FERNANDO ', '1996-03-28', '63HSUUU3ESJK0LA063', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255859', NULL, NULL, '22926', 4, 'Hoji ya henda', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(29, 'ILDOLGAR ACÁCIO DA COSTA TCHIPACA', '1996-03-03', '63HSUUU3ESJK0LA064', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255860', NULL, NULL, '22928', 4, 'Cazenga, Zona 18', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(30, 'ISABEL PEDRO PAULO ', '1996-03-03', '63HSUUU3ESJK0LA065', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255861', NULL, NULL, '24022', 4, 'Petrangol', 'M', 'Angolana', 'Cazenga', NULL, 0),
	(31, 'ISMAEL VUNGE MORAIS ', '1996-03-03', '63HSUUU3ESJK0LA066', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255862', NULL, NULL, '22931', 4, 'Cuca', 'M', 'Angolana', 'Rangel', NULL, 0),
	(32, 'JANETH GONÇALVES VILOLA', '1996-03-03', '63HSUUU3ESJK0LA067', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255863', NULL, NULL, '23808', 4, 'Rangel', 'M', 'Angolana', 'Boa Vista', NULL, 0),
	(33, 'JANUARIO DOMINGOS PAPUSSECO CASCOTE', '1996-03-03', '63HSUUU3ESJK0LA068', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255864', NULL, NULL, '23089', 4, 'Sambizanga', 'M', 'Angolana', 'São Paulo', NULL, 0),
	(34, 'JOAQUIM COIMBRA CORREIA', '1996-03-03', '63HSUUU3ESJK0LA069', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255865', NULL, NULL, '22017', 4, 'Cuca', 'M', 'Angolana', 'Luanda', NULL, 0),
	(35, 'JOSÉ ALBERTO CALUFELE GOMBIWA', '1996-03-03', '63HSUUU3ESJK0LA070', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255866', NULL, NULL, '23424', 4, 'Hoji ya henda', 'M', 'Angolana', 'Viana', NULL, 0),
	(36, 'JOSIAS SALOMÃO FIGUEIRA', '1996-03-03', '63HSUUU3ESJK0LA071', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255867', NULL, NULL, '23548', 4, 'Cazenga, Zona 18', 'M', 'Angolana', 'Cacuaco', NULL, 0),
	(37, 'JULIETA DOMINGOS DANIEL', '1996-03-03', '63HSUUU3ESJK0LA072', NULL, 'Luanda', '', '', 1, '', '1234', '', '', '928255868', '', '', '23114', 4, 'Vila Alice', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(38, 'KAMBILA DA SILVA NHANGA ', '1996-03-03', '63HSUUU3ESJK0LA073', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255869', NULL, NULL, '25075', 4, 'Cuca', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(39, 'LEONOR CARLOS', '1996-03-03', '63HSUUU3ESJK0LA074', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255870', NULL, NULL, '22247', 4, 'Rangel', 'M', 'Angolana', 'Cazenga', NULL, 0),
	(40, 'LINA LUZALA MATUSIMUA', '1996-03-03', '63HSUUU3ESJK0LA075', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255871', NULL, NULL, '24729', 4, 'Sambizanga', 'M', 'Angolana', 'Rangel', NULL, 0),
	(41, 'LOURENÇO MENDES CABRAL', '1996-03-03', '63HSUUU3ESJK0LA076', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255872', NULL, NULL, '22168', 4, 'Cuca', 'M', 'Angolana', 'Boa Vista', NULL, 0),
	(42, 'LUIDMILA DE FÁTIMA BREGANHA QUSSANGA', '1996-03-03', '63HSUUU3ESJK0LA077', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255873', NULL, NULL, '23488', 4, 'Hoji ya henda', 'M', 'Angolana', 'São Paulo', NULL, 0),
	(43, 'MANUEL VENTURA MIGUEL', '1996-03-03', '63HSUUU3ESJK0LA078', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255874', NULL, NULL, '23517', 4, 'Cazenga, Zona 18', 'M', 'Angolana', 'Luanda', NULL, 0),
	(44, 'MARIA MARLENE DE ALMEIDA ', '1996-03-03', '63HSUUU3ESJK0LA079', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255875', NULL, NULL, '25083', 4, 'Petrangol', 'M', 'Angolana', 'Viana', NULL, 0),
	(45, 'MATONDO AUGUSTO ZOLA NTALULU', '1996-03-03', '63HSUUU3ESJK0LA080', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255876', NULL, NULL, '23616', 4, 'Cuca', 'M', 'Angolana', 'Cacuaco', NULL, 0),
	(46, 'MILENA DAIANA BOTELHO FERNANDES MENDES', '1996-03-03', '63HSUUU3ESJK0LA081', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255877', NULL, NULL, '22605', 4, 'Rangel', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(47, 'NSIMBA LUTONADIO ADRÉ BERNARDO', '1996-03-03', '63HSUUU3ESJK0LA082', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255878', NULL, NULL, '23497', 4, 'Sambizanga', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(48, 'OSVALDO VUNGE ALBERTO', '1996-03-03', '63HSUUU3ESJK0LA083', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255879', NULL, NULL, '23172', 4, 'Cuca', 'M', 'Angolana', 'Cazenga', NULL, 0),
	(49, 'PATRÍCIO MUZIBI SIMÃO', '1996-03-03', '63HSUUU3ESJK0LA084', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255880', NULL, NULL, '23703', 4, 'Hoji ya henda', 'M', 'Angolana', 'Rangel', NULL, 0),
	(50, 'PAULA DALA LUTETE', '1996-03-03', '63HSUUU3ESJK0LA085', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255881', NULL, NULL, '22383', 4, 'Cazenga, Zona 18', 'M', 'Angolana', 'Boa Vista', NULL, 0),
	(51, 'RAFAEL ALEXANDRE ZECA', '1996-03-03', '63HSUUU3ESJK0LA086', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255882', NULL, NULL, '24001', 4, 'Petrangol', 'M', 'Angolana', 'São Paulo', NULL, 0),
	(52, 'RAFAELA ALEXANDRA BALOMBO QUIÚMA ', '1996-03-03', '63HSUUU3ESJK0LA087', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255883', NULL, NULL, '23554', 4, 'Cuca', 'M', 'Angolana', 'Luanda', NULL, 0),
	(53, 'RICARDO NDOMBAXI CALONDA', '1996-03-03', '63HSUUU3ESJK0LA088', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255884', NULL, NULL, '23334', 4, 'Rangel', 'M', 'Angolana', 'Viana', NULL, 0),
	(54, 'ROSÁRIA JANDIRA CAIOMBO CALIALIA', '1996-03-03', '63HSUUU3ESJK0LA089', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255885', NULL, NULL, '22839', 4, 'Sambizanga', 'M', 'Angolana', 'Cacuaco', NULL, 0),
	(55, 'SIMARA ROSA DOMINGOS DE ALMEIDA', '1996-03-03', '63HSUUU3ESJK0LA090', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255886', NULL, NULL, '23398', 4, 'Cuca', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(56, 'SUZANA ARMINDO MUHONGO', '1996-03-03', '63HSUUU3ESJK0LA091', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255887', NULL, NULL, '23282', 4, 'Hoji ya henda', 'M', 'Angolana', 'Ingombotas', NULL, 0),
	(57, 'TADEU BIAIA INÁCIO', '1996-03-03', '63HSUUU3ESJK0LA092', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255888', NULL, NULL, '21158', 4, 'Cazenga, Zona 18', 'M', 'Angolana', 'Cazenga', NULL, 0),
	(58, 'VLADIMIR FANIQUISSA COELHO', '1996-03-03', '63HSUUU3ESJK0LA093', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255889', NULL, NULL, '23919', 4, 'Petrangol', 'M', 'Angolana', 'Rangel', NULL, 0),
	(59, 'VENÂNCIO CARLOS HEBO', '1996-03-03', '63HSUUU3ESJK0LA094', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255890', NULL, NULL, '23294', 4, 'Cuca', 'M', 'Angolana', 'Boa Vista', NULL, 0),
	(60, 'VIOLETA MARIANA BAPTISTA', '1996-03-03', '63HSUUU3ESJK0LA095', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255891', NULL, NULL, '23673', 4, 'Rangel', 'M', 'Angolana', 'São Paulo', NULL, 0),
	(61, 'ANTUNES MANUEL IMPERIAL', '1996-03-03', '63HSUUU3ESJK0LA096', NULL, 'Luanda', NULL, NULL, 1, '', '1234', '', NULL, '928255892', NULL, NULL, '25433', 4, 'Sambizanga', 'M', 'Angolana', 'Luanda', NULL, 0),
	(62, 'HUGO RICARDO JOSÉ BASTOS', '1996-03-03', '63HSUUU3ESJK0LA097', NULL, 'Luanda', NULL, NULL, 2, '', '1234', '', NULL, '928255893', NULL, NULL, '25450', 4, 'Cuca', 'M', 'Angolana', 'Viana', NULL, 0),
	(74, 'AUGUSTO DOS SANTOS', '1990-07-03', '0034JIL045D', NULL, '2019-12-02', 'Luanda', '2020-01-03', NULL, 'ng@g.com', '1234', NULL, 'augusto@gescola.com', '83863672', '68', '68', '777007', 4, 'Aéneas', 'F', NULL, NULL, NULL, 0),
	(75, 'MARIA DE CARMO GONÇALVEIS', '1990-02-06', '0034JIL045G', NULL, '2019-12-02', 'Luanda', '2020-01-02', NULL, NULL, '1234', NULL, 'maria@gescola.com', '788382822', '68', '68', '777008', 4, 'AENEAS', 'M', NULL, NULL, NULL, 0),
	(76, 'TARCIO DIAS', '2019-12-03', '01010010', NULL, '2019-12-16', 'Luanda', '2019-12-23', NULL, 'admin@example.com', '1234', NULL, NULL, '928427642', NULL, NULL, NULL, 5, 'Cazenga', 'M', NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `pessoas` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.tipoavaliacao
CREATE TABLE IF NOT EXISTS `tipoavaliacao` (
  `tipoAvaliacao_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoAvaliacao_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `tipoAvaliacao_descricao` text CHARACTER SET utf8,
  `tipoAvaliacao_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`tipoAvaliacao_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.tipoavaliacao: ~0 rows (approximately)
/*!40000 ALTER TABLE `tipoavaliacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoavaliacao` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.tipo_documento
CREATE TABLE IF NOT EXISTS `tipo_documento` (
  `idtipo_documento` int(11) NOT NULL,
  `nome_tipodocumento` varchar(45) DEFAULT NULL,
  `documento_deleted` int(5) DEFAULT '0',
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_documento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table gestor_escolar.tipo_documento: ~4 rows (approximately)
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
INSERT INTO `tipo_documento` (`idtipo_documento`, `nome_tipodocumento`, `documento_deleted`, `estado`) VALUES
	(1, 'Boletim de Notas', 0, 'activo'),
	(2, 'Declaracão Sem Notas', 0, 'activo'),
	(3, 'Declaração Com Notas', 0, NULL),
	(4, 'Certificado', 0, NULL);
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.tipo_usuario
CREATE TABLE IF NOT EXISTS `tipo_usuario` (
  `tipoUsuario_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipoUsuario_nome` varchar(20) DEFAULT NULL,
  `tipoUsuario_descricao` tinytext,
  `tipoUsuario_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`tipoUsuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table gestor_escolar.tipo_usuario: ~4 rows (approximately)
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` (`tipoUsuario_id`, `tipoUsuario_nome`, `tipoUsuario_descricao`, `tipoUsuario_deleted`) VALUES
	(1, 'Director', NULL, 0),
	(2, 'Secretario', NULL, 0),
	(3, 'Professor', NULL, 0),
	(4, 'Estudante', NULL, 0);
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.turmapessoas
CREATE TABLE IF NOT EXISTS `turmapessoas` (
  `turmaPessoa_id` int(11) NOT NULL AUTO_INCREMENT,
  `turmaPessoa_pessoa` int(11) DEFAULT NULL,
  `turmaPessoa_turma` int(11) DEFAULT NULL,
  `turmaPessoa_disciplinas` text COLLATE utf8_bin,
  `turmaPessoa_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`turmaPessoa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.turmapessoas: ~32 rows (approximately)
/*!40000 ALTER TABLE `turmapessoas` DISABLE KEYS */;
INSERT INTO `turmapessoas` (`turmaPessoa_id`, `turmaPessoa_pessoa`, `turmaPessoa_turma`, `turmaPessoa_disciplinas`, `turmaPessoa_deleted`) VALUES
	(123, 4, 10, '["all"]', 1),
	(124, 5, 10, '["all"]', 1),
	(125, 6, 10, '["all"]', 1),
	(126, 7, 10, '["all"]', 1),
	(127, 8, 10, '["all"]', 1),
	(128, 9, 10, '["all"]', 1),
	(129, 32, 11, '["all"]', 1),
	(130, 33, 11, '["all"]', 1),
	(131, 34, 11, '["all"]', 1),
	(132, 35, 11, '["all"]', 1),
	(133, 3, 11, '["3"]', 1),
	(134, 3, 10, '["2"]', 1),
	(135, 37, 12, '["all"]', 0),
	(136, 3, 12, '["1"]', 0),
	(137, 5, 12, '["all"]', 0),
	(138, 6, 12, '["all"]', 0),
	(139, 7, 12, '["all"]', 0),
	(140, 8, 12, '["all"]', 0),
	(141, 9, 12, '["all"]', 0),
	(142, 10, 12, '["all"]', 0),
	(143, 11, 12, '["all"]', 0),
	(144, 12, 12, '["all"]', 0),
	(145, 13, 12, '["all"]', 0),
	(146, 14, 12, '["all"]', 0),
	(147, 15, 12, '["all"]', 0),
	(148, 16, 12, '["all"]', 0),
	(149, 17, 12, '["all"]', 0),
	(150, 18, 12, '["all"]', 0),
	(151, 19, 12, '["all"]', 0),
	(152, 20, 12, '["all"]', 1),
	(153, 21, 12, '["all"]', 0),
	(154, 22, 12, '["all"]', 0);
/*!40000 ALTER TABLE `turmapessoas` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.turmas
CREATE TABLE IF NOT EXISTS `turmas` (
  `turma_id` int(11) NOT NULL AUTO_INCREMENT,
  `turma_nome` varchar(45) NOT NULL,
  `turma_curso` int(11) NOT NULL,
  `turma_classeId` int(11) NOT NULL,
  `turma_turno` int(11) NOT NULL,
  `turma_ano` int(11) NOT NULL,
  `turma_disciplinas` text,
  `turma_descricao` text,
  `turma_dataCriacao` varchar(50) DEFAULT NULL,
  `turma_userCriador` int(11) DEFAULT NULL,
  `turma_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`turma_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table gestor_escolar.turmas: ~2 rows (approximately)
/*!40000 ALTER TABLE `turmas` DISABLE KEYS */;
INSERT INTO `turmas` (`turma_id`, `turma_nome`, `turma_curso`, `turma_classeId`, `turma_turno`, `turma_ano`, `turma_disciplinas`, `turma_descricao`, `turma_dataCriacao`, `turma_userCriador`, `turma_deleted`) VALUES
	(10, 'A1', 1, 1, 3, 2019, '["2"]', 'Turma A1', '2019-12-16 14:11:25', 1, 1),
	(11, 'A2', 1, 1, 1, 2019, '["3"]', 'A2', '2019-12-16 14:12:42', 1, 1),
	(12, 'A1', 1, 1, 4, 2019, '["1","2","3"]', 'Teste', '2019-12-16 15:05:22', 1, 0);
/*!40000 ALTER TABLE `turmas` ENABLE KEYS */;

-- Dumping structure for table gestor_escolar.turnos
CREATE TABLE IF NOT EXISTS `turnos` (
  `turno_id` int(11) NOT NULL AUTO_INCREMENT,
  `turno_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `turno_descricao` text COLLATE utf8_bin,
  `turno_dataCriacao` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `turno_userCriador` int(11) DEFAULT NULL,
  `turno_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`turno_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table gestor_escolar.turnos: ~3 rows (approximately)
/*!40000 ALTER TABLE `turnos` DISABLE KEYS */;
INSERT INTO `turnos` (`turno_id`, `turno_nome`, `turno_descricao`, `turno_dataCriacao`, `turno_userCriador`, `turno_deleted`) VALUES
	(1, 'Manhã', NULL, NULL, NULL, 0),
	(2, 'Tarde', NULL, NULL, NULL, 0),
	(3, 'Noite', '', NULL, NULL, 0);
/*!40000 ALTER TABLE `turnos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
