
function menu_refreshProfilePhoto()
{
  var srcVar = $("#img-menu-profile-photo").attr("src");
    $("#img-menu-profile-photo").attr("src", srcVar);
}

$(".num1, .num2, .num3").keyup(function(e){

    var atualvalor = $(this).val();

    if((atualvalor) < 0)
    {
        $(this).val("0");
    }
    if((atualvalor) > 20)
    {
        alert("Número Inválido! Valor Máximo 20")
        $(this).val("20");
    }

    var linha = $(this).parent().parent();

    var num1 = parseFloat($(linha).find(".num1").val());
    var num2 = parseFloat($(linha).find(".num2").val());
    var num3 = parseFloat($(linha).find(".num3").val());

    var res = (num1+num2+num3)/3;

    if(isNaN(res))
    {
        res = 0;
    }

    var resFinal = ((res - parseInt(res)) == 0) ? parseInt(res) : res.toFixed(2);

    $(linha).find(".resultado").val(resFinal);

});


$(".num1, .num2, .num3").keydown(function(e){

    var aborted = false;
    if(((e.keyCode) < 48) || ((e.keyCode) > 57))
    {
        if((e.keyCode != 190) && (e.keyCode != 8))
        {
            e.preventDefault();
            aborted = true;
        }
    }

    if(!aborted)
    {
        if((e.keyCode != 190))
        {
            //MARCAR COMO MODIFICADO
            $(this).addClass("modified-field");
        }
    }

});





